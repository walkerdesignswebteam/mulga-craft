// Makes ALL vimeo + youtube videos responsive without anything fancy.
(function ( window, document, undefined ) {
    /*
    * Grab all iframes on the page or return
    */
    var iframes = document.getElementsByTagName( 'iframe' ); 

    /*
    * Loop through the iframes array
    */
    for ( var i = 0; i < iframes.length; i++ ) {

        var iframe = iframes[i], 

        /*
        * RegExp, extend this if you need more players
        */
        players = /www.youtube.com|player.vimeo.com/;

        /*
        * If the RegExp pattern exists within the current iframe
        */
        if ( iframe.src.search( players ) > 0 ) {

            /*
            * Calculate the video ratio based on the iframe's w/h dimensions
            */
            var videoRatio        = ( iframe.height / iframe.width ) * 100;
            
            /*
            * Replace the iframe's dimensions and position
            * the iframe absolute, this is the trick to emulate 
            * the video ratio
            */
            iframe.style.position = 'absolute'; 
            iframe.style.top      = '0';
            iframe.style.left     = '0';
            iframe.width          = '100%';
            iframe.height         = '100%';
            
            /*
            * Wrap the iframe in a new <div> which uses a
            * dynamically fetched padding-top property based
            * on the video's w/h dimensions
            */
            var wrap              = document.createElement( 'div' );
            wrap.className        = 'fluid-vids';
            wrap.style.width      = '100%';
            wrap.style.position   = 'relative';
            wrap.style.paddingTop = videoRatio + '%';
            
            /*
            * Add the iframe inside our newly created <div>
            */
            var iframeParent      = iframe.parentNode;
            iframeParent.insertBefore( wrap, iframe );
            wrap.appendChild( iframe );

        }

    }
 
})( window, document );

function jqIsReady() {

    jQuery(document).ready(function () {

        $("#nav-mobile").mmenu({
            slidingSubmenus: false,
            offCanvas: {
                position: 'right',
            }
        });
        var API = $("#nav-mobile").data( "mmenu" );
        $("#nav-mobile-button").click(function() {
            API.open();
        });

        $("nav.nav-primary:first").accessibleMegaMenu({
            /* prefix for generated unique id attributes, which are required
             to indicate aria-owns, aria-controls and aria-labelledby */
            uuidPrefix: "accessible-megamenu",
            /* css class used to define the megamenu styling */
            menuClass: "nav-menu",
            /* css class for a top-level navigation item in the megamenu */
            topNavItemClass: "nav-item",
            /* css class for a megamenu panel */
            panelClass: "sub-nav",
            /* css class for a group of items within a megamenu panel */
            panelGroupClass: "sub-nav-group",
            /* css class for the hover state */
            hoverClass: "hover",
            /* css class for the focus state */
            focusClass: "focus",
            /* css class for the open state */
            openClass: "open"
        });


        //Mast Head Slideshow

        //Mast Head
        var tpj=jQuery;
        var revapi20;
        if(tpj("#masthead-hero").revolution === undefined){
            revslider_showDoubleJqueryError("#masthead-hero");
        }else{
            revapi20 = tpj("#masthead-hero").show().revolution({
                sliderType:"hero",
                jsFileLocation:"/assets/js/",
                // sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                },
                responsiveLevels:[1024,1024,768,480],
                // gridwidth:[1024,1024,768,480],
                // gridheight:[495,495,371,371],
                gridwidth:1024,
                gridheight:495,
                lazyType:"none",
                parallax: {
                    type:"mouse",
                    origo:"slidercenter",
                    speed:2000,
                    levels:[2,3,4,5,6,7,12,16,10,50],
                },
                shadow:0,
                spinner:"spinner0",
                autoHeight:"on",
                disableProgressBar:"on",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    disableFocusListener:false,
                }
            });
        }
        //run home page slideshow
        if(typeof(runHomePageSlideshow)!=='undefined') {
            runHomePageSlideshow();
        }


        $('body').prepend('<a href="javascript:;" class="back-to-top"><span>Back to<br /> the top</span></a>');
        var amountScrolled = 300;

        $(window).scroll(function() {
            if ( $(window).scrollTop() > amountScrolled ) {
                $('a.back-to-top').fadeIn('slow');
            } else {
                $('a.back-to-top').fadeOut('slow');
            }
        });

        $('a.back-to-top, a.simple-back-to-top').click(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 700);
            return false;
        });

        $('.inpagescrolllink').on('click', function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
        });

        //Set anchor location to avoid loading elements messing with scroll position
        if (location.hash) {
            setTimeout(function(){location.href = location.hash; }, 1000);
            setTimeout(function(){location.href = location.hash; }, 2000);
            location.href = location.hash;
        }

        /*************************/
        /* Window resize scripts */
        /*************************/
        function resizeSets() {
            $('.matchHeight').height('auto');
            $('.matchHeight').matchHeight();
        }

        $(window).resize(function (e) {
            resizeSets();
        });
        resizeSets();

        //Jquery validate for Mailchimp form
        $('#mc-embedded-subscribe-form').validate();

        $('[data-fancybox="gallery-image"]').fancybox({
            thumbs : {
                autoStart : true
            }
        });

        var royalSliders = $('.royalSlider');
        if(royalSliders.length>0) {
            $('.royalSlider').royalSlider({
                fullscreen: {
                    enabled: false,
                    nativeFS: true
                },
                controlNavigation: 'thumbnails',
                autoScaleSlider: true,
                autoScaleSliderWidth: 1200,
                autoScaleSliderHeight: 1200,
                loop: false,
                imageScaleMode: 'fit-if-smaller',
                navigateByClick: true,
                numImagesToPreload: 2,
                arrowsNav: true,
                arrowsNavAutoHide: true,
                arrowsNavHideOnTouch: true,
                keyboardNavEnabled: true,
                fadeinLoadedSlide: true,
                globalCaption: false,
                globalCaptionInside: false,
                thumbs: {
                    appendSpan: true,
                    firstMargin: true,
                    paddingBottom: 4
                }
            });
        }
    });
}

function testForJq() {

    if (typeof GLOB_JsLibsLoaded==="undefined") {
        setTimeout(testForJq, 5);
    }else{
        jqIsReady();
    }
}
testForJq();


