<?php
namespace Craft;

use QuickCart\Cart;
use QuickCart\Manager;

class QuickCart_CheckoutController extends QuickCart_BaseController
{
    protected $allowAnonymous = array('actionCheckout');

    public function actionCheckout(array $variables = array())
    {
        Manager::setCart();
        echo $this->getQuickCartTemplate('checkout', array(
            'routes'=>Cart::getRoutes(),
            'cart'=>Manager::getCart())
        );
    }
}