<?php
namespace Craft;
class QuickCart_ProductsService extends BaseApplicationComponent
{
    //get all product by criteria
    public function find($searchOptions=NULL)
    {
        if(empty($searchOptions)){
            $records = QuickCart_ProductRecord::model()->findAll();
        }else{
            $records = QuickCart_ProductRecord::model()->findAllByAttributes($searchOptions);
        }
        return QuickCart_ProductModel::populateModels($records);
    }
}