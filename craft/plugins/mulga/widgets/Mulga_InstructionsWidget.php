<?php
namespace Craft;

class Mulga_InstructionsWidget extends BaseWidget
{
    protected $colspan = 2;

    public function getName()
    {
        return Craft::t('Site General Notes');
    }

    public function getBodyHtml()
    {
        return '
<table class="data">
<tr><td>Tour pages will show availaible seats when less than 6 remain.</td></tr>
<tr><td><a href="/regen">Manually Regenerate Rezdy Cache</a><br />NOTE: Rezdy cache currently set to auto regen every 4 hours.</td></tr>
</table>
	';
    }
}