<?php
namespace Craft;
class QuickCart_CategoriesService extends BaseApplicationComponent
{
    public function getAllByParent($parent)
    {
        $records = QuickCart_CategoryRecord::model()->findAllByAttributes(array('parent'=>$parent),array('order'=>'`order` ASC, `id` ASC'));
        if($records) {
            return QuickCart_CategoryModel::populateModels($records);
        }
    }

    public function getById($id)
    {
        $record = QuickCart_CategoryRecord::model()->findById($id);
        if ($record) {
            return QuickCart_CategoryModel::populateModel($record);
        }
    }
    public function getByCode($code)
    {
        $record = QuickCart_CategoryRecord::model()->findByAttributes(array('code' => $code));
        if($record) {
            return QuickCart_CategoryModel::populateModel($record);
        }
    }

    public function getTree($parent=''){
        $a_ret = array();
        $o_categories = $this->getAllByParent($parent);
        if($o_categories) {
            foreach ($o_categories as $o_category) {
                $a_thisBranch = array('model' => $o_category);
                $o_children = $this->getTree($o_category->code);
                if (count($o_children) > 0) {
                    $a_thisBranch['children'] = $o_children;
                }
                $a_ret[] = $a_thisBranch;
            }
        }
        return $a_ret;
    }
    public function getFlatTreeCategories($parent=''){
        $a_ret = array();
        $o_categories = $this->getAllByParent($parent);
        if($o_categories) {
            foreach ($o_categories as $o_category) {
                $a_ret[] = $o_category;
                $a_children = $this->getFlatTreeCategories($o_category->code);
                if (count($a_children) > 0) {
                    $a_ret = array_merge($a_ret, $a_children);
                }
            }
        }
        return $a_ret;
    }
    public function getTreeSelectOptions($parent='',$s_indent = '', $b_includeBlank=true,$blankLabel='--TOP--'){
        $a_ret = array();
        if($b_includeBlank){
            $a_ret['']=$blankLabel;
        }
        $o_categories = $this->getAllByParent($parent);
        if($o_categories) {
            foreach ($o_categories as $o_category) {
                $a_ret[$o_category->code] = $s_indent . $o_category->name;
                $a_children = $this->getTreeSelectOptions($o_category->code, $s_indent . ' - ', false);
                if (count($a_children) > 0) {
                    $a_ret = array_merge($a_ret, $a_children);
                }
            }
        }
        return $a_ret;
    }
    public function getTreeSelectFieldOptions($parent=''){
        $a_opts = $this->getTreeSelectOptions($parent='',$s_indent = '',false);
        $a_ret = array();
        foreach($a_opts as $key=>$val){
            $a_ret[]=array('value'=>$key,'label'=>$val);
        }
        return $a_ret;
    }

    //reference model directly so we can update id
    public function saveCategory(QuickCart_CategoryModel &$model)
    {
        if ($id = $model->getAttribute('id')) {
            if (null === ($record = QuickCart_CategoryRecord::model()->findById($id))) {
                throw new Exception(Craft::t('Can\'t find category with ID "{id}"', array('id' => $id)));
            }
        } else {
            $record = new QuickCart_CategoryRecord();
        }
        //copy values into model
        foreach($model->getAttributes() as $key=>$val){
            $record->setAttribute($key,$val);
        }
        //check for errors
        $record->validate();
        $model->addErrors($record->getErrors());

        if ($model->hasErrors()) {
            return false;
        }else{
            $b_saveRes = $record->save();
            if($b_saveRes){
                $model->setAttribute('id', $record->getAttribute('id'));
            }
            return $b_saveRes;
        }
    }

    public function deleteCategory(QuickCart_CategoryModel $model){
        $o_category = QuickCart_CategoryRecord::model()->findById($model->id);
        if($o_category) {
            return $o_category->delete();
        }else{
            return false;
        }
    }

}