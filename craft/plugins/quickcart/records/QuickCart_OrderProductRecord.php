<?php
namespace Craft;
class QuickCart_OrderProductRecord extends BaseRecord
{
	public function getTableName()
    {
        return 'quickcart_orderproduct';
    }

    protected function defineAttributes()
    {
        return array(
			'order_id' =>  AttributeType::Number,
			
			'code' => AttributeType::String,
			'name' => AttributeType::String,
			'price' => array('type'=>AttributeType::Number,'decimals'=>2),
			'tax' => array('type'=>AttributeType::Number,'decimals'=>2),
			'quantity' => AttributeType::Number,
			
			'extraData' => AttributeType::Mixed,
			
        );
    }
	public function defineRelations()
    {
        return array(
            'order_id' => array(static::HAS_MANY, 'QuickCart_Order', 'id'),
        );
    }
}
