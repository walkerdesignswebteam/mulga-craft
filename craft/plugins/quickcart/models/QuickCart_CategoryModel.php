<?php
namespace Craft;
class QuickCart_CategoryModel extends BaseModel
{
    protected function defineAttributes()
    {
        return array(
            'id' => AttributeType::Number,

            //copy and pasted from record
            'name' => AttributeType::String,
            'parent' => AttributeType::String,
            'code' => AttributeType::String,
            'description'=> AttributeType::Mixed,
            'order' => AttributeType::Number,
            'enabled' => AttributeType::Bool,
        );
    }
}