//su root
//yum install nodejs
//yum install npm
//yum install ruby
//yum install rubygems
//gem install sass
//npm install --global gulp
//npm install gulp-react gulp-ruby-sass gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-rename gulp-cache del --save-dev

//To debug
//DEBUG=* gulp <task>

/*
 * Inspired by https://markgoodyear.com/2014/01/getting-started-with-gulp/
 */
module.exports = function(config) {
  "use strict";
	var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	minifycss = require('gulp-minify-css'),
	jshint = require('gulp-jshint'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	react = require('gulp-react')
	;
	//imagemin = require('gulp-imagemin'),
	//notify = require('gulp-notify'),
	//cache = require('gulp-cache'),
	//del = require('del')

	function onError(err) {
		console.log(err);
		this.emit('end');
	}
	function onMessage(msg) {
		console.log(msg);
	}


	gulp.task('styles', function () {
		return gulp.src(config.scssStyles)
			.pipe(sass({sourceComments: 'map',  outputStyle: 'expanded'}))  //,  sourceMap: 'sass'
			.on('error', onError)
			.pipe(autoprefixer('> 1%'))
			.on('error', onError)
			.pipe(gulp.dest('craft/src/csscache'))
			.on('error', onError)
			;
	});

	gulp.task('cssCombine',['styles'], function () {
		return gulp.src(config.cssCombine)
			.on('error', onError)
			.pipe(concat('styles.css'))
			.pipe(gulp.dest('public_html/assets/css/'))
			.pipe(rename({suffix: '.min'}))
			.pipe(minifycss())
			.on('error', onError)
			.pipe(gulp.dest('public_html/assets/css/'));
	});

	// Scripts
	gulp.task('js_libs', function () {
		return gulp.src(config.jsLibs)
		.pipe(concat('libs.js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('public_html/assets/js'))
		;
	});
	gulp.task('js_template', function () {
		return gulp.src(config.jsMain)
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(concat('main.js'))
		.pipe(gulp.dest('public_html/assets/js'))
		.pipe(rename({suffix: '.min'}))
		.pipe(uglify())
		.on('error', onError)
		.pipe(gulp.dest('public_html/assets/js'))
		;
	});

	gulp.task('jsx', function () {
		return gulp.src(config.jsx)
			.pipe(react())
			.on('error', onError)
			.pipe(concat('react_app.js'))
			.pipe(gulp.dest('public_html/assets/js'));
		;
	});

	gulp.task('default', function () {
		//main task - just run "gulp"
		gulp.watch('craft/src/scss/*.scss', ['cssCombine']);
		gulp.watch(config.jsx, ['jsx']);
		gulp.watch('craft/src/js/*.js', [
			'js_libs', //makes a libs.min.js file
			'js_template' // makes a main.js file
		]);

	});
};