<?php
namespace Craft;
class QuickCart_CategoryRecord extends BaseRecord
{
	public function getTableName()
    {
        return 'quickcart_category';
    }

    protected function defineAttributes()
    {
        return array(
			'name' => AttributeType::String,
			'parent' => AttributeType::String,
			'code' => array(AttributeType::Handle, 'required' => true),
			'description'=> AttributeType::Mixed,
            'order' => array(AttributeType::Number, 'default'=>0),
            'enabled' => AttributeType::Bool,
        );
    }
	public function defineIndexes()
    {
        return array(
            array('columns' => array('code'), 'unique' => true),
        );
    }
}