<?php
namespace Craft;
require_once 'autoload.php';

use QuickCart\Cart;

class QuickCartPlugin extends BasePlugin
{
    public function getName()
    {
         return Craft::t('QuickCart');
    }
    public function getVersion()
    {
        return '0.1.11';
    }
    public function getDeveloper()
    {
        return 'DATAwebnet';
    }
    public function getDeveloperUrl()
    {
        return 'http://datawebnet.com.au/';
    }
	public function hasCpSection()
    {
        return false;
    }
    //Manually add CP Nav link (as we want to specify the index path and label rather than using the default hasCpSection
    public function modifyCpNav(&$nav)
    {
        if (craft()->userSession->isAdmin())
        {
            $nav['quickcart'] = array('label' => 'QuickCart', 'url' => 'quickcart/cp/index');
        }
    }
	protected function defineSettings()
    {
        return array(
            'notificationEmail' => array(AttributeType::Email, 'required' => true),
			'paymentModules' => array(AttributeType::Mixed, 'default'=>array('rows'=>
				array('PurchaseOrder',0,1),
				array('Paypal',0,1),
				array('Quote',0,1),
				array('Eway',0,0),
				array('Payway',0,0)
			)),
			'shippingModules' => array(AttributeType::Mixed, 'default'=>array('rows'=>
				array('AustraliaPost',0,0),
				array('PricePerItem',0,0),
				array('PricePerOrder',0,0),
				array('Custom',0,0)
			)),
            'pricesIncludeTax' => array(AttributeType::Bool, 'required' => true),
        );
    }

	public function getSettingsHtml()
    {
       return craft()->templates->render('quickcart/cp/settings', array(
           'settings' => $this->getSettings()
       ));
   }
    public function registerCpRoutes()
    {
        return array(
            'quickcart/products' => array('action' => 'quickCart/adminProduct/index'),
            'quickcart/products/(?P<productId>[^/]+)' => array('action' => 'quickCart/adminProduct/edit'),
            'quickcart/categories' => array('action' => 'quickCart/adminCategory/index'),
            'quickcart/categories/(?P<categoryId>[^/]+)' => array('action' => 'quickCart/adminCategory/edit'),
        );
    }
    public function registerSiteRoutes()
    {
        //dynamically make the quickcart routes from static vars
        $a_routes = array();
        foreach(Cart::getRoutes() as $key=>$a_route){
            $a_parts = array();
            foreach($a_route['urlPartVars'] as $part){
                $a_parts[]='(?P<'.$part['varName'].'>[^/]+)';
            }
            $s_routekey = $a_route['rootUrl'].implode('/',$a_parts);
            $a_routes[$s_routekey] = array('action'=>$a_route['action']);
        }
        return $a_routes;
        /*
        //Sample of some routes before generating them myself
        return array(
            'checkout/(?P<stepkey>[^/]+)' => array('action' => 'quickCart/checkout'),
            'product/(?P<productCode>[^/]+)' => array('action' => 'quickCart/product/show'),
            'category/(?P<categoryCode>[^/]+)' => array('action' => 'quickCart/category/show'),
        );
         [checkout/(?P<stepKey>[^/]+)] => quickCart/checkout
        */
    }
}
