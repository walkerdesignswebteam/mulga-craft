<?php
namespace Craft;

use QuickCart\Cart;
use QuickCart\CartItem;
use QuickCart\Categories;

class QuickCart_AdminCategoryController extends QuickCart_BaseController
{
    public function actionIndex(array $variables = array())
    {

        craft()->templates->includeJsResource('quickcart/cp/js/main.js');
       // craft()->templates->includeJs('new Craft.AccountSettingsForm('.JsonHelper::encode($variables['account']->id).', '.($variables['account']->isCurrent() ? 'true' : 'false').');');

        $message = craft()->request->getParam('message');
        $messageType = craft()->request->getParam('success');

        if($messageType==1) {
            craft()->userSession->setNotice(Craft::t($message));
        }elseif($messageType==0) {
            craft()->userSession->setError(Craft::t($message));
        }
        craft()->formBuilder_entries->getAllEntries();

        $this->renderTemplate('quickcart/cp/category/index',  array(
            'routes'=>Cart::getRoutes(),
            'categoriesTree'=>craft()->quickCart_categories->getTree()
        ));
    }

    public function actionEdit(array $variables = array())
    {
        if(isset($variables['category'])){
            //Category save has been attempted, however an error occurred, sent category object back to form as is to show errors.
            $variables['title'] = $variables['category']->name;
        }else {

            if ($variables['categoryId'] == 'new' || empty($variables['categoryId'])) {
                //New
                $o_category = new QuickCart_CategoryModel();
                $o_category->enabled = true; //enable be default
                $variables['title'] = Craft::t('Create a new category');
            } else {
                //Editing existing
                $o_category = craft()->quickCart_categories->getById($variables['categoryId']);
                if (!$o_category) {
                    throw new HttpException(404);
                }
                $variables['title'] = $o_category->name;
            }
            $variables['category'] = $o_category;
        }
        $variables['categoryTree']=craft()->quickCart_categories->getTreeSelectOptions();
        $this->renderTemplate('quickcart/cp/category/edit',$variables);
    }

    public function actionSave()
    {
        //quick alias for getPost
        function postvar($name){ return craft()->request->getPost($name); }

        $this->requirePostRequest();

        $id = postvar('categoryId');
        $s_originalCode=false;

        if($id){
            $o_category = craft()->quickCart_categories->getById($id);
            if (!$o_category) {
                throw new HttpException(404);
            }
            $s_originalCode = $o_category->code;
        }else{
            $o_category = new QuickCart_CategoryModel();
        }
        $a_setVars = array('name','code','parent','description','enabled','order');
        foreach($a_setVars as $var){
            $o_category->setAttribute($var,postvar($var));
        }

        $o_catWithNewParent = craft()->quickCart_categories->getByCode($o_category->parent);
        if(!empty($o_category->parent) &&(empty($o_catWithNewParent) || $o_catWithNewParent->id==$o_category->id)){
            craft()->userSession->setError(Craft::t('Could not save category.'));
            $o_category->addError('parent','Category selected for parent is not valid.');
        }else {


            if (craft()->quickCart_categories->saveCategory($o_category)) {
                $i_childUpdates = 0;
                if ($s_originalCode && $s_originalCode != $o_category->code) {
                    $o_categoriesWithThisParent = craft()->quickCart_categories->getAllByParent($s_originalCode);
                    if($o_categoriesWithThisParent) {
                        foreach ($o_categoriesWithThisParent as $o_catWithThisParent) {
                            if ($o_category->id != $o_catWithThisParent->id) {
                                $o_catWithThisParent->parent = $o_category->code;
                                craft()->quickCart_categories->saveCategory($o_catWithThisParent);
                                $i_childUpdates++;
                            }
                        }
                    }
                }
                craft()->userSession->setNotice(Craft::t('Category ' . $o_category->name . ' saved' . ($i_childUpdates > 0 ? ' (remapped ' . $i_childUpdates . ' categories to new code)' : '') . '.'));
                $this->redirectToPostedUrl();
            }
        }

        craft()->userSession->setError(Craft::t('Could not save category.'));
        craft()->urlManager->setRouteVariables(array(
            'category' => $o_category
        ));
    }

    public function actionDelete()
    {
        $this->requirePostRequest();
        $this->requireAjaxRequest();

        $id = craft()->request->getRequiredPost('id');
        $o_category = craft()->quickCart_categories->getById($id);
        $b_result = false;
        if($o_category){
            $b_result = craft()->quickCart_categories->deleteCategory($o_category);
        }
        $this->returnJson(array('success' =>$b_result));

    }
    public function actionReorder()
    {
        $this->requirePostRequest();
        $this->requireAjaxRequest();

        $a_ids = craft()->request->getRequiredPost('ids');
        $i_movedItemId = craft()->request->getRequiredPost('draggeeId');
        $o_categories = craft()->quickCart_categories->getFlatTreeCategories();
        $o_movedCat = false;

        //update all sort orders
        foreach($o_categories as $o_category){
            if($o_category->id==$i_movedItemId){
                //store reference to the moved category
                $o_movedCat = $o_category;
            }
            $newIndex = array_search($o_category->id,$a_ids);
            $o_category->order = $newIndex;
            craft()->quickCart_categories->saveCategory($o_category);
        }

        //get the category before the new position
        $o_catBeforeMoved = false;
        if($o_movedCat->order>0){
            $o_catBeforeMoved = $o_categories[$o_movedCat->order-1];
        }

        if($o_catBeforeMoved){
            //if not already a child of the
            if($o_movedCat->parent!=$o_catBeforeMoved->code && $o_catBeforeMoved->parent!=$o_movedCat->parent){
                $this->returnJson(array('success'=>0,'message'=>'Parent categories must be manually switched.'));
            }
        }
        $this->returnJson(array('success'=>1,'message'=>'Categories re-ordered.'));
    }
}