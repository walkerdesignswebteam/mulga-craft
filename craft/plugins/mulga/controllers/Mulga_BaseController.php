<?php
namespace Craft;
class Mulga_BaseController extends BaseController
{
    //Will look first for user templates, then default to plugin templates if not found
    public function getTemplateHtml($path,$vars=array()){
        $origTemplatePath = craft()->path->getTemplatesPath();
        $s_pluginfolder = 'mulga';

        $templateOverridePath = 'plugins/'.$s_pluginfolder.'/'.$path;
        if(craft()->templates->findTemplate($templateOverridePath)){
            //echo $origTemplatePath; exit();
            $html = craft()->templates->render($templateOverridePath,$vars);
        }else{
            $pluginTemplatePath = craft()->path->getPluginsPath() . $s_pluginfolder.'/templates/';
            craft()->path->setTemplatesPath($pluginTemplatePath);
            $html = craft()->templates->render($path,$vars);
        }
        //set back to what the original path was
        craft()->path->setTemplatesPath($origTemplatePath);
        return $html;
    }

}