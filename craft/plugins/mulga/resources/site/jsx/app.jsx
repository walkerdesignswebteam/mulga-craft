var BookingAppSettings = {
    depositPerParticipant:false, //set by app props
    modalContainerID:'modal-reactcontainer',
    creditcarddetails:{},
    clearModal:function(){
        ReactDOM.unmountComponentAtNode(document.getElementById(BookingAppSettings.modalContainerID));
    },
    getAccomPrice:function(accomInfo,priceLabel){
        var i;
        var thisOpt;
        for(i=0;i<accomInfo.priceOptions.length;i++){
            thisOpt = accomInfo.priceOptions[i];
            if(thisOpt.label==priceLabel){
                return thisOpt.price;
            }
        }
        return false;
    },
    participantIsSharing:function(participant){
        if(participant.sharedwith=='none'||participant.sharedwith==''){
            return false;
        }else{
            return true;
        }
    },

    getSharedParticipantLabel:function(participant,participants) {
        var j;
        for (j = 0; j < participants.length; j++) {
            thisInnerPart = participants[j];
            if (participant.id === thisInnerPart.sharedwith) {
                return participant.firstName+' & '+ thisInnerPart.firstName;
            }
        }
        return false;
    },
    scrollToTopError:function(){
        setTimeout(function(){
            var jumpEl = jQuery(".el-errors").first().parents(".formControlWrap");
            if(jumpEl.length>0) {
                jQuery("html,body").animate({scrollTop: jumpEl.offset().top});
            }
        },100);
    },
    scrollToTopOfSection:function(){
        setTimeout(function(){
            var jumpEl = jQuery(".steps .active").first();
            if(jumpEl.length>0) {
                //console.log(jQuery("html body").scrollTop()+' '+jumpEl.offset().top);
                //if(jumpEl.offset().top>jQuery("html body").scrollTop()) {
                    jQuery("html body").animate({scrollTop: jumpEl.offset().top});
                //}
            }
        },100);
    },
    outputJson:function(obj){
        console.log(JSON.stringify(obj));
    },
    getTourPrice:function(priceOptions,type){
        var i;
        for(i=0;i<priceOptions.length;i++){
            if(priceOptions[i].label===type){
                return priceOptions[i].price;
            }
        }
        return false;
    }
}

var BookingUI = React.createClass({
    screenDomID:'screenContent',
    slideSpeed:500,
    getInitialState: function() {
        BookingAppSettings.depositPerParticipant = this.props.bookingfee;

        var debugMode = false;
        if(debugMode===false){
            return {
                debugMode:false,
                loading:true,

                availabilityinfo:false,
                promotionInfo:false,
                participants:[],
                bookingDetails:{},
                termsDetails:{},

                activeMenuItem:'ScreenTerms',
                menuItems: [
                    {index:0,id:'ScreenTerms',title:'Terms and Conditions'},
                    {index:1,id:'ScreenDetails',title:'Order Details'},
                    {index:2,id:'ScreenParticipants',title:'Tour Guests'},
                    {index:3,id:'ScreenExtras',title:'Extras'},
                    {index:4,id:'ScreenPayment',title:'Summary and Payment'},
                ]
            };
        }else{
            //preload a heap of defaults so we are not always needing to refill everything
            return {
                debugMode:true,
                loading:true,

                availabilityinfo:false,
                promotionInfo:false,
                participants:[{"id":"d0e4-5dd2-f5b4-e527","changed":true,"valid":true,"sharedwith":"17cd-7940-a00c-2e83","firstName":"Marcus","lastName":"House","isDisabled":false,"accom":["PKXMZD"],"extras":["PRGXVZ"]},{"id":"17cd-7940-a00c-2e83","changed":true,"valid":true,"sharedwith":"d0e4-5dd2-f5b4-e527","firstName":"Linda","lastName":"House","isDisabled":false,"extras":["P8XLPW"]},{"id":"8524-4d2d-9bf7-a35d","changed":true,"valid":true,"sharedwith":"b02e-4924-75f0-613f","firstName":"Alex","lastName":"House","isDisabled":false,"accom":["P8ZPVW"],"extras":["PRGXVZ","P8XLPW"]},{"id":"b02e-4924-75f0-613f","changed":true,"valid":true,"sharedwith":"8524-4d2d-9bf7-a35d","firstName":"Lachlan","lastName":"House","isDisabled":false,"extras":[]},{"id":"c6f4-4761-686a-ed2f","changed":true,"valid":true,"sharedwith":"none","firstName":"Melanie","lastName":"Tyson","isDisabled":false,"accom":["P8ZPVW"],"extras":["P8XLPW"]}],
                bookingDetails:{"firstName":"Marcus","lastName":"House","email":"marcus@walkerdesigns.com.au","phone":"0438402887","address":"86 Mt Leslie Rd","city":"Prospect Vale","state":"Tasmania","postcode":"7250","country":"Australia","howhear":"From Google!\nOh, and from Mulga :)\n\nkind regards\n\nMarcus"},
                termsDetails:{termsagree:'Agreed',termsagreefuture:'Agree'},

                activeMenuItem:'ScreenTerms',
                menuItems: [
                    {index:0,id:'ScreenTerms',title:'Terms and Conditions'},
                    {index:1,id:'ScreenDetails',title:'Order Details'},
                    {index:2,id:'ScreenParticipants',title:'Tour Guests'},
                    {index:3,id:'ScreenExtras',title:'Extras'},
                    {index:4,id:'ScreenPayment',title:'Summary and Payment'}
                ]
            };
       }
    },
    componentDidMount:function(){
        var post_data = this.props;
        var _self = this;
        //if(this.state.debugMode===false) {
            this.setState({loading: true});

            $.ajax({
                type: "POST",
                url: '/tour/bookajax',
                data: post_data,
                success: function (data) {
                    _self.setState({
                        loading:false,
                        availabilityinfo:data,
                    });
                    _self.loadScreen(_self.state.activeMenuItem,false);
                }
            });
        //}else{
            //debug more has a bunch of default data
            //this.loadScreen(this.state.activeMenuItem);
        //}
    },
    refreshAccomAvailabilities:function(){
        var i,j;
        var thisAccom,thisPart,thisCnt;
        for(i=0;i<this.state.availabilityinfo.accom.length;i++){
            thisCnt=0;
            thisAccom = this.state.availabilityinfo.accom[i];
            for(j=0;j<this.state.participants.length;j++){
                thisPart = this.state.participants[j];
                if(thisPart.accom) {
                    if (thisPart.accom.indexOf(thisAccom.productCode) !== -1) {
                        thisCnt++;
                    }
                }
            }
            this.state.availabilityinfo.accom[i].seatsSelected = thisCnt;
        }
    },
    onUpdateParticipants:function(participants){
        this.setState({participants:participants});
    },
    onSaveParticipants:function(participants){
        this.onUpdateParticipants(participants);
        this.loadScreen('ScreenExtras');
    },
    onUpdateExtras:function(participants){
        this.setState({participants:participants});
        this.refreshAccomAvailabilities();
    },
    onSaveExtras:function(participants){
        this.onUpdateExtras(participants);
        this.loadScreen('ScreenPayment');
    },
    onUpdateBookingDetails:function(details){
        this.setState({bookingDetails:details});
    },
    onSaveBookingDetails:function(details){
        this.onUpdateBookingDetails(details);
        if(this.state.participants.length==0){
            this.setState({participants:[{id:WdTools.makeUniqueId(),changed:false,valid:false,sharedwith:'',firstName:details.firstName,lastName:details.lastName}]});
        }
        this.loadScreen('ScreenParticipants');
    },
    onUpdatePaymentDetails:function(details){
        BookingAppSettings.creditcarddetails = details;
    },
    verifyBookingDataStillValid:function(newLoadedData){
        this.forceUpdate();
    },
    onSavePaymentDetails:function(details){
        this.onUpdatePaymentDetails(details);

        //check that all allocations are ok
        //post_data = {participants:this.state.participants,cmd:'checkOkToPay'};
        var _thisRef = this;
        var post_data = this.props;
        $.ajax({
            type: "POST",
            url: '/tour/bookajax',
            data: post_data,
            success: function (data) {
                _thisRef.verifyBookingDataStillValid(data);
            }
        });
    },

    onUpdateTerms:function(details){
        this.setState({termsDetails:details});
    },
    onSaveTerms:function(details){
        this.onUpdateTerms(details);
        this.loadScreen('ScreenDetails');
    },
    onDiscountChange:function(promotionInfo){
        this.setState({promotionInfo:promotionInfo});
    },
    loadScreen:function(screenId,animateUser){
        $('#'+this.screenDomID).slideUp(this.slideSpeed,function(){
            switch (screenId) {
                case 'ScreenTerms':

                    this.refreshAccomAvailabilities();

                    reactScreen = <BookingUI.ScreenTerms
                        onDiscountChange={this.onDiscountChange} termsDetails={this.state.termsDetails}
                        onSave={this.onSaveTerms} appProps={this.props} onChange={this.onUpdateTerms}></BookingUI.ScreenTerms>;
                    break;
                case 'ScreenParticipants':
                    reactScreen = <BookingUI.ScreenParticipants
                        data={{participants:this.state.participants,availabilityinfo:this.state.availabilityinfo}}
                        onUpdateParticipants={this.onUpdateParticipants}
                        onSaveParticipants={this.onSaveParticipants}></BookingUI.ScreenParticipants>;
                    break;
                case 'ScreenExtras':
                    reactScreen = <BookingUI.ScreenExtras
                        data={{participants:this.state.participants,availabilityinfo:this.state.availabilityinfo}}
                        onUpdateExtras={this.onUpdateExtras} onSaveExtras={this.onSaveExtras}></BookingUI.ScreenExtras>;
                    break;
                case 'ScreenDetails':
                    reactScreen = <BookingUI.ScreenDetails
                        data={this.state.bookingDetails}
                        onChange={this.onUpdateBookingDetails} onSave={this.onSaveBookingDetails}></BookingUI.ScreenDetails>;
                    break;
                case 'ScreenPayment':
                    reactScreen = <BookingUI.ScreenPayment
                        data={BookingAppSettings.creditcarddetails} promotionInfo={this.state.promotionInfo} bookingCosts={this.getPrices()} appProps={this.props} appState={this.state}
                        onChange={this.onUpdatePaymentDetails} onSave={this.onSavePaymentDetails}></BookingUI.ScreenPayment>;
                    break;
                default:
                    reactScreen = <div>BookingUI.{screenId} was not found.</div>;
                    break;
            }

            ReactDOM.render(reactScreen, document.getElementById(this.screenDomID));
            this.setState({activeMenuItem: screenId});
            $('#'+this.screenDomID).slideDown(this.slideSpeed,function(){
                if (typeof(animateUser) === 'undefined' || animateUser!==false ) {
                    setTimeout( function(){BookingAppSettings.scrollToTopOfSection();},100);
                }
            });

        }.bind(this));
    },
    getScreenIndex:function(){
        var activeIndex = 0;
        for(var i=0;i<this.state.menuItems.length;i++){
            if(this.state.activeMenuItem==this.state.menuItems[i].id){
                activeIndex=i;
            }
        }
        return activeIndex;
    },
    menuItemClicked:function(menuItem){
        this.loadScreen(menuItem.id);
    },
    getAccomExtraInfoForParticipant:function(participant,participants){
        var i,j;
        var thisAccom;
        var a_ret = [];
        if(participant.accomExtras && participant.accomExtras.length>0){
            for(i=0;i<this.state.availabilityinfo.accomExtras.length;i++){
                thisAccom = this.state.availabilityinfo.accomExtras[i];
                for(j=0;j<participant.accomExtras.length;j++) {
                    if (thisAccom.productCode == participant.accomExtras[j]) {
                        var priceToGet = 'Single';
                        var name = participant.firstName;
                        if (BookingAppSettings.participantIsSharing(participant)) {
                            priceToGet = participant.doubletwin;
                            name = BookingAppSettings.getSharedParticipantLabel(participant, participants);
                        }
                        a_ret.push({
                            name: name,
                            product: thisAccom.product.name,
                            type: priceToGet,
                            price: BookingAppSettings.getAccomPrice(thisAccom, priceToGet)
                        });
                    }
                }
            }
        }
        return a_ret;
    },
    getAccomInfoForParticipant:function(participant,participants){
        var i,j;
        var thisAccom;
        if(participant.accom && participant.accom.length>0){
            for(i=0;i<this.state.availabilityinfo.accom.length;i++){
                thisAccom = this.state.availabilityinfo.accom[i];
                if(thisAccom.productCode==participant.accom[0]){
                    var priceToGet = 'Single';
                    var name = participant.firstName;
                    if(BookingAppSettings.participantIsSharing(participant)){
                        priceToGet=participant.doubletwin;
                        name = BookingAppSettings.getSharedParticipantLabel(participant,participants);
                    }
                    return {name:name,product:thisAccom.product.name,type:priceToGet,price:BookingAppSettings.getAccomPrice(thisAccom,priceToGet)}
                }
            }
        }
        return false;
    },
    getPrices:function(){
        if(this.state.availabilityinfo.tour){
            //Get tour total
            var a_subtotals = [];
            var keyID=0;
            var peopleOnTourSingle = 0;
            var peopleOnTourDouble = 0;

            var priceOfTourSingle = BookingAppSettings.getTourPrice(this.state.availabilityinfo.tour.priceOptions,'Single');
            var priceOfTourDouble = BookingAppSettings.getTourPrice(this.state.availabilityinfo.tour.priceOptions,'Double');
            for(i=0;i<this.state.participants.length;i++) {
                if (BookingAppSettings.participantIsSharing(this.state.participants[i]) === false) {
                    peopleOnTourSingle++;
                }else{
                    peopleOnTourDouble++;
                }
            }

            var totalTourPriceSingle = peopleOnTourSingle*priceOfTourSingle;
            var totalTourPriceDouble = peopleOnTourDouble*priceOfTourDouble;

            var total = totalTourPriceSingle+totalTourPriceDouble;
            if(this.state.participants.length>0) {
                a_subtotals.push({key: keyID++, subTitle: 'Tour'});
            }else{
                a_subtotals.push({key: keyID++, subTitle:'Awaiting selection of guests...'});
            }

            if(peopleOnTourDouble>0){
                a_subtotals.push({key:keyID++,label:peopleOnTourDouble+' guest'+(peopleOnTourDouble>1?'s':'')+' x '+WdTools.priceFormat(priceOfTourDouble)+' twin share',total:totalTourPriceDouble});
            }
            if(peopleOnTourSingle>0){
                a_subtotals.push({key:keyID++,label:peopleOnTourSingle+' guest'+(peopleOnTourSingle>1?'s':'')+' x '+WdTools.priceFormat(priceOfTourSingle)+' single',total:totalTourPriceSingle});
            }




            //Get accommodation entries
            var i,j;
            var thisAccomInfo;
            var pushedSub = false;
            var thisPrice;
            for(i=0;i<this.state.participants.length;i++){
                thisAccomInfo = this.getAccomInfoForParticipant(this.state.participants[i],this.state.participants);
                if(thisAccomInfo){
                    if(pushedSub===false){
                        a_subtotals.push({key:keyID++,subTitle:'Accommodation'});
                        pushedSub=true;
                    }
                    thisPrice = thisAccomInfo.price;
                    total+=thisPrice;
                    a_subtotals.push({key:keyID++,label:thisAccomInfo.name+' - '+thisAccomInfo.product+' - '+thisAccomInfo.type  ,total:thisPrice,info:thisAccomInfo});
                }
            }

            //Get accommodation extras
            //pushedSub = false;
            var thisLabel;
            var singlePrice;
            for(i=0;i<this.state.participants.length;i++){
                a_thisAccomInfo = this.getAccomExtraInfoForParticipant(this.state.participants[i],this.state.participants);
                for(j=0;j<a_thisAccomInfo.length;j++) {
                    if (a_thisAccomInfo[j]) {

                        thisAccomInfo = a_thisAccomInfo[j]
                        //if (pushedSub === false) {
                            //a_subtotals.push({key: keyID++, subTitle: 'Pre/Post Tour Accommodation'});
                            //pushedSub = true;
                        //}
                        console.log(thisAccomInfo);
                        thisPrice = thisAccomInfo.price;
                        total += thisPrice;
                        if(thisAccomInfo.type==='Twin' || thisAccomInfo.type==='Double'){
                            singlePrice = thisAccomInfo.price/2;
                            thisLabel = thisAccomInfo.name + ' - ' + thisAccomInfo.product + ' - 2 guests x '+WdTools.priceFormat(singlePrice);
                        }else{
                            thisLabel = thisAccomInfo.name + ' - ' + thisAccomInfo.product + ' - ' + thisAccomInfo.type;
                        }
                        a_subtotals.push({
                            key: keyID++,
                            label: thisLabel,
                            total: thisPrice,
                            info: thisAccomInfo
                        });
                    }
                }
            }

            //Get extras
            var thisExtra;
            var thisPart;
            var namesWithExtra;
            //pushedSub = false;
            for(i=0;i<this.state.availabilityinfo.extras.length;i++){
                thisExtra = this.state.availabilityinfo.extras[i];
                namesWithExtra=[];
                for(j=0;j<this.state.participants.length;j++){
                    thisPart = this.state.participants[j];
                    if(thisPart.extras) {
                        if (thisPart.extras.indexOf(thisExtra.productCode) !== -1) {
                            namesWithExtra.push(thisPart.firstName);
                        }
                    }
                }
                if(namesWithExtra.length>0) {
                    //if (pushedSub === false) {
                        //a_subtotals.push({key: keyID++, subTitle: 'Extras'});
                        //pushedSub = true;
                    //}
                    thisPrice = namesWithExtra.length*thisExtra.priceOptions[0].price;
                    total+=thisPrice;
                    a_subtotals.push({key:keyID++,label:namesWithExtra.length+' x '+thisExtra.product.name+' ('+namesWithExtra.join(', ')+')',total:thisPrice});
                }
            }

            return ({
                subtotals:a_subtotals,
                total:total
            });
        }
    },
    render:function(){
        var _state = this.state;
        var screenIndex = this.getScreenIndex();
        var menuItemsBefore = this.state.menuItems.map(function(menuItem) {
            if(menuItem.index<=screenIndex) {
                var activeClass = (this.state.activeMenuItem === menuItem.id ? 'active' : 'inactive');
                return (
                    <li key={menuItem.id} className={activeClass} onClick={this.menuItemClicked.bind(this,menuItem)}><a href="javascript:;">{menuItem.title}</a></li>
                );
            }
        }.bind(this));

        var menuItemsAfter = this.state.menuItems.map(function(menuItem) {
            if(menuItem.index>screenIndex) {
                return (
                    <li key={menuItem.id}><span>{menuItem.title}</span></li>
                );
            }
        }.bind(this));

        var prices = this.getPrices();
        var priceSummary;
        var priceSummaryPanel;
        if(prices) {
            priceSummary = prices.subtotals.map(function (price) {
                if(price.subTitle){
                    return (<li key={price.key} className="subTitle"><span><strong>{price.subTitle}</strong></span></li>);
                }else {
                    return (<li key={price.key}><span>{price.label}<br /><strong>{WdTools.priceFormat(price.total)}</strong></span></li>);
                }
            }.bind(this));

            var i;
            var priceElements = [];
            for(i=0;i<prices.length;i++){
                priceElements.push(<li>{prices[i].label}</li>);
            }
            var depositAmount = BookingAppSettings.depositPerParticipant*this.state.participants.length;
            if(this.state.participants.length==0){
                depositAmount = BookingAppSettings.depositPerParticipant;
            }

            var discount = 0;
            if(this.state.promotionInfo){
                if(this.state.promotionInfo.couponType.value==='percentage'){
                    if(prices.subtotals.length >1){
                        discount = prices.subtotals[1].total*(this.state.promotionInfo.couponValue/100);
                    }
                }else if(this.state.promotionInfo.couponType.value==='dollar' ) {
                    discount = this.state.promotionInfo.couponValue*this.state.participants.length;
                }
            }

            priceSummaryPanel =
            <div>
                <h2>Your Booking Information</h2>
                <ul className="list-unstyled priceSummary">{priceSummary}</ul>
                {prices.total>0?<div><h4>Total {WdTools.priceFormat(prices.total)} incl GST</h4></div>:null}
                {discount>0?<div><h4>Discount {WdTools.priceFormat(discount)} incl GST</h4><h3>Total After Discount<br />{WdTools.priceFormat(prices.total-discount)} incl GST</h3></div>:null}
                {prices.total>0?<div><hr /><h3>Booking fee<br />{WdTools.priceFormat(depositAmount)} incl GST</h3></div>:null}
            </div>
            ;
        }

        if(BookingAppSettings.processingPayment===true){
            menuItemsBefore = [];
            menuItemsAfter = [];
        }


        return(
            <div className="bookingUI">
                <div className="row" style={{display:(this.state.loading?'block':'none')}}>
                    <div className="col-sm-12">
                        <p>Please wait, loading tour information... <i className="fa fa-refresh fa-spin"></i></p>
                    </div>
                </div>

                <div className="row" style={{display:(this.state.loading?'none':'block')}}>
                    <div className="col-xs-12 col-sm-8">
                        <div className="row">
                            <div className="col-sm-12">
                                <ul className="steps before list-unstyled">{menuItemsBefore}</ul>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12"><div id={this.screenDomID} className="screenContent"></div></div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <ul className="steps after list-unstyled">{menuItemsAfter}</ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-4">{priceSummaryPanel}</div>
                </div>
                <div id={BookingAppSettings.modalContainerID}></div>
            </div>
        );
    }
});





BookingUI.ScreenParticipants = React.createClass({
     getInitialState: function() {
         var defaultParticipants = [
             {id:WdTools.makeUniqueId(),changed:false,valid:false,sharedwith:'',firstName:'',lastName:''},
         ];
        if(this.props.data.participants!==false){
            defaultParticipants = this.props.data.participants;
        }
        return {
            valid:false,
            participants:defaultParticipants
        };
    },
    addParticipant:function(){
        if(this.state.participants.length>=this.props.data.availabilityinfo.tour.seatsAvailable){
            ReactDOM.render(
                <WdUi.Modal title="No Tour Spots Remaining" wdOnClose={BookingAppSettings.clearModal}>
                    <div className="modal-body">Only <strong>{this.props.data.availabilityinfo.tour.seatsAvailable}</strong> seats remain on this tour. Please contact us if you wish to book a custom event or be reserved for a future tour.</div>
                    <div className="modal-footer">
                        <button onClick={BookingAppSettings.clearModal} type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </WdUi.Modal>,document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else {
            var newParticipants = this.state.participants;
            newParticipants.push({
                id: WdTools.makeUniqueId(),
                changed: false,
                valid: false,
                sharedwith:'',
                firstName: '',
                lastName: ''
            });
            this.setState({participants:newParticipants});
        }

    },
    removeParticipant:function(participantToRemove){
        var newParticipants = [];
        var i;
        var thisParticipant;
        for(i=0;i<this.state.participants.length;i++){
            thisParticipant = this.state.participants[i];
            if(participantToRemove.id!==thisParticipant.id){
                //killinkedreference - could be done in the refresh
                if(thisParticipant.sharedwith===participantToRemove.id){
                    thisParticipant.sharedwith='';
                }
                newParticipants.push(thisParticipant);
            }
        }
        this.setState({participants:newParticipants});
    },
    validateParticipants:function(forceErrorMessagesToShow){
        var i;
        var allValid = true;
        var updatedParticipants = this.state.participants;
        for(i=0;i<updatedParticipants.length;i++) {
            if(forceErrorMessagesToShow) {
                updatedParticipants[i].changed = true; //this will force error message to show in form
            }
            if (updatedParticipants[i].valid === false) {
                allValid = false;
            }
        }
        this.setState({participants:updatedParticipants,valid:allValid});
    },
    updateRoomLinksForParticipant:function(participant){
        var i;
        var updatedParticipants = this.state.participants;
        //link
        var crossLinkedId = false;
        for(i=0;i<updatedParticipants.length;i++){
            if(BookingAppSettings.participantIsSharing(participant)===false && updatedParticipants[i].sharedwith==participant.id){
                //if participant set to blank or none, blank out any other participant linked.
                updatedParticipants[i].sharedwith = '';
                updatedParticipants[i].doubletwin = '';
            }else if(crossLinkedId===false && participant.sharedwith==updatedParticipants[i].id) {
                //set the target participant to have reverse link back to the participant that just changed
                updatedParticipants[i].sharedwith = participant.id;
                updatedParticipants[i].doubletwin = participant.doubletwin;
                crossLinkedId = updatedParticipants[i].id;
            }else if(updatedParticipants[i].sharedwith==participant.id){
                //kill off any previous links to the participant id (otherwise multiple can be linked back to the one which causes worlds of trouble.
                updatedParticipants[i].sharedwith = '';
                updatedParticipants[i].doubletwin = '';
            }
        }
        this.setState({participants:updatedParticipants});
    },
    onChange:function(participant){
        var i;
        var updatedParticipants = this.state.participants;
        for(i=0;i<updatedParticipants.length;i++){
            if(participant.id==updatedParticipants[i].id){
                updatedParticipants[i] = participant;
                break;
            }
        }
        //link shared participants if needed
        this.setState({participants:updatedParticipants});
        this.updateRoomLinksForParticipant(participant);
        this.validateParticipants(false);
        this.props.onUpdateParticipants(this.state.participants);
    },

    nextStep:function(){
        this.validateParticipants(true);
        if(this.state.valid===false){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                <WdUi.Modal title="More detail required" wdOnClose={BookingAppSettings.clearModal}>
                    <div className="modal-body">Please enter all required details for each guest.</div>
                    <div className="modal-footer">
                        <button onClick={BookingAppSettings.clearModal} type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </WdUi.Modal>,document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.props.onSaveParticipants(this.state.participants);
        }
    },
    render:function(){
        return(
        <div>
            <h2>Tour Guests</h2>
            <p>Please enter the details for each guest below. To add more guests, press the <strong><em>&quot;Add Guest&quot;</em></strong> button.</p>
            <BookingUI.ParticipantFormList data={this.state.participants} onChange={this.onChange} onRemove={this.removeParticipant}></BookingUI.ParticipantFormList>
            <div className="row">
                <div className="col-xs-6"><a className="btn btn-success" onClick={this.addParticipant}><i className="fa fa-plus-circle"></i> Add Guest</a></div>
                <div className="col-xs-6 text-right"><div style={{display:'inline-block'}} onClick={this.nextStep}><a className="btn btn-primary" disabled={this.state.valid?"":"disabled"}>Continue</a></div></div>
            </div>
        </div>
        );
    }
});
BookingUI.ParticipantFormList = React.createClass({
    onChange: function(participant){
        this.props.onChange(participant);
    },
    onRemove: function(participant){
        this.props.onRemove(participant);
    },
    render:function(){
        var _thisref = this;

        var singleOnly = false;
        if(this.props.data.length==1){
            singleOnly=true;
        }
        var allparticipants = this.props.data;
        var content = this.props.data.map(function(participant) {
            var shareSelectOpts = [{value:'',label:'Please select'},{value:'none',label:'Single Room (don\'t share)'}];
            for(var i=0;i<allparticipants.length;i++) {
                var thisParticipant = allparticipants[i];
                //only provide share option if not self and not already assigned
                var showThisOption = false;
                //default - show if not already paired at all
                if(BookingAppSettings.participantIsSharing(thisParticipant)===false){
                    showThisOption=true;
                }
                //dont shot if self
                if(thisParticipant.id === participant.id){
                    showThisOption=false;
                }
                //show if already paired to it
                if(thisParticipant.sharedwith===participant.id){
                    showThisOption=true;
                }
                if (showThisOption) {
                    var label = 'Unnamed Guest '+(i+1);
                    if(thisParticipant.firstName){
                        label = 'Share with '+thisParticipant.firstName + ' ' + thisParticipant.lastName;
                    }
                    shareSelectOpts.push({
                        value: thisParticipant.id,
                        label: label
                    });
                }
            }
            return (
                <BookingUI.ParticipantForm key={participant.id} data={participant} shareOpts={shareSelectOpts} singleOnly={singleOnly} onRemove={_thisref.onRemove} onChange={_thisref.onChange} />
            );
        });
        return (
            <ul className="participantList list-unstyled">{content}</ul>
        );
    }
});
BookingUI.ParticipantForm = React.createClass({
    getInitialState: function() {
        return {
            participant:this.props.data,
            valid:false
        };
    },
    handleElementChange: function(el,newValue){
        var newParticipant = this.state.participant;
        newParticipant[el.props.dataKey] = newValue;
        this.setState({participant:newParticipant});
        this.props.onChange(this.state.participant);
    },
    handleFormChange: function(el){
        var newobj = this.state.participant;
        newobj.valid = el.state.valid;
        this.setState({participant:newobj});
        this.props.onChange(this.state.participant);
    },
    remove: function(){
        ReactDOM.render(
            <WdUi.Modal title="Delete Guest?" wdOnClose={BookingAppSettings.clearModal}>
                <div className="modal-body">Are you sure you want to delete this guest? Doing so will remove any associated information assigned to this guest.</div>
                <div className="modal-footer">
                    <button onClick={BookingAppSettings.clearModal} type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button onClick={this.removeConfirmed} data-dismiss="modal" className="btn btn-primary">Confirm Remove</button>
                </div>
            </WdUi.Modal>,document.getElementById(BookingAppSettings.modalContainerID)
        );
    },
    removeConfirmed:function(){
        BookingAppSettings.clearModal();
        var thisRef = this;
        //just so we can clear the modal before the participant is removed.
        setTimeout( function(){thisRef.props.onRemove(thisRef.state.participant)},600);
    },
    render:function(){
        var twinDoubleOptsOpts = [{value:'',label:'Please select'},{value:'Double',label:'Double'},{value:'Twin',label:'Twin (2 single beds)'}];
        var showDoubleTwinQ = false;
        if(BookingAppSettings.participantIsSharing(this.state.participant)){
            showDoubleTwinQ=true;
        }
        return(
            <li className="participant">
                <div className="row">
                    <div className="col-sm-12">
                        <div style={{float:'right'}}>{this.props.singleOnly?null:<a type="button" onClick={this.remove} className="btn btn-danger btn-sm"><i className="fa fa-minus-circle"></i> Remove {this.state.participant.firstName}</a>}</div>
                        <h3 className="no-margin"> {(this.state.participant.firstName?this.state.participant.firstName+'\'s Details':'Guest Details')}</h3>
                    </div>
                </div>
                <WdForm forceValidation={this.state.participant.changed} wdOnSubmit={this.submit} wdOnChange={this.handleFormChange}>
                    <div className="row">
                        <div className="col-sm-6"><WdForm.Input rules={{required:true}} type="text" label="First Name" value={this.state.participant.firstName} dataKey="firstName" wdOnChange={this.handleElementChange} /></div>
                        <div className="col-sm-6"><WdForm.Input rules={{required:true}} type="text" label="Last Name" value={this.state.participant.lastName} dataKey="lastName" wdOnChange={this.handleElementChange}  /></div>
                    </div>
                    <div className="row" style={{display:(this.props.singleOnly?'none':'block')}}>
                        <div className="col-sm-6"><WdForm.Select rules={{required:(this.props.singleOnly?false:true)}} options={this.props.shareOpts} label="Share accommodation with" value={this.state.participant.sharedwith} dataKey="sharedwith" wdOnChange={this.handleElementChange} /></div>
                        <div className="col-sm-6" style={{display:(showDoubleTwinQ?'block':'none')}}><WdForm.Select rules={{required:showDoubleTwinQ}} options={twinDoubleOptsOpts} label="Double/Twin" value={this.state.participant.doubletwin} dataKey="doubletwin" wdOnChange={this.handleElementChange} /></div>
                    </div>
                </WdForm>
            </li>
        );
    }
});

BookingUI.ScreenExtras = React.createClass({
    getInitialState: function() {
        return {
            valid:false,
            accom_errors:[],
            extras_errors:[],
            participants:this.props.data.participants
        };
    },
    setParticipantAccomExtraOption:function(participant,productCode,selected){
        //make accom array if it doesn't exist
        if(participant.accomExtras){
            //already exists
        }else{
            participant.accomExtras=[];
        }

        var newExtras = [];
        //strip any mention of this product code from the selected participant
        for (var i=0; i<participant.accomExtras.length; i++) {
            if(participant.accomExtras[i]!==productCode) {
                newExtras.push(participant.accomExtras[i]);
            }
        }
        //re-add if specified
        if(selected===true){
            newExtras.push(productCode);
        }

        participant.accomExtras = newExtras;
        return participant;
    },
    setParticipantExtraOption:function(participant,productCode,selected){
        //make accom array if it doesn't exist
        if(participant.extras){
            //already exists
        }else{
            participant.extras=[];
        }

        var newExtras = [];
        //strip any mention of this product code from the selected participant
        for (var i=0; i<participant.extras.length; i++) {
            if(participant.extras[i]!==productCode) {
                newExtras.push(participant.extras[i]);
            }
        }
        //re-add if specified
        if(selected===true){
            newExtras.push(productCode);
        }

        participant.extras = newExtras;
        return participant;
    },
    handleAccomChange: function(newStateInfo){
        var i;
        var thisOpt;
        for(i=0;i<this.state.participants.length;i++){
            thisOpt = newStateInfo.updatedOption;
            if(this.state.participants[i].id===thisOpt.key) {
                //only allow a single accom option to be selected per participant
                var newAccom = [];
                if(thisOpt.selected===true){
                    if(newStateInfo.availabilityinfo.seatsAvailable-newStateInfo.availabilityinfo.seatsSelected<=0){
                        BookingAppSettings.scrollToTopError();
                        ReactDOM.render(
                            <WdUi.Modal title="None Available" wdOnClose={BookingAppSettings.clearModal}>
                                <div className="modal-body">There are no more rooms of this type available. Please select an alternate accommodation option for {this.state.participants[i].firstName}.</div>
                                <div className="modal-footer">
                                    <button onClick={BookingAppSettings.clearModal} type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
                                </div>
                            </WdUi.Modal>,document.getElementById(BookingAppSettings.modalContainerID)
                        );
                        break;
                    }else {
                        newAccom = [newStateInfo.availabilityinfo.productCode];
                    }
                }
                this.state.participants[i].accom = newAccom;
            }
        }
        this.validateExtras();
        this.props.onUpdateExtras(this.state.participants);
    },
    handleAccomExtraChange: function(newStateInfo){
        var i;
        var thisOpt;
        for(i=0;i<this.state.participants.length;i++){
            thisOpt = newStateInfo.updatedOption;
            if(this.state.participants[i].id===thisOpt.key) {
                this.state.participants[i] = this.setParticipantAccomExtraOption(this.state.participants[i],newStateInfo.availabilityinfo.productCode,thisOpt.selected);
            }
        }
        this.validateExtras();
        this.props.onUpdateExtras(this.state.participants);
    },
    handleExtraChange: function(newStateInfo){
        var i;
        var thisOpt;
        for(i=0;i<this.state.participants.length;i++){
            thisOpt = newStateInfo.updatedOption;
            if(this.state.participants[i].id===thisOpt.key) {
                this.state.participants[i] = this.setParticipantExtraOption(this.state.participants[i],newStateInfo.availabilityinfo.productCode,thisOpt.selected);
            }
        }
        this.validateExtras();
        this.props.onUpdateExtras(this.state.participants);
    },
    validateExtras:function(){
        //make sure that all users have a room option
        var i,valid;
        var thisPart;
        var a_errors = [];
        var a_validPartIds=[];
        for(i=0;i<this.state.participants.length;i++){
            thisPart = this.state.participants[i];
            valid = false;
            if(thisPart.accom){
                if(thisPart.accom.length>0){
                    a_validPartIds.push(thisPart.id)
                    valid=true;
                }
            }
            //check if this participant is a linked one, if so, ignore
            if(a_validPartIds.indexOf(thisPart.sharedwith)!==-1) {
                valid=true;
            }

            if(valid===false){
                a_errors.push(thisPart.firstName);
            }
        }

        if(this.props.data.availabilityinfo.accom.length>0) {
            this.state.accom_errors = [];
            if (a_errors.length > 0) {
                this.state.accom_errors = [a_errors.join(', ') + ' has no accommodation selected.'];
            }
        }


        if(this.state.accom_errors.length>0){
           this.state.valid = false;
        }else{
            this.state.valid = true;
        }
        this.forceUpdate();
    },
    nextStep:function(){
        this.validateExtras();
        if(this.state.accom_errors.length>0){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                <WdUi.Modal title="Accommodation Information Needed" wdOnClose={BookingAppSettings.clearModal}>
                    <div className="modal-body">Please select an accommodation option for each guest.</div>
                    <div className="modal-footer">
                        <button onClick={BookingAppSettings.clearModal} type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </WdUi.Modal>,document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.props.onSaveExtras(this.state.participants);
        }
    },
    componentDidMount:function(){
        this.validateExtras();
    },
    render:function(){
        var i;
        var cnt=0;
        var accomErrors = this.state.accom_errors.map(function(error) {
            cnt++;
            return (<div key={cnt}>{error}</div>);
        });

        var accomUI = null;
        if(this.props.data.availabilityinfo.accom.length>0){
            accomUI =
                <div className="formControlWrap">
                    <BookingUI.ExtraOptionsList onChange={this.handleAccomChange} title="Accommodation Options" description="Please select a single accommodation option for each guest for the tour (one selection required per single or double)." data={{
                            extratype:'accom',
                            participants:this.state.participants,
                            availabilityinfo:this.props.data.availabilityinfo.accom}
                        } />
                    {this.state.accom_errors.length>0?<div className="el-errors">{accomErrors}</div>:null}
                </div>
        }

        var accomExtrasUI = null;
        if(this.props.data.availabilityinfo.accomExtras.length>0){
            accomExtrasUI =
                <div className="formControlWrap">
                    <BookingUI.ExtraOptionsList onChange={this.handleAccomExtraChange} title="" description="Please select any extras required for each guest." data={{
                        extratype:'accomExtras',
                        participants:this.state.participants,
                        availabilityinfo:this.props.data.availabilityinfo.accomExtras}
                    } />
                </div>
        }

        var extrasUI = null;
        if(this.props.data.availabilityinfo.extras.length>0){
            extrasUI =
                <div className="formControlWrap">
                    <BookingUI.ExtraOptionsList onChange={this.handleExtraChange} title="" description="" data={{
                        extratype:'extra',
                        participants:this.state.participants,
                        availabilityinfo:this.props.data.availabilityinfo.extras
                    }} />
                </div>
        }

        return(
            <div>
                <h2>Tour Extras</h2>
                {accomUI}
                {accomExtrasUI}
                <hr />
                {extrasUI}
                <div className="col-xs-12 text-right"><div style={{display:'inline-block'}} onClick={this.nextStep}><a className="btn btn-primary" disabled={this.state.valid?"":"disabled"}>Continue</a></div></div>
            </div>
        );
    }
});
BookingUI.ExtraOptionsList = React.createClass({
    onChange:function(switchState){
        this.props.onChange(switchState);
    },
    render:function(){
        var _thisref = this;
        var content = this.props.data.availabilityinfo.map(function(option) {
            var participants = _thisref.props.data.participants;
            return (
                <BookingUI.ExtraOption key={option.id} data={{
                    extratype:_thisref.props.data.extratype,
                    participants:participants,
                    availabilityinfo:option
                }} onChange={_thisref.onChange} />
            );
        });
        return(
            <div>
                <h3>{this.props.title}</h3>
                <p>{this.props.description}</p>
                <ul className="participantList list-unstyled">{content}</ul>
            </div>
        );
    }
});
BookingUI.ExtraOption = React.createClass({
    onChange:function(switchState){
        this.props.onChange({availabilityinfo:this.props.data.availabilityinfo,updatedOption:switchState});
    },
    isAccomOptSelected:function(participant){
        if(participant.accom && participant.accom.indexOf(this.props.data.availabilityinfo.productCode)!==-1) {
            return true;
        }
        return false;
    },
    isAccomExtraOptSelected:function(participant){
        if(participant.accomExtras && participant.accomExtras.indexOf(this.props.data.availabilityinfo.productCode)!==-1) {
            return true;
        }
        return false;
    },
    isExtraOptSelected:function(participant){
        if(participant.extras && participant.extras.indexOf(this.props.data.availabilityinfo.productCode)!==-1) {
            return true;
        }
        return false;
    },
    render:function(){
        var switcherGroupData = [];
        var thisPart;
        var i,j;
        var shouldAdd;
        var thisPrice;
        if(this.props.data.extratype==='extra') {
            for (i = 0; i < this.props.data.participants.length; i++) {
                thisPart = this.props.data.participants[i];
                switcherGroupData.push({key:thisPart.id,value: thisPart.id, label: thisPart.firstName, isDisabled:thisPart.isDisabled, selected:this.isExtraOptSelected(thisPart)});
            }
        }else if(this.props.data.extratype==='accom' || this.props.data.extratype==='accomExtras'){
            for (i = 0; i < this.props.data.participants.length; i++) {
                thisPart = this.props.data.participants[i];
                if(this.props.data.extratype==='accomExtras') {
                    var b_isSelected = this.isAccomExtraOptSelected(thisPart);
                }else{
                    var b_isSelected = this.isAccomOptSelected(thisPart);
                }
                if(BookingAppSettings.participantIsSharing(thisPart)===false){
                    switcherGroupData.push({key:thisPart.id, value: thisPart.id, label: thisPart.firstName, isDisabled:thisPart.isDisabled, selected:b_isSelected});
                }else{
                    //make sure reversed shared user does not get added
                    shouldAdd=true;
                    for(j=0;j<switcherGroupData.length;j++){
                        if(switcherGroupData[j].value===thisPart.sharedwith){
                            shouldAdd = false;
                        }
                    }
                    if(shouldAdd) {
                        var joinedUserLabel = BookingAppSettings.getSharedParticipantLabel(thisPart,this.props.data.participants);
                        switcherGroupData.push({key:thisPart.id, value: thisPart.id, label: joinedUserLabel, isDisabled:thisPart.isDisabled, selected:b_isSelected});
                    }
                }
            }
        }


        var b_combineTwinDouble=false;
        var n_twinPrice=false;
        var n_doublePrice=false;
        for (i = 0; i<this.props.data.availabilityinfo.priceOptions.length; i++){
            thisPrice = this.props.data.availabilityinfo.priceOptions[i];
            if(thisPrice.label=='Twin'){
                n_twinPrice = thisPrice.price;
            }else if(thisPrice.label=='Double'){
                n_doublePrice = thisPrice.price;
            }
        }
        if(n_twinPrice && n_twinPrice===n_doublePrice){
            b_combineTwinDouble=true;
        }
        var a_priceStrChunks = [];
        for (i = 0; i<this.props.data.availabilityinfo.priceOptions.length; i++){
            thisPrice = this.props.data.availabilityinfo.priceOptions[i];
            if(b_combineTwinDouble && thisPrice.label==='Double'){
                continue; //skip if we are combining twin/double.
            }
            if(b_combineTwinDouble && thisPrice.label==='Twin') {
                a_priceStrChunks.push(WdTools.priceFormat(thisPrice.price/2) + ' Double/Twin pp');
            }else{
                a_priceStrChunks.push(WdTools.priceFormat(thisPrice.price) + ' ' + thisPrice.label);
            }
        }
        var priceStr = a_priceStrChunks.join(', ');


        var availabilityMsg = null;
        if(this.props.data.extratype==='accom'){
            var accomLeft = this.props.data.availabilityinfo.seatsAvailable-this.props.data.availabilityinfo.seatsSelected;
            availabilityMsg = <em>({accomLeft} available)</em>;
        }
        return(
            <li className="extra-opt">
                <div className="extra-name">{this.props.data.availabilityinfo.product.name} {availabilityMsg}</div>
                <div className="extra-price">{priceStr}</div>
                <div className="extra-desc">{this.props.data.availabilityinfo.product.shortDescription}</div>
                <WdForm.SwitcherGroup data={switcherGroupData} onChange={this.onChange} />
            </li>
        );
    }
});

BookingUI.ScreenDetails = React.createClass({
    getInitialState: function() {
        return {
            valid:false,
            forceValidate:false,
            details:this.props.data
        };
    },

    handleElementChange: function(el,newValue){
        var details = this.state.details;
        details[el.props.dataKey] = newValue;
        this.state.details = details;
        this.props.onChange(this.state.details);
    },
    handleFormChange: function(el){
        this.setState({valid:el.state.valid});
    },
    nextStep:function(){
        this.setState({forceValidate:true});
        if(this.state.valid===false){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                <WdUi.Modal title="More detail required" wdOnClose={BookingAppSettings.clearModal}>
                    <div className="modal-body">Please enter all required booking details.</div>
                    <div className="modal-footer">
                        <button onClick={BookingAppSettings.clearModal} type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </WdUi.Modal>,document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.props.onSave(this.state.details);
        }
    },
    render:function(){
        return(
            <div>
                <h2>Contact Details For This Booking</h2>
                <p>Please enter the contact details for the person making this booking. If you are making a booking for more than one person you will be asked to add their names on the next screen.</p>
                <WdForm forceValidation={this.state.forceValidate} wdOnSubmit={this.nextStep} wdOnChange={this.handleFormChange}>
                    <div className="row">
                        <div className="col-sm-12"><h3>Contact Information</h3></div>
                        <div className="col-sm-6"><WdForm.Input rules={{required:true}} type="text" label="First Name" value={this.state.details.firstName} dataKey="firstName" wdOnChange={this.handleElementChange} /></div>
                        <div className="col-sm-6"><WdForm.Input rules={{required:true}} type="text" label="Last Name" value={this.state.details.lastName} dataKey="lastName" wdOnChange={this.handleElementChange}  /></div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6"><WdForm.Input rules={{required:true,email:true}} type="text" label="Email" value={this.state.details.email} dataKey="email" wdOnChange={this.handleElementChange} /></div>
                        <div className="col-sm-6"><WdForm.Input rules={{required:true}} type="text" label="Preferred Phone Number" value={this.state.details.phone} dataKey="phone" wdOnChange={this.handleElementChange}  /></div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12"><hr /><h3>Address</h3></div>
                        <div className="col-sm-6"><WdForm.Input rules={{required:true}} type="text" label="Address" value={this.state.details.address} dataKey="address" wdOnChange={this.handleElementChange} /></div>
                        <div className="col-sm-6"><WdForm.Input rules={{required:true}} type="text" label="City" value={this.state.details.city} dataKey="city" wdOnChange={this.handleElementChange}  /></div>
                    </div>
                    <div className="row">
                        <div className="col-sm-4"><WdForm.Input rules={{required:true}} type="text" label="State" value={this.state.details.state} dataKey="state" wdOnChange={this.handleElementChange} /></div>
                        <div className="col-sm-4"><WdForm.Input rules={{required:true}} type="text" label="Postcode" value={this.state.details.postcode} dataKey="postcode" wdOnChange={this.handleElementChange}  /></div>
                        <div className="col-sm-4"><WdForm.Input rules={{required:true}} type="text" label="Country" value={this.state.details.country} dataKey="country" wdOnChange={this.handleElementChange} /></div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <hr />
                            <p>Please let us know how you found out about Mulga Bicycle Tours and this tour specifically.</p>
                            <WdForm.TextArea type="text" showLabel={false} label="How did you hear about this tour?" value={this.state.details.howhear} dataKey="howhear" wdOnChange={this.handleElementChange} /></div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12 text-right"><div style={{display:'inline-block'}} onClick={this.nextStep}><a className="btn btn-primary" disabled={this.state.valid?"":"disabled"}>Continue</a></div></div>
                    </div>
                </WdForm>

            </div>
        );
    }
});

BookingUI.ScreenPayment = React.createClass({
    getInitialState: function() {
        //set deposit amount
        var depositAmount = BookingAppSettings.depositPerParticipant*this.props.appState.participants.length;
        var currDetails = this.props.data;
        currDetails.depositamount = depositAmount;
        currDetails.paymenttype = '';
        return {
            valid:false,
            forceValidate:false,
            details:currDetails,

            paymentProcessing:false,
            paymentTitle:false,
            paymentSubTitle:false,
            paymentFinished:false,
            paymentFinishedError:false,
            appState:this.props.appState,
            participants:this.props.appState.participants,
            bookingCosts:this.props.bookingCosts
        };
    },

    handleElementChange: function(el,newValue){
        var details = this.state.details;
        details[el.props.dataKey] = newValue;
        this.state.details = details;
        this.props.onChange(this.state.details);
    },
    handlePaymentTypeChange: function(el,newValue){
        this.state.details.paymenttype=newValue;
        this.forceUpdate();
    },
    handleFormChange: function(el){
        this.setState({valid:el.state.valid});
    },
    payDataCheck:function(data){
        var b_fail = false;
        if(this.state.participants.length>data.tour.seatsAvailable){
            this.setState({
                paymentTitle:'Required Tour Places Unavailable',
                paymentSubTitle:'You have attempted to book '+this.state.participants.length+' guests on the tour. '+data.tour.seatsAvailable+' places are available. Please try your booking again.'
            });
            b_fail=true;
        }else{
            //check accommodation
            var i,j;
            var thisPart;
            var thisAccom;
            var cnt;
            for(i=0;i<data.accom.length;i++){
                thisAccom = data.accom[i];
                cnt=0;
                for(j=0;j<this.state.participants.length;j++){
                    thisPart = this.state.participants[j];
                    if(thisPart.accom){
                        if(thisPart.accom.indexOf(thisAccom.productCode)){
                            cnt++;
                        }
                    }
                }
                if(cnt>thisAccom.seatsAvailable){
                    this.setState({
                        paymentTitle:'Required Accommodation Places Unavailable',
                        paymentSubTitle:'You have attempted to book '+cnt+' guests in '+thisAccom.product.name+'. Only '+thisAccom.seatsAvailable+' places are now available. Please modify the number of guests and try your booking again.',
                        paymentWaiting:false
                    });
                    b_fail=true;
                    break;
                }
            }
        }
        if(b_fail===false){
            this.ajaxCompletePay();
        }
    },
    completePayment:function(result){
        var success = false;
        if(result.paymentInfo){
            if(result.paymentInfo.status==='paid'){
                success = true;
            }
        }
        //hide navigation so user cannot go and mess with the same order.
        if(success){
            $('.steps.before.list-unstyled').hide();
            this.setState({
                paymentWaiting:false,
                paymentTitle:'Payment Complete',
                paymentSubTitle:'Transaction Number: '+result.paymentInfo.transactionNumber+' with reference '+result.paymentInfo.transactionRef+' completed successfully. You will receive an email confirmation at '+this.state.appState.bookingDetails.email+' containing your order and payment information.',
                paymentFinished:true,
                paymentFinishedError:false
            });
        }else{
            if(result.paymentInfo.status==='error'){
                this.setState({
                    paymentWaiting: false,
                    paymentTitle: 'Payment Error',
                    paymentSubTitle: result.paymentInfo.error.join(', '),
                    paymentFinished: true,
                    paymentFinishedError: true,
                });
            }else {
                $('.steps.before.list-unstyled').hide();
                this.setState({
                    paymentWaiting: false,
                    paymentTitle: 'Payment Error',
                    paymentSubTitle: 'Your payment or booking failed for an unknown reason. Please contact us to check the status of your booking/payment.',
                    paymentFinished: true,
                    paymentFinishedError: true,
                });
            }
        }
    },
    ajaxCompletePay:function(){

        this.setState({
            paymentTitle:'Booking Verified',
            paymentSubTitle:'Please wait, your payment is now being processed... '
        });
        var _thisRef = this;
        var dataToSend = {
            tour:this.props.appProps,
            cc_details:this.state.details,
            participants:this.state.appState.participants,
            bookingDetails:this.state.appState.bookingDetails,
            bookingCosts:this.state.bookingCosts,
            promotionCode:this.props.promotionInfo.couponCode,
            availabilityinfo:this.state.appState.availabilityinfo
        };

        $.ajax({
            type: "POST",
            url: '/tour/pay',
            data: dataToSend,
            success: function (result) {
                _thisRef.completePayment(result);
            }
        });
    },
    pay:function(){
        this.setState({forceValidate:true});
        if(this.state.details.paymenttype=='creditcard' && this.state.valid===false){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                <WdUi.Modal title="More detail required" wdOnClose={BookingAppSettings.clearModal}>
                    <div className="modal-body">Please enter all required payment details.</div>
                    <div className="modal-footer">
                        <button onClick={BookingAppSettings.clearModal} type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </WdUi.Modal>,document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.setState({
                paymentProcessing:true,
                paymentWaiting:true,
                paymentTitle:'Checking Booking Details',
                paymentSubTitle:'Please wait while we verify your booking... '
            });
            var _thisRef = this;
            $.ajax({
                type: "POST",
                url: '/tour/bookajax',
                data: this.props.appProps,
                success: function (data) {
                    _thisRef.payDataCheck(data);
                }
            });
            //this.props.onSave(this.state.details);
        }
    },
    popupCvnModal:function(){
        ReactDOM.render(
            <WdUi.Modal title="What is CVN (CVV2/CVC2)" wdOnClose={BookingAppSettings.clearModal}>
                <div className="modal-body">
                    <p>The CVN code is a special 3-4 digit code used as a security measure for all Internet transactions. It is called CVV2 (Credit card Verification Value) for Visa cards and CVC2 (Credit card Verification Code) for MasterCard.</p>
                    <p>Since the CVN is listed on your credit card, but is not stored anywhere, the only way to know the correct number for your credit card is to physically have possession of the card itself. Because of this, it is a security enhancement against misuse of credit cards. All VISA, MasterCard and American Express cards newer than 5 years have a CVN.</p>
                    <p><strong>Note:</strong> The CVN code is printed on your card; it is not raised from the surface of the card like your credit card's account number.</p>
                </div>
                <div className="modal-footer">
                    <button onClick={BookingAppSettings.clearModal} type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </WdUi.Modal>,document.getElementById(BookingAppSettings.modalContainerID)
        );
    },
    tryPaymentAgain:function(){
        this.setState({paymentProcessing:false});
    },
    render:function(){
        var i;
        var creditCardOpts = [
            {value:'',label:'Please select...'},
            {value:'Visa',label:'Visa'},
            {value:'Mastercard',label:'Mastercard'}
        ];
        var monthCardOpts = [
            {value:'',label:'Select Month...'},
            {value:'01',label:'01'},
            {value:'02',label:'02'},
            {value:'03',label:'03'},
            {value:'04',label:'04'},
            {value:'05',label:'05'},
            {value:'06',label:'06'},
            {value:'07',label:'07'},
            {value:'08',label:'08'},
            {value:'09',label:'09'},
            {value:'10',label:'10'},
            {value:'11',label:'11'},
            {value:'12',label:'12'}
        ];

        var yearsToShow = 20;
        var currYear = new Date().getFullYear();
        var yearCardOpts = [
            {value:'',label:'Select Year...'},
        ];
        for(i=0;i<yearsToShow;i++){
            thisYear = currYear+i;
            yearCardOpts.push({value:thisYear-2000,label:thisYear});
        }

        var endNavigation=null;
        if(this.state.paymentFinishedError===true) {
            if($('.steps.before.list-unstyled').is(":visible")){
                endNavigation = <a href="javascript:;" onClick={this.tryPaymentAgain} className="btn btn-primary">Try Payment Again</a>;
            }else {
                endNavigation = <a href="/contact" className="btn btn-primary">An Error Occurred - Please Contact Us</a>;
            }
        }else if(this.state.paymentFinished===true){
            endNavigation = <a href="/tours" className="btn btn-primary">Back to Tours</a>;
        }

        return(
            <div>
                <div style={{display:this.state.paymentProcessing?'block':'none'}}>
                    <h2>{this.state.paymentTitle}</h2>
                    <p>{this.state.paymentSubTitle} {this.state.paymentWaiting?<i className="fa fa-refresh fa-spin"></i>:null}</p>
                    {endNavigation}
                </div>

                <div style={{display:this.state.paymentProcessing?'none':'block'}}>
                    <h2>Payment Details</h2>
                    <div className="paymentAmount">
                        <div className="main">Booking fee: <span>{WdTools.priceFormat(this.state.details.depositamount)} incl GST</span></div>
                        <div className="subline">{WdTools.priceFormat(BookingAppSettings.depositPerParticipant)} per guest</div>
                    </div>
                    <hr/>
                    <div className="row">
                        <div className="col-xs-12">
                            <h3>Payment Method</h3>
                            <WdForm.Select rules={{required:true}} options={[
                                {value:'',label:'Please select...'},
                                {value:'creditcard',label:'Credit Card'},
                                {value:'banktransfer',label:'Bank Transfer'}
                            ]} label="Please select your method of payment" value={this.state.details.paymenttype} dataKey="cardtype" wdOnChange={this.handlePaymentTypeChange} />
                        </div>
                        <div style={{display:this.state.details.paymenttype==''?'block':'none'}} className="col-xs-12 text-right"><div style={{display:'inline-block'}}><a className="btn btn-primary" disabled="disabled">Book Now</a></div></div>
                    </div>
                    <div style={{display:this.state.details.paymenttype=='creditcard'?'block':'none'}}>
                        <div id="eWAYBlock" style={{float:'right', margin:'0px 0px 10px 10px'}}>
                            <a href="http://www.eway.com.au/secure-site-seal?i=11&s=3&pid=5ff76b25-0692-4ddf-8186-f852ca7b9b5e" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
                                <img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&size=3&pid=5ff76b25-0692-4ddf-8186-f852ca7b9b5e" />
                            </a>
                        </div>
                        <h3>Pay by Credit Card</h3>
                        <p>Please enter your credit card information below to immediately make payment.</p>
                        <p>Your credit card details are immediately encrypted and processed directly by eWAY. At no point does this site store your card details.</p>

                        <WdForm forceValidation={this.state.forceValidate} wdOnSubmit={this.nextStep} wdOnChange={this.handleFormChange}>
                            <div className="row">
                                <div className="col-sm-6"><WdForm.Select rules={{required:true}} options={creditCardOpts} label="Card Type" value={this.state.details.cardtype} dataKey="cardtype" wdOnChange={this.handleElementChange} /></div>
                                <div className="col-sm-6"><WdForm.Input rules={{required:true}} type="text" label="Card Name" value={this.state.details.cardname} dataKey="cardname" wdOnChange={this.handleElementChange}  /></div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6"><WdForm.Input rules={{required:true,isNumeric:true,minLength:14,maxLength:16}} type="text" label="Card Number" value={this.state.details.cardnumber} dataKey="cardnumber" wdOnChange={this.handleElementChange}  /></div>
                                <div className="col-sm-6"><WdForm.Input rules={{required:true,isNumeric:true,minLength:3,maxLength:4}} type="text" label="CVN" value={this.state.details.cvn} dataKey="cvn" wdOnChange={this.handleElementChange}  /><a href="javascript:;" onClick={this.popupCvnModal} >What is a CVN?</a></div>

                            </div>
                            <div className="row">
                                <div className="col-sm-6"><WdForm.Select rules={{required:true}} options={monthCardOpts} label="Expiry Month" value={this.state.details.expirymonth} dataKey="expirymonth" wdOnChange={this.handleElementChange} /></div>
                                <div className="col-sm-6"><WdForm.Select rules={{required:true}} options={yearCardOpts} label="Expiry Year" value={this.state.details.expiryyear} dataKey="expiryyear" wdOnChange={this.handleElementChange} /></div>
                            </div>

                            <div className="row">
                                <div className="col-xs-12 text-right"><div style={{display:'inline-block'}} onClick={this.pay}><a className="btn btn-primary" disabled={this.state.valid?"":"disabled"}>Book and Pay Now via Credit Card</a></div></div>
                            </div>
                        </WdForm>
                    </div>

                    <div style={{display:this.state.details.paymenttype=='banktransfer'?'block':'none'}}>
                        <h3>Pay by Bank Transfer</h3>
                        <p>Please transfer the required booking fee of <strong>{WdTools.priceFormat(this.state.details.depositamount)}</strong> to the bank account with details shown below.</p>
                        <p>If payment is not received within <strong>72 hours</strong>, your booking may be cancelled.</p>
                        <p><strong>Reference:</strong><br /><span>When making your bank transfer payment, please use <strong>&quot;<em>{this.state.appState.bookingDetails.lastName}</em>&quot;</strong> as your reference.</span></p>
                        <div className="row">
                            <div className="col-sm-6"><strong>Bank Name:</strong></div>
                            <div className="col-sm-6">Commonweatlth Bank</div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6"><strong>BSB:</strong></div>
                            <div className="col-sm-6">062907</div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6"><strong>Account Number:</strong></div>
                            <div className="col-sm-6">10352850</div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6"><strong>Account Name:</strong></div>
                            <div className="col-sm-6">Multilocus Interactive Pty Ltd trading as Mulga Bicycle Tours</div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12 text-right"><div style={{display:'inline-block'}} onClick={this.pay}><a className="btn btn-primary">Book Now</a></div></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

BookingUI.ScreenTerms = React.createClass({
    /*
     <h2>Insurance</h2>
     <p>We recommend that you purchase travel insurance that provides coverage for the cancellation of the tour, and covers costs incurred by you like transportation costs (for example, airfares), stolen goods, lost luggage, damaged property, medical expenses, and cancellation fees.</p>
     <p>We recommend that you hold current ambulance service membership and/or health or travel insurance. If you become ill or injured during the tour, we will endeavour to organise medical transportation for you but at your cost.</p>
     <p>We also strongly recommend you have Cyclist Insurance. This can be purchased separately and also comes with some cycling club memberships such as Bicycle Network, Bicycle NSW, Bicycle Queensland and Pedal Power (ACT). Cyclist insurance can cover you and your bike if you have an accident while riding.</p>
     */
    getInitialState: function() {
        return {
            couponExtended:false,
            valid:false,
            forceValidate:false,

            gettingDiscount:false,
            discountError:true,
            discountMessage:true,

            details:this.props.termsDetails
        };
    },
    handleElementChange: function(el,newValue){
        var details = this.state.details;
        details[el.props.dataKey] = newValue;
        this.state.details = details;
        this.props.onChange(this.state.details);
    },
    handleFormChange: function(el){
        this.setState({valid:el.state.valid});
    },
    nextStep:function(){
        this.setState({forceValidate:true});
        if(this.state.valid===false){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                <WdUi.Modal title="Agreement Needed" wdOnClose={BookingAppSettings.clearModal}>
                    <div className="modal-body">Please agree to all terms and conditions.</div>
                    <div className="modal-footer">
                        <button onClick={BookingAppSettings.clearModal} type="button" className="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </WdUi.Modal>,document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.props.onSave(this.state.details);
        }
    },
    handlePromotionCodeValChange:function(el,newValue){
        var details = this.state.details;
        details[el.props.dataKey] = newValue;
        this.setState({details: details});
    },
    handlePromotionCodeChange:function(){
        this.state.gettingDiscount = true;
        this.setState({gettingDiscount: true});

        var _thisRef = this;
        $.ajax({
            type: "POST",
            url: '/tour/getpromotioninfoajax',
            data: {tourId:this.props.appProps.tourid,tourStarttime:this.props.appProps.starttime,promotionCode:this.state.details.promotionCode},
            success: function (data) {
                var details = _thisRef.state.details;
                var showError = false;
                if(data.promotionInfo===false){
                    showError = true;
                }
                _thisRef.setState({gettingDiscount: false,discountError:showError,discountMessage:data.message});
                _thisRef.props.onDiscountChange(data.promotionInfo);
            }
        });
    },
    extendCoupon:function(){
        this.state.couponExtended=true;
        this.setState({couponExtended: true});
    },
    render:function(){
        //discount message
        discountMessage = null;
        if(this.state.discountMessage!==false){
            if(this.state.discountError===true){
                discountMessage = <p style={{fontSize:'90%', color:'#ff0000'}}>{this.state.discountMessage}</p>
            }else{
                discountMessage = <p style={{fontSize:'90%',color:'#449d44'}}>{this.state.discountMessage}</p>
            }
        }

        return(
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <h2>Terms and Conditions</h2>
                        <p>Here's the formal part of the booking process. We need to bring this to your attention first so we can then get on with organising the fun stuff! It includes links to documents that you will need to complete. You do not have to do this now, we will send these documents to you in our email confirming that we have received your booking request.</p>
                        <p>Acceptance of your booking is subject to Mulga Bicycle Tours receiving a signed <a target="_blank" href={this.props.appProps.waiverForm}>Waiver And Assumption Of Risk Form</a> and a completed <a target="_blank" href={this.props.appProps.personalInfoForm}>Guest Personal Information Form</a> for each member of your party indicating acceptance of our Terms and Conditions, Safety Guidelines, Code of Conduct and Privacy Policy. These documents are available at the bottom of all our web pages.</p>
                        <WdForm forceValidation={this.state.forceValidate} wdOnChange={this.handleFormChange}>
                            <WdForm.Checkbox rules={{required:true}} errorText="You must indicate your agreement to the above before continuing." label="I acknowledge and agree to the conditions set out in the Waiver And Assumption Of Risk Form and undertake to email or post to Mulga Bicycle Tours a signed Waiver And Assumption Of Risk Form and a completed Guest Personal Information Form for each person that I am making the booking for." checked={this.state.details.termsagree?true:false} valueIfSelected="Agreed" dataKey="termsagree" wdOnChange={this.handleElementChange} />
                            <WdForm.Checkbox rules={{required:true}} errorText="You must indicate your agreement to the above before continuing." label="I agree to read all tour information sent to me by Mulga Bicycle Tours" checked={this.state.details.termsagreefuture?true:false} valueIfSelected="Agreed" dataKey="termsagreefuture" wdOnChange={this.handleElementChange} />
                        </WdForm>

                        <div className="col-xs-12">
                            <div style={{display:this.state.couponExtended?'none':'block'}} className="coupon-hide-link"><a href="javascript:;" onClick={this.extendCoupon}>Add a Gift Card or Member Benefit Coupon +</a></div>
                            <div style={{display:this.state.couponExtended?'block':'none'}}>
                                <h3>Add a Gift Card or Member Benefit coupon</h3>
                                <p>If you have a gift card or coupon enter the code here. If the code is still current we will display your discount as you make your choices on the following screens.</p>
                                <div className="row">
                                    <div className="col-sm-4">
                                        <WdForm.Input type="text" label="Enter code" showLabel={false} value={this.state.details.promotionCode} dataKey="promotionCode" wdOnChange={this.handlePromotionCodeValChange}  />
                                    </div>
                                    <div className="col-sm-4">
                                        <a className="btn btn-info" onClick={this.handlePromotionCodeChange}>Apply Discount</a>
                                    </div>

                                </div>
                                {this.state.gettingDiscount===true?<span>Please wait.. retrieving discount information... <i className="fa fa-refresh fa-spin"></i></span>:null}
                                {discountMessage}
                            </div>
                        </div>


                        <div className="text-right"><div style={{display:'inline-block'}} onClick={this.nextStep}><a className="btn btn-primary" disabled={this.state.valid?"":"disabled"}>Continue</a></div></div>
                    </div>
                </div>
            </div>
        );
    }
});


