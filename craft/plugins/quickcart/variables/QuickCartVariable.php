<?php
namespace Craft;
class QuickCartVariable
{
    //======================================================================
    // Get Entries Criteria
    //======================================================================
    function getProducts($criteria=NULL)
    {
       return craft()->quickCart_products->find($criteria);
    }
}