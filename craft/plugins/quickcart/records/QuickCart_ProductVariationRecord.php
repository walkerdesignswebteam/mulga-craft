<?php
namespace Craft;
class QuickCart_ProductVariationRecord extends BaseRecord
{
	public function getTableName()
    {
        return 'quickcart_product_variation';
    }

    protected function defineAttributes()
    {
        return array(
			'product_id' => AttributeType::Number,
			'sort_id' => AttributeType::Number,
			'name' => AttributeType::String,
			'code' => AttributeType::String,
			//override = Will replace base cost with value
			//addition = Will add to the base cose
			//percentage = Will add a precentage of base cost (0-100)
			'type' => array(AttributeType::Enum, 'values' => "override,addition,percentage"),
			'value' => array('type'=>AttributeType::Number,'decimals'=>2),
        );
    }
	public function defineRelations()
    {
        return array(
            'product_id' => array(static::HAS_MANY, 'QuickCart_Product', 'id'),
        );
    }
}