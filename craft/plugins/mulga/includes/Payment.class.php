<?php
class Payment {
	private $gatewayUrl;
	private $customerId;
	private $testMode;
	
	private $totalCents;
	private $totalTaxCents;
	
	private $cardType;
	private $cardName;
	private $cardNumber;
	private $cardExpireMonth;
	private $cardExpireYear;
	private $cardCvn;
	
	private $formPostUrl;
	
	public function __construct($opts){
		$this->testMode=false;
		if(isset($opts['testMode'])===true){
				$this->testMode=$opts['testMode'];
		}
		if($this->testMode===true){
				$this->gatewayUrl='https://www.eway.com.au/gateway_cvn/xmltest/testpage.asp';
		}else{
				$this->gatewayUrl='https://www.eway.com.au/gateway_cvn/xmlpayment.asp';
		}

		$this->customerId=$opts['customerId'];
		$this->totalCents=$opts['totalCents'];

		if($opts['paymenttype']=='creditcard') {
			$this->cardType = $opts['cardtype'];
			$this->cardName = $opts['cardname'];
			$this->cardNumber = $opts['cardnumber'];
			$this->cardExpireMonth = $opts['expirymonth'];
			$this->cardExpireYear = $opts['expiryyear'];
			$this->cardCvn = $opts['cvn'];

			$this->cardExpireMonth = $opts['expirymonth'];
			$this->cardExpireYear = $opts['expiryyear'];
			$this->cardCvn = $opts['cvn'];
		}

		$this->firstName=$opts['firstName'];
		$this->lastName=$opts['lastName'];
		$this->email=$opts['email'];
	}
	public function handleBankTransferPayment(){
		$orderID='MULGA-BT-'.date('ymdHis');
		return array('status'=>'paid','transactionNumber'=>$orderID,'transactionRef'=>$orderID);
	}
	public function handleEwayPayment(){
		
		$orderID='MULGA-CC-'.date('ymdHis');
		
		//convert to cents and round to nearest cent
		$totalCents = floor($this->totalCents);

		$eway = new \EwayPayment($this->customerId,$this->gatewayUrl);

		//Substitute 'FirstName', 'Lastname' etc for $_POST["FieldName"] where FieldName is the name of your INPUT field on your webpage
		$eway->setCustomerFirstname( $this->firstName );
		$eway->setCustomerLastname( $this->lastName );
		$eway->setCustomerEmail( $this->email );
		//$eway->setCustomerAddress( '123 Someplace Street, Somewhere ACT' );
		//$eway->setCustomerPostcode( '2609' );
		//$eway->setCustomerInvoiceDescription( 'Testing' );
		$eway->setTrxnNumber($orderID);
		$eway->setCustomerInvoiceRef('INV-'.$orderID);
		$eway->setCardHoldersName($this->cardName);
		$eway->setCardNumber($this->cardNumber);
		$eway->setCardExpiryMonth($this->cardExpireMonth);
		$eway->setCardExpiryYear($this->cardExpireYear);
		$eway->setCVN($this->cardCvn);

		if($this->testMode===true){
			$eway->setTotalAmount(round($totalCents/100)*100);
			//test failed transactions by adding a few cents to the total
			//$eway->setTotalAmount($totalCents+5);
		}else{
			$eway->setTotalAmount($totalCents);
		}

		$eway->setOption1("orderID: ".$orderID);

		if( $eway->doPayment() == EWAY_TRANSACTION_OK ) {
			$s_ewayMessage=''
			.'Transaction Status: '.$eway->getTrxnStatus()
			.', Transaction Number: '.$eway->getTrxnNumber()
			.', Transaction Reference: '.$eway->getTrxnReference()
			.', Return Amount: '.$eway->getReturnAmount()
			.', Auth Code: '.$eway->getAuthCode();

			return array('status'=>'paid','transactionNumber'=>$eway->getTrxnNumber(),'transactionRef'=>$eway->getTrxnReference(),'fullMessage'=>$s_ewayMessage);
		} else {
			return array('status'=>'error','error'=>array('A transaction error occurred (Error: '.$eway->getError().')',$eway->getErrorMessage()));
		}
	}
}
