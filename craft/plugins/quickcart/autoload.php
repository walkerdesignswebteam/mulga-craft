<?php
$s_dirsToAutoload = array(
    'includes'
);
function autoloadPhpRecurse($s_fullDirPath){
    $handle = opendir($s_fullDirPath);
    $a_filesToLoad = array();
    $a_subdirsToLoad = array();
    if($handle) {
        while (false !== ($entry = readdir($handle))) {
            $s_fullSubPath = $s_fullDirPath.'/'.$entry;
            if($entry=='..' || $entry=='.'){
                //ignore
            }else {
                if (is_dir($s_fullSubPath)) {
                    $a_subdirsToLoad[]=$s_fullSubPath;
                } else {
                    $parts = explode('.', $entry);
                    if ($parts[count($parts) - 1] === 'php') {
                        $a_filesToLoad[]=$s_fullSubPath;
                    }
                }
            }
        }
        closedir($handle);
    }
    sort($a_filesToLoad);
    sort($a_subdirsToLoad);
    foreach($a_filesToLoad as $s_file){
        require $s_file;
    }
    foreach($a_subdirsToLoad as $s_dir){
        autoloadPhpRecurse($s_dir);
    }
}
foreach($s_dirsToAutoload as $s_dir){
    autoloadPhpRecurse(dirname(__FILE__).'/'.$s_dir);
}
