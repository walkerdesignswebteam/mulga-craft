<?php
namespace QuickCart;

class Manager extends Cart
{
    public static function setCart(){
        $_SESSION['quickCart']=array(
            'products'=>array(
                array('productCode'=>'SWM-HLM','qty'=>3),
                array('productCode'=>'TEST-PROD','qty'=>12)
            )
        );
    }
    public static function getCart(){
        return $_SESSION['quickCart'];
    }
}
