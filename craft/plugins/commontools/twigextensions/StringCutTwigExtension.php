<?php
//twigVar|stringCut(45)
namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class StringCutTwigExtension extends \Twig_Extension {

	public function getName() {
		return 'StringCut';
	}

	public function getFilters()
	{
		return array(
			'stringCut' => new Twig_Filter_Method($this, 'stringCut'),
		);
	}

	public function stringCut($string,$length) {
		if(mb_strlen($string)>$length){
			return mb_strcut($string,0,$length).'...';
		}
		return $string;
	}

}
