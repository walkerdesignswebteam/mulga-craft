<?php
namespace Craft;
class Mulga_OrdersModel extends BaseModel
{
    protected function defineAttributes()
    {
        return array(
            'firstName' => AttributeType::String,
            'lastName' => AttributeType::String,
            'orderCode' => AttributeType::String,
            'paymentSummary' => AttributeType::Mixed
        );
    }
}