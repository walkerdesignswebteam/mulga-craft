<?php
namespace Craft;
class QuickCart_OrderRecord extends BaseRecord
{
	public function getTableName()
    {
        return 'quickcart_order';
    }

    protected function defineAttributes()
    {
        return array(
			'billingFirstName' => AttributeType::String,
			'billingLastName' => AttributeType::String,
			'billingCompany' => AttributeType::String,
			'billingPhone' => AttributeType::String,
			'billingEmail' => AttributeType::Email,
			'billingAddress' => AttributeType::String,
			'billingSuburb' => AttributeType::String,
			'billingZip' => AttributeType::String,
			'billingCountry' => AttributeType::String,
			
			'deliveryFirstName' => AttributeType::String,
			'deliveryLastName' => AttributeType::String,
			'deliveryCompany' => AttributeType::String,
			'deliveryPhone' => AttributeType::String,
			'deliveryEmail' => AttributeType::Email,
			'deliveryAddress' => AttributeType::String,
			'deliverySuburb' => AttributeType::String,
			'deliveryZip' => AttributeType::String,
			'deliveryCountry' => AttributeType::String,
			
			'shippingMethod' => AttributeType::String,
			'paymentMethod'  => AttributeType::String,
			'status' => AttributeType::Number,
			'deliveryRequired' =>  AttributeType::Bool,
			
			'paymentResult' => AttributeType::Mixed,
			
			'extraData' => AttributeType::Mixed,

			'userId' => AttributeType::Number,
        );
    }
}