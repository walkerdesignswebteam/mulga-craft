//MIXINS 
var WdformMixin = {
    getInitialState: function() {
        return {valid:false,hasChanged:false}
    },
    componentWillMount: function() {
        if(this.props.callbackElementAttach){
            this.props.callbackElementAttach(this);
        }
    },
    componentDidMount:function(){
        //validate for the first time
        this.validate(this.props.value);
    },

    //here we can re-run validation if rules and values change dynamically
    //careful here. this is a good place to cause endless loops
    componentDidUpdate: function(prevprops) {
        //if(this.props.value) {
            if (this.props.value !== prevprops.value) {
                this.validate(this.props.value);
            }
       // }
        //force validate if rules change
        if(this.props.rules) {
            if (this.props.rules && (this.props.rules.required !== prevprops.rules.required)){
                this.validate(this.props.value);
            }
        }
    },

    validate:function(newVal){
        var stateIsValid = true;
        this.a_errors = [];
        this.s_errors = '';
        var errId=0;
        if(this.props.rules){
            //required
            if(this.props.rules.required){
                if(validator.isNull(newVal)){
                    stateIsValid = false;

                    var s_error = this.props.label+' is required';
                    if(this.props.errorText){
                        s_error = this.props.errorText;
                    }
                    this.a_errors.push({id:errId,message:s_error});
                    errId++;
                }
            }

            //ensure is numeric
            if(this.props.rules.isNumeric){
                if(validator.isNull(newVal)===false && validator.isNumeric(newVal)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' must be numeric'});
                    errId++;
                }
            }

            //minLength
            if(this.props.rules.minLength){
                if(validator.isNull(newVal)===false && validator.isLength(newVal,this.props.rules.minLength)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' must be at least '+this.props.rules.minLength+' characters'});
                    errId++;
                }
            }
            //maxLength
            if(this.props.rules.maxLength){
                if(validator.isNull(newVal)===false && validator.isLength(newVal,0,this.props.rules.maxLength)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' can only contain up to '+(this.props.rules.maxLength)+' characters'});
                    errId++;
                }
            }

            //email
            if(this.props.rules.email){
                if(validator.isNull(newVal)===false && validator.isEmail(newVal)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' is not a valid email address'});
                    errId++;
                }
            }
            //urls
            if(this.props.rules.url){
                if(validator.isNull(newVal)===false && validator.isURL(newVal)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' is not a valid url'});
                    errId++;
                }
            }
        }
        this.labelErrors = this.a_errors.map(function(error) {
            return (
                React.createElement("span", {className: "suberror", key: error.id}, error.message)
            );
        });
        this.state.valid = stateIsValid;
        this.forceUpdate(); //take this away... and all goes to hell :) Not 100% sure why I need this but the screen doesn't update unless we do.

        //fire event if specified
        if(this.props.wdOnChange){
            this.props.wdOnChange(this,newVal);
        }

        //callback up the form for validation listening
        if(this.props.callbackElementValueChange) {
            this.props.callbackElementValueChange(this);
        }
    },
    update:function(obj){
        this.state.hasChanged = true;
        this.validate(obj.target.value);
    },
    updateWithValue:function(val){
        this.state.hasChanged = true;
        this.validate(val);
    },
    //For autofills
    intervalCheck:function(){
        //this was killing the promo code stuff for some reason. Don't do this.
    },
};


//FORM HANDLER
var WdForm = React.createClass({displayName: "WdForm",
    elementAttach:function(elementRef){
        this.wdformElements.push(elementRef);
    },
    elementValueChange:function(elementRef){
        //unregister any elements that are no longer on screen (can happen with dynamically visible elements when they drop from screen).
        //var newFormElements = [];
        //for(var i=0;i<this.wdformElements.length;i++){
        //    if(this.wdformElements[i]._reactInternalInstance){
        //        newFormElements.push(this.wdformElements[i]);
        //    }
        //}
        //this.wdformElements[i] = newFormElements;

        //validate form
        var invalidCnt=0;
        for(var i=0;i<this.wdformElements.length;i++){
            if(this.wdformElements[i].state.valid===false){
                invalidCnt++;
            }
        }

        this.state.invalidCount = invalidCnt;
        this.state.valid = invalidCnt===0;

        this.forceUpdate();
        if(this.props.wdOnChange){
            //add a very quick delay (1ms is enough) so that the form change event fires after any element events
            var _this = this;
            setTimeout(function(){_this.props.wdOnChange(_this)}, 1);
        }
    },
    registerElements:function(children){
        //just in case it is an empty form
        if (typeof children !== 'object' || children === null) {
            return children;
        }
        //loop all children 
        return React.Children.map(children, function (child) {
            if(child && child.type) {
                if (child.type.displayName == 'SubmitButton') {
                    return React.cloneElement(child, {
                        onClick: this.submit,
                        label: React.createElement(
                            "span",
                            null,
                            child.props.label,
                            this.state.showSubmitError && this.state.invalidCount > 0 ? React.createElement("span", null, " (", this.state.invalidCount, " error", this.state.invalidCount !== 1 ? 's' : null, ")") : null)
                    });
                } else if (child.props && child.props.label) {
                    return React.cloneElement(child, {
                        callbackElementAttach: this.elementAttach,
                        callbackElementValueChange: this.elementValueChange,
                        wdformElId: 'wdform-el-' + WdTools.makeUniqueId()
                    }, child.props.children);
                } else {
                    return React.cloneElement(child, {}, this.registerElements(child.props.children));
                }
            }
        }, this);
    },
    submit:function(e){
        if(this.state.valid){
            this.props.wdOnSubmit();
        }else {
            this.forceValidation();
            this.forceUpdate();
        }
    },
    forceValidation:function(){
        //force all elements to validate
        for(var i=0;i<this.wdformElements.length;i++){
            this.wdformElements[i].state.hasChanged=true;
            this.wdformElements[i].forceUpdate();
        }
        this.state.showSubmitError=true;
    },
    getInitialState:function(){
        return {valid:false,invalidCount:0,showSubmitError:false}
    },
    componentWillMount:function(){
        this.wdformElements=[];
    },
    componentDidUpdate:function(){
        if(this.props.forceValidation===true) {
            this.forceValidation();
        }
    },
    render:function(){
        this.formId = 'wdform-'+WdTools.makeUniqueId();
        var reprocessedChildren = this.registerElements(this.props.children);
        return React.createElement("form", {id: this.formId, className: this.state.showSubmitError===true && this.state.valid===false?"wdform invalid":"wdform valid", onSubmit: this.submit}, reprocessedChildren)
    }
});


//ELEMENTS
WdForm.Input = React.createClass({displayName: "Input",
    mixins: [WdformMixin],
    componentDidMount:function(){
        var _thisRef=this;
        this.interval = setInterval(function(){ _thisRef.intervalCheck() }, 1000);
        this.setState({value:this.props.value});
    },
    componentWillUnmount: function() {
        return clearInterval(this.interval);
    },
    updateStateVal:function(event){
        this.setState({value: event.target.value});
    },
    onBlur:function(event){
        this.updateStateVal(event);
        this.update(event);
    },
    render:function(){
        var maxLength=255;
        if(this.props.rules && this.props.rules.maxLength){
            maxLength=this.props.rules.maxLength;
        }
        var showLabel = true;
        if(this.props.hasOwnProperty('showLabel') && this.props.showLabel===false){
            showLabel = false;
        }
        return (
            React.createElement("div", {className: "formControlWrap"}, 
                showLabel?React.createElement("label", {htmlFor: this.props.wdformElId}, this.props.label):null, 
                React.createElement("input", React.__spread({},  this.props, {maxLength: maxLength, id: this.props.wdformElId, className: "form-control", placeholder: this.props.label, value: this.state.value, onChange: this.updateStateVal, onBlur: this.onBlur})), 
                this.state.hasChanged===true && this.state.valid===false?React.createElement("div", null, React.createElement("label", {htmlFor: this.props.wdformElId, className: "el-errors"}, this.labelErrors)):null
                /* <pre>state: {JSON.stringify(this.state, undefined, 2)}</pre> */
            )
        )
    }
});


//Select source http://matt-harrison.com/building-a-complex-web-component-with-facebooks-react-library/
WdForm.Select = React.createClass({displayName: "Select",
    mixins: [WdformMixin],
    render: function(){
        var optionNodes = this.props.options.map(function(option){
            return React.createElement("option", {key: WdTools.makeUniqueId(), value: option.value}, option.label);
        });

        return (
            React.createElement("div", {className: "formControlWrap"}, 
                React.createElement("label", {htmlFor: this.props.wdformElId}, this.props.label), 
                React.createElement("select", {id: this.props.wdformElId, className: "form-control", ref: "menu", value: this.props.value, onChange: this.update}, 
                    optionNodes
                ), 
                this.state.hasChanged===true && this.state.valid===false?React.createElement("label", {htmlFor: this.props.wdformElId, className: "el-errors"}, this.labelErrors):null
            )
        );
    }
});


//Select source http://matt-harrison.com/building-a-complex-web-component-with-facebooks-react-library/
WdForm.Switcher = React.createClass({displayName: "Switcher",
    mixins: [WdformMixin],
    componentDidMount:function(){
        var b_isSelected=false;
        if(this.props.isSelected===true){
            b_isSelected=true;
        }
        this.setState({selected:b_isSelected,key:this.props.data.key});
        this.validate(this.props.value);
    },
    handleChange:function(e){
        if(this.props.isDisabled && this.props.isDisabled===true) {
            //this is disabled
        }else{
            if(this.props.isSelected===true){
                this.state.selected=false;
                this.state.value=false;
            }else{
                this.state.selected=true;
                this.state.value=this.props.valueIfSelected;
            }
            this.state.hasChanged=true;
            this.forceUpdate();
            if(this.props.onChange) {
                this.props.onChange(this.state);
            }
        }
    },
    render: function(){
        var icon;
        var classNames = 'switcher';
        if(this.props.isSelected) {
            classNames += ' selected';
            icon = React.createElement("i", {className: "fa fa-check"});
        }
        if(this.props.isDisabled && this.props.isDisabled===true) {
            classNames += ' disabled';
            icon = React.createElement("i", {className: "fa fa-ban"});
        }

        return (React.createElement("div", {className: classNames, value: this.props.value, onClick: this.handleChange}, React.createElement("span", {className: "switcherlabel"}, this.props.label, " ", icon)));
    }
});

WdForm.Checkbox = React.createClass({displayName: "Checkbox",
    mixins: [WdformMixin],
    componentDidMount:function(){
        var b_isSelected=false;
        if(this.props.checked===true){
            b_isSelected=true;
        }
        this.state.checked = b_isSelected;
        this.state.value = b_isSelected?this.props.valueIfSelected:'';
        this.state.valid = b_isSelected;
        this.validate(this.state.value);
    },

    handleChange: function(event) {
        var isSelected = event.target.checked;
        this.state.checked = isSelected;
        this.state.value = isSelected?this.props.valueIfSelected:'';
        this.state.hasChanged = true;
        this.validate(this.state.value);
    },
    render:function(){
        return (
            React.createElement("div", {className: "formControlWrap"}, 
                React.createElement("label", {className: "checkradio"}, 
                    React.createElement("input", {checked: this.state.checked, type: "checkbox", id: this.props.wdformElId, value: this.state.value, onChange: this.handleChange}), 
                    React.createElement("span", {className: "checkradiolabel"}, this.props.label)
                ), 
                this.state.hasChanged===true && this.state.valid===false?React.createElement("div", null, React.createElement("label", {htmlFor: this.props.wdformElId, className: "el-errors"}, this.labelErrors)):null
            )
        )
    }
});


//Select source http://matt-harrison.com/building-a-complex-web-component-with-facebooks-react-library/
WdForm.SwitcherGroup = React.createClass({displayName: "SwitcherGroup",
    mixins: [WdformMixin],
    updateSwitcher:function(switcherState){
        this.state.hasChanged = true;
        if(this.props.onChange){
            this.props.onChange(switcherState);
        }
    },
    render: function(){
        var _self = this;
        var content = this.props.data.map(function(option) {
            return (
                React.createElement(WdForm.Switcher, {isSelected: option.selected, isDisabled: option.isDisabled, label: option.label, valueIfSelected: option.value, key: option.key, data: option, onChange: _self.updateSwitcher})
            );
        });
        return (
            React.createElement("div", {className: "formControlWrap"}, 
                React.createElement("div", {className: "switcherGroup"}, content)
            )
        );
    }
});


WdForm.Date = React.createClass({displayName: "Date",
    mixins: [WdformMixin],
    componentDidMount:function(){
        var pickerEl =$('#'+this.props.wdformElId);
        var _this = this;
        pickerEl.datepicker({format: "dd/mm/yyyy"}).on("changeDate", function(e) {
            _this.value = e.target.value;
            _this.handleChange(e);
        }).on("clearDate", function(e) {
            _this.value = e.target.value;
            _this.handleChange(e);
        });
        this.validate(this.props.value);
    },
    handleChange:function(e){
        //fire custom event up chain so that parent can update props.value
        //Not 100% sure if this is the best way. Couldnt find real clear documentation to do this. 
        this.update(e);
    },
    render:function(){
        return (
            React.createElement("div", {className: "formControlWrap"}, 
                React.createElement("label", {htmlFor: this.props.wdformElId}, this.props.label), 
                React.createElement("input", React.__spread({type: "text", id: this.props.wdformElId, className: "form-control date-picker", placeholder: "DD/MM/YYYY", onChange: this.handleChange},   this.props)), 
                this.state.hasChanged===true && this.state.valid===false?React.createElement("label", {htmlFor: this.props.wdformElId, className: "el-errors"}, this.labelErrors):null
            )
        )
    }
});

WdForm.Button = React.createClass({displayName: "Button",
    render:function(){
        return (
            React.createElement("button", React.__spread({type: "submit", onClick: this.props.onClick},  this.props), this.props.label)
        )
    }
});
//element that is tied to a form
WdForm.SubmitButton = React.createClass({displayName: "SubmitButton",
    render:function(){
        return (
            React.createElement("button", React.__spread({type: "submit"},  this.props), this.props.label)
        )
    }
});

WdForm.TextArea = React.createClass({displayName: "TextArea",
    mixins: [WdformMixin],
    componentDidMount:function(){
        var _thisRef=this;
        this.interval = setInterval(function(){ _thisRef.intervalCheck() }, 1000); //handle autofills.
        this.setState({value:this.props.value});
    },
    componentWillUnmount: function() {
        return clearInterval(this.interval);
    },
    updateStateVal:function(event){
        this.setState({value: event.target.value});
    },
    onBlur:function(event){
        this.updateStateVal(event);
        this.update(event);
    },
    render:function(){
        var showLabel = true;
        if(this.props.hasOwnProperty('showLabel') && this.props.showLabel===false){
            showLabel = false;
        }
        return (
            React.createElement("div", {className: "formControlWrap"}, 
                showLabel?React.createElement("label", {htmlFor: this.props.wdformElId}, this.props.label):null, 
                React.createElement("textarea", React.__spread({},  this.props, {value: this.state.value, id: this.props.wdformElId, className: "form-control", rows: "3", placeholder: this.props.label, onChange: this.updateStateVal, onBlur: this.onBlur})), 
                this.state.hasChanged===true && this.state.valid===false?React.createElement("label", {htmlFor: this.props.wdformElId, className: "el-errors"}, this.labelErrors):null
            )
        )
    }
});



var WdTools = {
	genBit:function(){
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },
    makeUniqueId:function(){
    	return this.genBit() + '-' + this.genBit() + '-' + this.genBit() + '-' + this.genBit();
    },
    toCurrency:function(number, decPlaces, thouSeparator, decSeparator) {
        var n = number
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    },
    priceFormat:function(number,valIfZero){
        if(number==0){
            if(typeof(valIfZero)==='undefined'){
                return 'Free';
            }else{
                return valIfZero;
            }
        }
        return '$'+this.toCurrency(number);
    }
};
var WdUi = React.createClass({displayName: "WdUi",
  render: function() {
    return (
    React.createElement("div", null)
    ); 
  }
});
 
WdUi.Modal = React.createClass({displayName: "Modal",
  componentDidMount:function(){
    var thisRef = this;
    var modalRef = $(ReactDOM.findDOMNode(this));
    modalRef.modal({
      backdrop: 'static'
    });
    modalRef.on('hidden.bs.modal', function (e) {
      //fire event if specified
      if(thisRef.props.wdOnClose){
          thisRef.props.wdOnClose(thisRef);
      }
    });
  },
  render: function() {
    return (
      React.createElement("div", {className: "modal fade", tabIndex: "-1", role: "dialog", "aria-labelledby": "myModalLabel", id: "modal"+WdTools.makeUniqueId()}, 
        React.createElement("div", {className: "modal-dialog", role: "document"}, 
          React.createElement("div", {className: "modal-content"}, 
            React.createElement("div", {className: "modal-header"}, 
              React.createElement("button", {type: "button", className: "close", "data-dismiss": "modal", "aria-label": "Close"}, React.createElement("span", {"aria-hidden": "true"}, "×")), 
              React.createElement("h4", {className: "modal-title", id: "myModalLabel"}, this.props.title)
            ), 
            this.props.children
          )
        )
      )
    );
  }
});
var BookingAppSettings = {
    depositPerParticipant:false, //set by app props
    modalContainerID:'modal-reactcontainer',
    creditcarddetails:{},
    clearModal:function(){
        ReactDOM.unmountComponentAtNode(document.getElementById(BookingAppSettings.modalContainerID));
    },
    getAccomPrice:function(accomInfo,priceLabel){
        var i;
        var thisOpt;
        for(i=0;i<accomInfo.priceOptions.length;i++){
            thisOpt = accomInfo.priceOptions[i];
            if(thisOpt.label==priceLabel){
                return thisOpt.price;
            }
        }
        return false;
    },
    participantIsSharing:function(participant){
        if(participant.sharedwith=='none'||participant.sharedwith==''){
            return false;
        }else{
            return true;
        }
    },

    getSharedParticipantLabel:function(participant,participants) {
        var j;
        for (j = 0; j < participants.length; j++) {
            thisInnerPart = participants[j];
            if (participant.id === thisInnerPart.sharedwith) {
                return participant.firstName+' & '+ thisInnerPart.firstName;
            }
        }
        return false;
    },
    scrollToTopError:function(){
        setTimeout(function(){
            var jumpEl = jQuery(".el-errors").first().parents(".formControlWrap");
            if(jumpEl.length>0) {
                jQuery("html,body").animate({scrollTop: jumpEl.offset().top});
            }
        },100);
    },
    scrollToTopOfSection:function(){
        setTimeout(function(){
            var jumpEl = jQuery(".steps .active").first();
            if(jumpEl.length>0) {
                //console.log(jQuery("html body").scrollTop()+' '+jumpEl.offset().top);
                //if(jumpEl.offset().top>jQuery("html body").scrollTop()) {
                    jQuery("html body").animate({scrollTop: jumpEl.offset().top});
                //}
            }
        },100);
    },
    outputJson:function(obj){
        console.log(JSON.stringify(obj));
    },
    getTourPrice:function(priceOptions,type){
        var i;
        for(i=0;i<priceOptions.length;i++){
            if(priceOptions[i].label===type){
                return priceOptions[i].price;
            }
        }
        return false;
    }
}

var BookingUI = React.createClass({displayName: "BookingUI",
    screenDomID:'screenContent',
    slideSpeed:500,
    getInitialState: function() {
        BookingAppSettings.depositPerParticipant = this.props.bookingfee;

        var debugMode = false;
        if(debugMode===false){
            return {
                debugMode:false,
                loading:true,

                availabilityinfo:false,
                promotionInfo:false,
                participants:[],
                bookingDetails:{},
                termsDetails:{},

                activeMenuItem:'ScreenTerms',
                menuItems: [
                    {index:0,id:'ScreenTerms',title:'Terms and Conditions'},
                    {index:1,id:'ScreenDetails',title:'Order Details'},
                    {index:2,id:'ScreenParticipants',title:'Tour Guests'},
                    {index:3,id:'ScreenExtras',title:'Extras'},
                    {index:4,id:'ScreenPayment',title:'Summary and Payment'},
                ]
            };
        }else{
            //preload a heap of defaults so we are not always needing to refill everything
            return {
                debugMode:true,
                loading:true,

                availabilityinfo:false,
                promotionInfo:false,
                participants:[{"id":"d0e4-5dd2-f5b4-e527","changed":true,"valid":true,"sharedwith":"17cd-7940-a00c-2e83","firstName":"Marcus","lastName":"House","isDisabled":false,"accom":["PKXMZD"],"extras":["PRGXVZ"]},{"id":"17cd-7940-a00c-2e83","changed":true,"valid":true,"sharedwith":"d0e4-5dd2-f5b4-e527","firstName":"Linda","lastName":"House","isDisabled":false,"extras":["P8XLPW"]},{"id":"8524-4d2d-9bf7-a35d","changed":true,"valid":true,"sharedwith":"b02e-4924-75f0-613f","firstName":"Alex","lastName":"House","isDisabled":false,"accom":["P8ZPVW"],"extras":["PRGXVZ","P8XLPW"]},{"id":"b02e-4924-75f0-613f","changed":true,"valid":true,"sharedwith":"8524-4d2d-9bf7-a35d","firstName":"Lachlan","lastName":"House","isDisabled":false,"extras":[]},{"id":"c6f4-4761-686a-ed2f","changed":true,"valid":true,"sharedwith":"none","firstName":"Melanie","lastName":"Tyson","isDisabled":false,"accom":["P8ZPVW"],"extras":["P8XLPW"]}],
                bookingDetails:{"firstName":"Marcus","lastName":"House","email":"marcus@walkerdesigns.com.au","phone":"0438402887","address":"86 Mt Leslie Rd","city":"Prospect Vale","state":"Tasmania","postcode":"7250","country":"Australia","howhear":"From Google!\nOh, and from Mulga :)\n\nkind regards\n\nMarcus"},
                termsDetails:{termsagree:'Agreed',termsagreefuture:'Agree'},

                activeMenuItem:'ScreenTerms',
                menuItems: [
                    {index:0,id:'ScreenTerms',title:'Terms and Conditions'},
                    {index:1,id:'ScreenDetails',title:'Order Details'},
                    {index:2,id:'ScreenParticipants',title:'Tour Guests'},
                    {index:3,id:'ScreenExtras',title:'Extras'},
                    {index:4,id:'ScreenPayment',title:'Summary and Payment'}
                ]
            };
       }
    },
    componentDidMount:function(){
        var post_data = this.props;
        var _self = this;
        //if(this.state.debugMode===false) {
            this.setState({loading: true});

            $.ajax({
                type: "POST",
                url: '/tour/bookajax',
                data: post_data,
                success: function (data) {
                    _self.setState({
                        loading:false,
                        availabilityinfo:data,
                    });
                    _self.loadScreen(_self.state.activeMenuItem,false);
                }
            });
        //}else{
            //debug more has a bunch of default data
            //this.loadScreen(this.state.activeMenuItem);
        //}
    },
    refreshAccomAvailabilities:function(){
        var i,j;
        var thisAccom,thisPart,thisCnt;
        for(i=0;i<this.state.availabilityinfo.accom.length;i++){
            thisCnt=0;
            thisAccom = this.state.availabilityinfo.accom[i];
            for(j=0;j<this.state.participants.length;j++){
                thisPart = this.state.participants[j];
                if(thisPart.accom) {
                    if (thisPart.accom.indexOf(thisAccom.productCode) !== -1) {
                        thisCnt++;
                    }
                }
            }
            this.state.availabilityinfo.accom[i].seatsSelected = thisCnt;
        }
    },
    onUpdateParticipants:function(participants){
        this.setState({participants:participants});
    },
    onSaveParticipants:function(participants){
        this.onUpdateParticipants(participants);
        this.loadScreen('ScreenExtras');
    },
    onUpdateExtras:function(participants){
        this.setState({participants:participants});
        this.refreshAccomAvailabilities();
    },
    onSaveExtras:function(participants){
        this.onUpdateExtras(participants);
        this.loadScreen('ScreenPayment');
    },
    onUpdateBookingDetails:function(details){
        this.setState({bookingDetails:details});
    },
    onSaveBookingDetails:function(details){
        this.onUpdateBookingDetails(details);
        if(this.state.participants.length==0){
            this.setState({participants:[{id:WdTools.makeUniqueId(),changed:false,valid:false,sharedwith:'',firstName:details.firstName,lastName:details.lastName}]});
        }
        this.loadScreen('ScreenParticipants');
    },
    onUpdatePaymentDetails:function(details){
        BookingAppSettings.creditcarddetails = details;
    },
    verifyBookingDataStillValid:function(newLoadedData){
        this.forceUpdate();
    },
    onSavePaymentDetails:function(details){
        this.onUpdatePaymentDetails(details);

        //check that all allocations are ok
        //post_data = {participants:this.state.participants,cmd:'checkOkToPay'};
        var _thisRef = this;
        var post_data = this.props;
        $.ajax({
            type: "POST",
            url: '/tour/bookajax',
            data: post_data,
            success: function (data) {
                _thisRef.verifyBookingDataStillValid(data);
            }
        });
    },

    onUpdateTerms:function(details){
        this.setState({termsDetails:details});
    },
    onSaveTerms:function(details){
        this.onUpdateTerms(details);
        this.loadScreen('ScreenDetails');
    },
    onDiscountChange:function(promotionInfo){
        this.setState({promotionInfo:promotionInfo});
    },
    loadScreen:function(screenId,animateUser){
        $('#'+this.screenDomID).slideUp(this.slideSpeed,function(){
            switch (screenId) {
                case 'ScreenTerms':

                    this.refreshAccomAvailabilities();

                    reactScreen = React.createElement(BookingUI.ScreenTerms, {
                        onDiscountChange: this.onDiscountChange, termsDetails: this.state.termsDetails, 
                        onSave: this.onSaveTerms, appProps: this.props, onChange: this.onUpdateTerms});
                    break;
                case 'ScreenParticipants':
                    reactScreen = React.createElement(BookingUI.ScreenParticipants, {
                        data: {participants:this.state.participants,availabilityinfo:this.state.availabilityinfo}, 
                        onUpdateParticipants: this.onUpdateParticipants, 
                        onSaveParticipants: this.onSaveParticipants});
                    break;
                case 'ScreenExtras':
                    reactScreen = React.createElement(BookingUI.ScreenExtras, {
                        data: {participants:this.state.participants,availabilityinfo:this.state.availabilityinfo}, 
                        onUpdateExtras: this.onUpdateExtras, onSaveExtras: this.onSaveExtras});
                    break;
                case 'ScreenDetails':
                    reactScreen = React.createElement(BookingUI.ScreenDetails, {
                        data: this.state.bookingDetails, 
                        onChange: this.onUpdateBookingDetails, onSave: this.onSaveBookingDetails});
                    break;
                case 'ScreenPayment':
                    reactScreen = React.createElement(BookingUI.ScreenPayment, {
                        data: BookingAppSettings.creditcarddetails, promotionInfo: this.state.promotionInfo, bookingCosts: this.getPrices(), appProps: this.props, appState: this.state, 
                        onChange: this.onUpdatePaymentDetails, onSave: this.onSavePaymentDetails});
                    break;
                default:
                    reactScreen = React.createElement("div", null, "BookingUI.", screenId, " was not found.");
                    break;
            }

            ReactDOM.render(reactScreen, document.getElementById(this.screenDomID));
            this.setState({activeMenuItem: screenId});
            $('#'+this.screenDomID).slideDown(this.slideSpeed,function(){
                if (typeof(animateUser) === 'undefined' || animateUser!==false ) {
                    setTimeout( function(){BookingAppSettings.scrollToTopOfSection();},100);
                }
            });

        }.bind(this));
    },
    getScreenIndex:function(){
        var activeIndex = 0;
        for(var i=0;i<this.state.menuItems.length;i++){
            if(this.state.activeMenuItem==this.state.menuItems[i].id){
                activeIndex=i;
            }
        }
        return activeIndex;
    },
    menuItemClicked:function(menuItem){
        this.loadScreen(menuItem.id);
    },
    getAccomExtraInfoForParticipant:function(participant,participants){
        var i,j;
        var thisAccom;
        var a_ret = [];
        if(participant.accomExtras && participant.accomExtras.length>0){
            for(i=0;i<this.state.availabilityinfo.accomExtras.length;i++){
                thisAccom = this.state.availabilityinfo.accomExtras[i];
                for(j=0;j<participant.accomExtras.length;j++) {
                    if (thisAccom.productCode == participant.accomExtras[j]) {
                        var priceToGet = 'Single';
                        var name = participant.firstName;
                        if (BookingAppSettings.participantIsSharing(participant)) {
                            priceToGet = participant.doubletwin;
                            name = BookingAppSettings.getSharedParticipantLabel(participant, participants);
                        }
                        a_ret.push({
                            name: name,
                            product: thisAccom.product.name,
                            type: priceToGet,
                            price: BookingAppSettings.getAccomPrice(thisAccom, priceToGet)
                        });
                    }
                }
            }
        }
        return a_ret;
    },
    getAccomInfoForParticipant:function(participant,participants){
        var i,j;
        var thisAccom;
        if(participant.accom && participant.accom.length>0){
            for(i=0;i<this.state.availabilityinfo.accom.length;i++){
                thisAccom = this.state.availabilityinfo.accom[i];
                if(thisAccom.productCode==participant.accom[0]){
                    var priceToGet = 'Single';
                    var name = participant.firstName;
                    if(BookingAppSettings.participantIsSharing(participant)){
                        priceToGet=participant.doubletwin;
                        name = BookingAppSettings.getSharedParticipantLabel(participant,participants);
                    }
                    return {name:name,product:thisAccom.product.name,type:priceToGet,price:BookingAppSettings.getAccomPrice(thisAccom,priceToGet)}
                }
            }
        }
        return false;
    },
    getPrices:function(){
        if(this.state.availabilityinfo.tour){
            //Get tour total
            var a_subtotals = [];
            var keyID=0;
            var peopleOnTourSingle = 0;
            var peopleOnTourDouble = 0;

            var priceOfTourSingle = BookingAppSettings.getTourPrice(this.state.availabilityinfo.tour.priceOptions,'Single');
            var priceOfTourDouble = BookingAppSettings.getTourPrice(this.state.availabilityinfo.tour.priceOptions,'Double');
            for(i=0;i<this.state.participants.length;i++) {
                if (BookingAppSettings.participantIsSharing(this.state.participants[i]) === false) {
                    peopleOnTourSingle++;
                }else{
                    peopleOnTourDouble++;
                }
            }

            var totalTourPriceSingle = peopleOnTourSingle*priceOfTourSingle;
            var totalTourPriceDouble = peopleOnTourDouble*priceOfTourDouble;

            var total = totalTourPriceSingle+totalTourPriceDouble;
            if(this.state.participants.length>0) {
                a_subtotals.push({key: keyID++, subTitle: 'Tour'});
            }else{
                a_subtotals.push({key: keyID++, subTitle:'Awaiting selection of guests...'});
            }

            if(peopleOnTourDouble>0){
                a_subtotals.push({key:keyID++,label:peopleOnTourDouble+' guest'+(peopleOnTourDouble>1?'s':'')+' x '+WdTools.priceFormat(priceOfTourDouble)+' twin share',total:totalTourPriceDouble});
            }
            if(peopleOnTourSingle>0){
                a_subtotals.push({key:keyID++,label:peopleOnTourSingle+' guest'+(peopleOnTourSingle>1?'s':'')+' x '+WdTools.priceFormat(priceOfTourSingle)+' single',total:totalTourPriceSingle});
            }




            //Get accommodation entries
            var i,j;
            var thisAccomInfo;
            var pushedSub = false;
            var thisPrice;
            for(i=0;i<this.state.participants.length;i++){
                thisAccomInfo = this.getAccomInfoForParticipant(this.state.participants[i],this.state.participants);
                if(thisAccomInfo){
                    if(pushedSub===false){
                        a_subtotals.push({key:keyID++,subTitle:'Accommodation'});
                        pushedSub=true;
                    }
                    thisPrice = thisAccomInfo.price;
                    total+=thisPrice;
                    a_subtotals.push({key:keyID++,label:thisAccomInfo.name+' - '+thisAccomInfo.product+' - '+thisAccomInfo.type  ,total:thisPrice,info:thisAccomInfo});
                }
            }

            //Get accommodation extras
            //pushedSub = false;
            var thisLabel;
            var singlePrice;
            for(i=0;i<this.state.participants.length;i++){
                a_thisAccomInfo = this.getAccomExtraInfoForParticipant(this.state.participants[i],this.state.participants);
                for(j=0;j<a_thisAccomInfo.length;j++) {
                    if (a_thisAccomInfo[j]) {

                        thisAccomInfo = a_thisAccomInfo[j]
                        //if (pushedSub === false) {
                            //a_subtotals.push({key: keyID++, subTitle: 'Pre/Post Tour Accommodation'});
                            //pushedSub = true;
                        //}
                        console.log(thisAccomInfo);
                        thisPrice = thisAccomInfo.price;
                        total += thisPrice;
                        if(thisAccomInfo.type==='Twin' || thisAccomInfo.type==='Double'){
                            singlePrice = thisAccomInfo.price/2;
                            thisLabel = thisAccomInfo.name + ' - ' + thisAccomInfo.product + ' - 2 guests x '+WdTools.priceFormat(singlePrice);
                        }else{
                            thisLabel = thisAccomInfo.name + ' - ' + thisAccomInfo.product + ' - ' + thisAccomInfo.type;
                        }
                        a_subtotals.push({
                            key: keyID++,
                            label: thisLabel,
                            total: thisPrice,
                            info: thisAccomInfo
                        });
                    }
                }
            }

            //Get extras
            var thisExtra;
            var thisPart;
            var namesWithExtra;
            //pushedSub = false;
            for(i=0;i<this.state.availabilityinfo.extras.length;i++){
                thisExtra = this.state.availabilityinfo.extras[i];
                namesWithExtra=[];
                for(j=0;j<this.state.participants.length;j++){
                    thisPart = this.state.participants[j];
                    if(thisPart.extras) {
                        if (thisPart.extras.indexOf(thisExtra.productCode) !== -1) {
                            namesWithExtra.push(thisPart.firstName);
                        }
                    }
                }
                if(namesWithExtra.length>0) {
                    //if (pushedSub === false) {
                        //a_subtotals.push({key: keyID++, subTitle: 'Extras'});
                        //pushedSub = true;
                    //}
                    thisPrice = namesWithExtra.length*thisExtra.priceOptions[0].price;
                    total+=thisPrice;
                    a_subtotals.push({key:keyID++,label:namesWithExtra.length+' x '+thisExtra.product.name+' ('+namesWithExtra.join(', ')+')',total:thisPrice});
                }
            }

            return ({
                subtotals:a_subtotals,
                total:total
            });
        }
    },
    render:function(){
        var _state = this.state;
        var screenIndex = this.getScreenIndex();
        var menuItemsBefore = this.state.menuItems.map(function(menuItem) {
            if(menuItem.index<=screenIndex) {
                var activeClass = (this.state.activeMenuItem === menuItem.id ? 'active' : 'inactive');
                return (
                    React.createElement("li", {key: menuItem.id, className: activeClass, onClick: this.menuItemClicked.bind(this,menuItem)}, React.createElement("a", {href: "javascript:;"}, menuItem.title))
                );
            }
        }.bind(this));

        var menuItemsAfter = this.state.menuItems.map(function(menuItem) {
            if(menuItem.index>screenIndex) {
                return (
                    React.createElement("li", {key: menuItem.id}, React.createElement("span", null, menuItem.title))
                );
            }
        }.bind(this));

        var prices = this.getPrices();
        var priceSummary;
        var priceSummaryPanel;
        if(prices) {
            priceSummary = prices.subtotals.map(function (price) {
                if(price.subTitle){
                    return (React.createElement("li", {key: price.key, className: "subTitle"}, React.createElement("span", null, React.createElement("strong", null, price.subTitle))));
                }else {
                    return (React.createElement("li", {key: price.key}, React.createElement("span", null, price.label, React.createElement("br", null), React.createElement("strong", null, WdTools.priceFormat(price.total)))));
                }
            }.bind(this));

            var i;
            var priceElements = [];
            for(i=0;i<prices.length;i++){
                priceElements.push(React.createElement("li", null, prices[i].label));
            }
            var depositAmount = BookingAppSettings.depositPerParticipant*this.state.participants.length;
            if(this.state.participants.length==0){
                depositAmount = BookingAppSettings.depositPerParticipant;
            }

            var discount = 0;
            if(this.state.promotionInfo){
                if(this.state.promotionInfo.couponType.value==='percentage'){
                    if(prices.subtotals.length >1){
                        discount = prices.subtotals[1].total*(this.state.promotionInfo.couponValue/100);
                    }
                }else if(this.state.promotionInfo.couponType.value==='dollar' ) {
                    discount = this.state.promotionInfo.couponValue*this.state.participants.length;
                }
            }

            priceSummaryPanel =
            React.createElement("div", null, 
                React.createElement("h2", null, "Your Booking Information"), 
                React.createElement("ul", {className: "list-unstyled priceSummary"}, priceSummary), 
                prices.total>0?React.createElement("div", null, React.createElement("h4", null, "Total ", WdTools.priceFormat(prices.total), " incl GST")):null, 
                discount>0?React.createElement("div", null, React.createElement("h4", null, "Discount ", WdTools.priceFormat(discount), " incl GST"), React.createElement("h3", null, "Total After Discount", React.createElement("br", null), WdTools.priceFormat(prices.total-discount), " incl GST")):null, 
                prices.total>0?React.createElement("div", null, React.createElement("hr", null), React.createElement("h3", null, "Booking fee", React.createElement("br", null), WdTools.priceFormat(depositAmount), " incl GST")):null
            )
            ;
        }

        if(BookingAppSettings.processingPayment===true){
            menuItemsBefore = [];
            menuItemsAfter = [];
        }


        return(
            React.createElement("div", {className: "bookingUI"}, 
                React.createElement("div", {className: "row", style: {display:(this.state.loading?'block':'none')}}, 
                    React.createElement("div", {className: "col-sm-12"}, 
                        React.createElement("p", null, "Please wait, loading tour information... ", React.createElement("i", {className: "fa fa-refresh fa-spin"}))
                    )
                ), 

                React.createElement("div", {className: "row", style: {display:(this.state.loading?'none':'block')}}, 
                    React.createElement("div", {className: "col-xs-12 col-sm-8"}, 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-sm-12"}, 
                                React.createElement("ul", {className: "steps before list-unstyled"}, menuItemsBefore)
                            )
                        ), 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-sm-12"}, React.createElement("div", {id: this.screenDomID, className: "screenContent"}))
                        ), 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-sm-12"}, 
                                React.createElement("ul", {className: "steps after list-unstyled"}, menuItemsAfter)
                            )
                        )
                    ), 
                    React.createElement("div", {className: "col-xs-12 col-sm-4"}, priceSummaryPanel)
                ), 
                React.createElement("div", {id: BookingAppSettings.modalContainerID})
            )
        );
    }
});





BookingUI.ScreenParticipants = React.createClass({displayName: "ScreenParticipants",
     getInitialState: function() {
         var defaultParticipants = [
             {id:WdTools.makeUniqueId(),changed:false,valid:false,sharedwith:'',firstName:'',lastName:''},
         ];
        if(this.props.data.participants!==false){
            defaultParticipants = this.props.data.participants;
        }
        return {
            valid:false,
            participants:defaultParticipants
        };
    },
    addParticipant:function(){
        if(this.state.participants.length>=this.props.data.availabilityinfo.tour.seatsAvailable){
            ReactDOM.render(
                React.createElement(WdUi.Modal, {title: "No Tour Spots Remaining", wdOnClose: BookingAppSettings.clearModal}, 
                    React.createElement("div", {className: "modal-body"}, "Only ", React.createElement("strong", null, this.props.data.availabilityinfo.tour.seatsAvailable), " seats remain on this tour. Please contact us if you wish to book a custom event or be reserved for a future tour."), 
                    React.createElement("div", {className: "modal-footer"}, 
                        React.createElement("button", {onClick: BookingAppSettings.clearModal, type: "button", className: "btn btn-primary", "data-dismiss": "modal"}, "OK")
                    )
                ),document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else {
            var newParticipants = this.state.participants;
            newParticipants.push({
                id: WdTools.makeUniqueId(),
                changed: false,
                valid: false,
                sharedwith:'',
                firstName: '',
                lastName: ''
            });
            this.setState({participants:newParticipants});
        }

    },
    removeParticipant:function(participantToRemove){
        var newParticipants = [];
        var i;
        var thisParticipant;
        for(i=0;i<this.state.participants.length;i++){
            thisParticipant = this.state.participants[i];
            if(participantToRemove.id!==thisParticipant.id){
                //killinkedreference - could be done in the refresh
                if(thisParticipant.sharedwith===participantToRemove.id){
                    thisParticipant.sharedwith='';
                }
                newParticipants.push(thisParticipant);
            }
        }
        this.setState({participants:newParticipants});
    },
    validateParticipants:function(forceErrorMessagesToShow){
        var i;
        var allValid = true;
        var updatedParticipants = this.state.participants;
        for(i=0;i<updatedParticipants.length;i++) {
            if(forceErrorMessagesToShow) {
                updatedParticipants[i].changed = true; //this will force error message to show in form
            }
            if (updatedParticipants[i].valid === false) {
                allValid = false;
            }
        }
        this.setState({participants:updatedParticipants,valid:allValid});
    },
    updateRoomLinksForParticipant:function(participant){
        var i;
        var updatedParticipants = this.state.participants;
        //link
        var crossLinkedId = false;
        for(i=0;i<updatedParticipants.length;i++){
            if(BookingAppSettings.participantIsSharing(participant)===false && updatedParticipants[i].sharedwith==participant.id){
                //if participant set to blank or none, blank out any other participant linked.
                updatedParticipants[i].sharedwith = '';
                updatedParticipants[i].doubletwin = '';
            }else if(crossLinkedId===false && participant.sharedwith==updatedParticipants[i].id) {
                //set the target participant to have reverse link back to the participant that just changed
                updatedParticipants[i].sharedwith = participant.id;
                updatedParticipants[i].doubletwin = participant.doubletwin;
                crossLinkedId = updatedParticipants[i].id;
            }else if(updatedParticipants[i].sharedwith==participant.id){
                //kill off any previous links to the participant id (otherwise multiple can be linked back to the one which causes worlds of trouble.
                updatedParticipants[i].sharedwith = '';
                updatedParticipants[i].doubletwin = '';
            }
        }
        this.setState({participants:updatedParticipants});
    },
    onChange:function(participant){
        var i;
        var updatedParticipants = this.state.participants;
        for(i=0;i<updatedParticipants.length;i++){
            if(participant.id==updatedParticipants[i].id){
                updatedParticipants[i] = participant;
                break;
            }
        }
        //link shared participants if needed
        this.setState({participants:updatedParticipants});
        this.updateRoomLinksForParticipant(participant);
        this.validateParticipants(false);
        this.props.onUpdateParticipants(this.state.participants);
    },

    nextStep:function(){
        this.validateParticipants(true);
        if(this.state.valid===false){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                React.createElement(WdUi.Modal, {title: "More detail required", wdOnClose: BookingAppSettings.clearModal}, 
                    React.createElement("div", {className: "modal-body"}, "Please enter all required details for each guest."), 
                    React.createElement("div", {className: "modal-footer"}, 
                        React.createElement("button", {onClick: BookingAppSettings.clearModal, type: "button", className: "btn btn-primary", "data-dismiss": "modal"}, "OK")
                    )
                ),document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.props.onSaveParticipants(this.state.participants);
        }
    },
    render:function(){
        return(
        React.createElement("div", null, 
            React.createElement("h2", null, "Tour Guests"), 
            React.createElement("p", null, "Please enter the details for each guest below. To add more guests, press the ", React.createElement("strong", null, React.createElement("em", null, "\"Add Guest\"")), " button."), 
            React.createElement(BookingUI.ParticipantFormList, {data: this.state.participants, onChange: this.onChange, onRemove: this.removeParticipant}), 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-xs-6"}, React.createElement("a", {className: "btn btn-success", onClick: this.addParticipant}, React.createElement("i", {className: "fa fa-plus-circle"}), " Add Guest")), 
                React.createElement("div", {className: "col-xs-6 text-right"}, React.createElement("div", {style: {display:'inline-block'}, onClick: this.nextStep}, React.createElement("a", {className: "btn btn-primary", disabled: this.state.valid?"":"disabled"}, "Continue")))
            )
        )
        );
    }
});
BookingUI.ParticipantFormList = React.createClass({displayName: "ParticipantFormList",
    onChange: function(participant){
        this.props.onChange(participant);
    },
    onRemove: function(participant){
        this.props.onRemove(participant);
    },
    render:function(){
        var _thisref = this;

        var singleOnly = false;
        if(this.props.data.length==1){
            singleOnly=true;
        }
        var allparticipants = this.props.data;
        var content = this.props.data.map(function(participant) {
            var shareSelectOpts = [{value:'',label:'Please select'},{value:'none',label:'Single Room (don\'t share)'}];
            for(var i=0;i<allparticipants.length;i++) {
                var thisParticipant = allparticipants[i];
                //only provide share option if not self and not already assigned
                var showThisOption = false;
                //default - show if not already paired at all
                if(BookingAppSettings.participantIsSharing(thisParticipant)===false){
                    showThisOption=true;
                }
                //dont shot if self
                if(thisParticipant.id === participant.id){
                    showThisOption=false;
                }
                //show if already paired to it
                if(thisParticipant.sharedwith===participant.id){
                    showThisOption=true;
                }
                if (showThisOption) {
                    var label = 'Unnamed Guest '+(i+1);
                    if(thisParticipant.firstName){
                        label = 'Share with '+thisParticipant.firstName + ' ' + thisParticipant.lastName;
                    }
                    shareSelectOpts.push({
                        value: thisParticipant.id,
                        label: label
                    });
                }
            }
            return (
                React.createElement(BookingUI.ParticipantForm, {key: participant.id, data: participant, shareOpts: shareSelectOpts, singleOnly: singleOnly, onRemove: _thisref.onRemove, onChange: _thisref.onChange})
            );
        });
        return (
            React.createElement("ul", {className: "participantList list-unstyled"}, content)
        );
    }
});
BookingUI.ParticipantForm = React.createClass({displayName: "ParticipantForm",
    getInitialState: function() {
        return {
            participant:this.props.data,
            valid:false
        };
    },
    handleElementChange: function(el,newValue){
        var newParticipant = this.state.participant;
        newParticipant[el.props.dataKey] = newValue;
        this.setState({participant:newParticipant});
        this.props.onChange(this.state.participant);
    },
    handleFormChange: function(el){
        var newobj = this.state.participant;
        newobj.valid = el.state.valid;
        this.setState({participant:newobj});
        this.props.onChange(this.state.participant);
    },
    remove: function(){
        ReactDOM.render(
            React.createElement(WdUi.Modal, {title: "Delete Guest?", wdOnClose: BookingAppSettings.clearModal}, 
                React.createElement("div", {className: "modal-body"}, "Are you sure you want to delete this guest? Doing so will remove any associated information assigned to this guest."), 
                React.createElement("div", {className: "modal-footer"}, 
                    React.createElement("button", {onClick: BookingAppSettings.clearModal, type: "button", className: "btn btn-default", "data-dismiss": "modal"}, "Cancel"), 
                    React.createElement("button", {onClick: this.removeConfirmed, "data-dismiss": "modal", className: "btn btn-primary"}, "Confirm Remove")
                )
            ),document.getElementById(BookingAppSettings.modalContainerID)
        );
    },
    removeConfirmed:function(){
        BookingAppSettings.clearModal();
        var thisRef = this;
        //just so we can clear the modal before the participant is removed.
        setTimeout( function(){thisRef.props.onRemove(thisRef.state.participant)},600);
    },
    render:function(){
        var twinDoubleOptsOpts = [{value:'',label:'Please select'},{value:'Double',label:'Double'},{value:'Twin',label:'Twin (2 single beds)'}];
        var showDoubleTwinQ = false;
        if(BookingAppSettings.participantIsSharing(this.state.participant)){
            showDoubleTwinQ=true;
        }
        return(
            React.createElement("li", {className: "participant"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-sm-12"}, 
                        React.createElement("div", {style: {float:'right'}}, this.props.singleOnly?null:React.createElement("a", {type: "button", onClick: this.remove, className: "btn btn-danger btn-sm"}, React.createElement("i", {className: "fa fa-minus-circle"}), " Remove ", this.state.participant.firstName)), 
                        React.createElement("h3", {className: "no-margin"}, " ", (this.state.participant.firstName?this.state.participant.firstName+'\'s Details':'Guest Details'))
                    )
                ), 
                React.createElement(WdForm, {forceValidation: this.state.participant.changed, wdOnSubmit: this.submit, wdOnChange: this.handleFormChange}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "First Name", value: this.state.participant.firstName, dataKey: "firstName", wdOnChange: this.handleElementChange})), 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "Last Name", value: this.state.participant.lastName, dataKey: "lastName", wdOnChange: this.handleElementChange}))
                    ), 
                    React.createElement("div", {className: "row", style: {display:(this.props.singleOnly?'none':'block')}}, 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Select, {rules: {required:(this.props.singleOnly?false:true)}, options: this.props.shareOpts, label: "Share accommodation with", value: this.state.participant.sharedwith, dataKey: "sharedwith", wdOnChange: this.handleElementChange})), 
                        React.createElement("div", {className: "col-sm-6", style: {display:(showDoubleTwinQ?'block':'none')}}, React.createElement(WdForm.Select, {rules: {required:showDoubleTwinQ}, options: twinDoubleOptsOpts, label: "Double/Twin", value: this.state.participant.doubletwin, dataKey: "doubletwin", wdOnChange: this.handleElementChange}))
                    )
                )
            )
        );
    }
});

BookingUI.ScreenExtras = React.createClass({displayName: "ScreenExtras",
    getInitialState: function() {
        return {
            valid:false,
            accom_errors:[],
            extras_errors:[],
            participants:this.props.data.participants
        };
    },
    setParticipantAccomExtraOption:function(participant,productCode,selected){
        //make accom array if it doesn't exist
        if(participant.accomExtras){
            //already exists
        }else{
            participant.accomExtras=[];
        }

        var newExtras = [];
        //strip any mention of this product code from the selected participant
        for (var i=0; i<participant.accomExtras.length; i++) {
            if(participant.accomExtras[i]!==productCode) {
                newExtras.push(participant.accomExtras[i]);
            }
        }
        //re-add if specified
        if(selected===true){
            newExtras.push(productCode);
        }

        participant.accomExtras = newExtras;
        return participant;
    },
    setParticipantExtraOption:function(participant,productCode,selected){
        //make accom array if it doesn't exist
        if(participant.extras){
            //already exists
        }else{
            participant.extras=[];
        }

        var newExtras = [];
        //strip any mention of this product code from the selected participant
        for (var i=0; i<participant.extras.length; i++) {
            if(participant.extras[i]!==productCode) {
                newExtras.push(participant.extras[i]);
            }
        }
        //re-add if specified
        if(selected===true){
            newExtras.push(productCode);
        }

        participant.extras = newExtras;
        return participant;
    },
    handleAccomChange: function(newStateInfo){
        var i;
        var thisOpt;
        for(i=0;i<this.state.participants.length;i++){
            thisOpt = newStateInfo.updatedOption;
            if(this.state.participants[i].id===thisOpt.key) {
                //only allow a single accom option to be selected per participant
                var newAccom = [];
                if(thisOpt.selected===true){
                    if(newStateInfo.availabilityinfo.seatsAvailable-newStateInfo.availabilityinfo.seatsSelected<=0){
                        BookingAppSettings.scrollToTopError();
                        ReactDOM.render(
                            React.createElement(WdUi.Modal, {title: "None Available", wdOnClose: BookingAppSettings.clearModal}, 
                                React.createElement("div", {className: "modal-body"}, "There are no more rooms of this type available. Please select an alternate accommodation option for ", this.state.participants[i].firstName, "."), 
                                React.createElement("div", {className: "modal-footer"}, 
                                    React.createElement("button", {onClick: BookingAppSettings.clearModal, type: "button", className: "btn btn-primary", "data-dismiss": "modal"}, "OK")
                                )
                            ),document.getElementById(BookingAppSettings.modalContainerID)
                        );
                        break;
                    }else {
                        newAccom = [newStateInfo.availabilityinfo.productCode];
                    }
                }
                this.state.participants[i].accom = newAccom;
            }
        }
        this.validateExtras();
        this.props.onUpdateExtras(this.state.participants);
    },
    handleAccomExtraChange: function(newStateInfo){
        var i;
        var thisOpt;
        for(i=0;i<this.state.participants.length;i++){
            thisOpt = newStateInfo.updatedOption;
            if(this.state.participants[i].id===thisOpt.key) {
                this.state.participants[i] = this.setParticipantAccomExtraOption(this.state.participants[i],newStateInfo.availabilityinfo.productCode,thisOpt.selected);
            }
        }
        this.validateExtras();
        this.props.onUpdateExtras(this.state.participants);
    },
    handleExtraChange: function(newStateInfo){
        var i;
        var thisOpt;
        for(i=0;i<this.state.participants.length;i++){
            thisOpt = newStateInfo.updatedOption;
            if(this.state.participants[i].id===thisOpt.key) {
                this.state.participants[i] = this.setParticipantExtraOption(this.state.participants[i],newStateInfo.availabilityinfo.productCode,thisOpt.selected);
            }
        }
        this.validateExtras();
        this.props.onUpdateExtras(this.state.participants);
    },
    validateExtras:function(){
        //make sure that all users have a room option
        var i,valid;
        var thisPart;
        var a_errors = [];
        var a_validPartIds=[];
        for(i=0;i<this.state.participants.length;i++){
            thisPart = this.state.participants[i];
            valid = false;
            if(thisPart.accom){
                if(thisPart.accom.length>0){
                    a_validPartIds.push(thisPart.id)
                    valid=true;
                }
            }
            //check if this participant is a linked one, if so, ignore
            if(a_validPartIds.indexOf(thisPart.sharedwith)!==-1) {
                valid=true;
            }

            if(valid===false){
                a_errors.push(thisPart.firstName);
            }
        }

        if(this.props.data.availabilityinfo.accom.length>0) {
            this.state.accom_errors = [];
            if (a_errors.length > 0) {
                this.state.accom_errors = [a_errors.join(', ') + ' has no accommodation selected.'];
            }
        }


        if(this.state.accom_errors.length>0){
           this.state.valid = false;
        }else{
            this.state.valid = true;
        }
        this.forceUpdate();
    },
    nextStep:function(){
        this.validateExtras();
        if(this.state.accom_errors.length>0){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                React.createElement(WdUi.Modal, {title: "Accommodation Information Needed", wdOnClose: BookingAppSettings.clearModal}, 
                    React.createElement("div", {className: "modal-body"}, "Please select an accommodation option for each guest."), 
                    React.createElement("div", {className: "modal-footer"}, 
                        React.createElement("button", {onClick: BookingAppSettings.clearModal, type: "button", className: "btn btn-primary", "data-dismiss": "modal"}, "OK")
                    )
                ),document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.props.onSaveExtras(this.state.participants);
        }
    },
    componentDidMount:function(){
        this.validateExtras();
    },
    render:function(){
        var i;
        var cnt=0;
        var accomErrors = this.state.accom_errors.map(function(error) {
            cnt++;
            return (React.createElement("div", {key: cnt}, error));
        });

        var accomUI = null;
        if(this.props.data.availabilityinfo.accom.length>0){
            accomUI =
                React.createElement("div", {className: "formControlWrap"}, 
                    React.createElement(BookingUI.ExtraOptionsList, {onChange: this.handleAccomChange, title: "Accommodation Options", description: "Please select a single accommodation option for each guest for the tour (one selection required per single or double).", data: {
                            extratype:'accom',
                            participants:this.state.participants,
                            availabilityinfo:this.props.data.availabilityinfo.accom}
                        }), 
                    this.state.accom_errors.length>0?React.createElement("div", {className: "el-errors"}, accomErrors):null
                )
        }

        var accomExtrasUI = null;
        if(this.props.data.availabilityinfo.accomExtras.length>0){
            accomExtrasUI =
                React.createElement("div", {className: "formControlWrap"}, 
                    React.createElement(BookingUI.ExtraOptionsList, {onChange: this.handleAccomExtraChange, title: "", description: "Please select any extras required for each guest.", data: {
                        extratype:'accomExtras',
                        participants:this.state.participants,
                        availabilityinfo:this.props.data.availabilityinfo.accomExtras}
                    })
                )
        }

        var extrasUI = null;
        if(this.props.data.availabilityinfo.extras.length>0){
            extrasUI =
                React.createElement("div", {className: "formControlWrap"}, 
                    React.createElement(BookingUI.ExtraOptionsList, {onChange: this.handleExtraChange, title: "", description: "", data: {
                        extratype:'extra',
                        participants:this.state.participants,
                        availabilityinfo:this.props.data.availabilityinfo.extras
                    }})
                )
        }

        return(
            React.createElement("div", null, 
                React.createElement("h2", null, "Tour Extras"), 
                accomUI, 
                accomExtrasUI, 
                React.createElement("hr", null), 
                extrasUI, 
                React.createElement("div", {className: "col-xs-12 text-right"}, React.createElement("div", {style: {display:'inline-block'}, onClick: this.nextStep}, React.createElement("a", {className: "btn btn-primary", disabled: this.state.valid?"":"disabled"}, "Continue")))
            )
        );
    }
});
BookingUI.ExtraOptionsList = React.createClass({displayName: "ExtraOptionsList",
    onChange:function(switchState){
        this.props.onChange(switchState);
    },
    render:function(){
        var _thisref = this;
        var content = this.props.data.availabilityinfo.map(function(option) {
            var participants = _thisref.props.data.participants;
            return (
                React.createElement(BookingUI.ExtraOption, {key: option.id, data: {
                    extratype:_thisref.props.data.extratype,
                    participants:participants,
                    availabilityinfo:option
                }, onChange: _thisref.onChange})
            );
        });
        return(
            React.createElement("div", null, 
                React.createElement("h3", null, this.props.title), 
                React.createElement("p", null, this.props.description), 
                React.createElement("ul", {className: "participantList list-unstyled"}, content)
            )
        );
    }
});
BookingUI.ExtraOption = React.createClass({displayName: "ExtraOption",
    onChange:function(switchState){
        this.props.onChange({availabilityinfo:this.props.data.availabilityinfo,updatedOption:switchState});
    },
    isAccomOptSelected:function(participant){
        if(participant.accom && participant.accom.indexOf(this.props.data.availabilityinfo.productCode)!==-1) {
            return true;
        }
        return false;
    },
    isAccomExtraOptSelected:function(participant){
        if(participant.accomExtras && participant.accomExtras.indexOf(this.props.data.availabilityinfo.productCode)!==-1) {
            return true;
        }
        return false;
    },
    isExtraOptSelected:function(participant){
        if(participant.extras && participant.extras.indexOf(this.props.data.availabilityinfo.productCode)!==-1) {
            return true;
        }
        return false;
    },
    render:function(){
        var switcherGroupData = [];
        var thisPart;
        var i,j;
        var shouldAdd;
        var thisPrice;
        if(this.props.data.extratype==='extra') {
            for (i = 0; i < this.props.data.participants.length; i++) {
                thisPart = this.props.data.participants[i];
                switcherGroupData.push({key:thisPart.id,value: thisPart.id, label: thisPart.firstName, isDisabled:thisPart.isDisabled, selected:this.isExtraOptSelected(thisPart)});
            }
        }else if(this.props.data.extratype==='accom' || this.props.data.extratype==='accomExtras'){
            for (i = 0; i < this.props.data.participants.length; i++) {
                thisPart = this.props.data.participants[i];
                if(this.props.data.extratype==='accomExtras') {
                    var b_isSelected = this.isAccomExtraOptSelected(thisPart);
                }else{
                    var b_isSelected = this.isAccomOptSelected(thisPart);
                }
                if(BookingAppSettings.participantIsSharing(thisPart)===false){
                    switcherGroupData.push({key:thisPart.id, value: thisPart.id, label: thisPart.firstName, isDisabled:thisPart.isDisabled, selected:b_isSelected});
                }else{
                    //make sure reversed shared user does not get added
                    shouldAdd=true;
                    for(j=0;j<switcherGroupData.length;j++){
                        if(switcherGroupData[j].value===thisPart.sharedwith){
                            shouldAdd = false;
                        }
                    }
                    if(shouldAdd) {
                        var joinedUserLabel = BookingAppSettings.getSharedParticipantLabel(thisPart,this.props.data.participants);
                        switcherGroupData.push({key:thisPart.id, value: thisPart.id, label: joinedUserLabel, isDisabled:thisPart.isDisabled, selected:b_isSelected});
                    }
                }
            }
        }


        var b_combineTwinDouble=false;
        var n_twinPrice=false;
        var n_doublePrice=false;
        for (i = 0; i<this.props.data.availabilityinfo.priceOptions.length; i++){
            thisPrice = this.props.data.availabilityinfo.priceOptions[i];
            if(thisPrice.label=='Twin'){
                n_twinPrice = thisPrice.price;
            }else if(thisPrice.label=='Double'){
                n_doublePrice = thisPrice.price;
            }
        }
        if(n_twinPrice && n_twinPrice===n_doublePrice){
            b_combineTwinDouble=true;
        }
        var a_priceStrChunks = [];
        for (i = 0; i<this.props.data.availabilityinfo.priceOptions.length; i++){
            thisPrice = this.props.data.availabilityinfo.priceOptions[i];
            if(b_combineTwinDouble && thisPrice.label==='Double'){
                continue; //skip if we are combining twin/double.
            }
            if(b_combineTwinDouble && thisPrice.label==='Twin') {
                a_priceStrChunks.push(WdTools.priceFormat(thisPrice.price/2) + ' Double/Twin pp');
            }else{
                a_priceStrChunks.push(WdTools.priceFormat(thisPrice.price) + ' ' + thisPrice.label);
            }
        }
        var priceStr = a_priceStrChunks.join(', ');


        var availabilityMsg = null;
        if(this.props.data.extratype==='accom'){
            var accomLeft = this.props.data.availabilityinfo.seatsAvailable-this.props.data.availabilityinfo.seatsSelected;
            availabilityMsg = React.createElement("em", null, "(", accomLeft, " available)");
        }
        return(
            React.createElement("li", {className: "extra-opt"}, 
                React.createElement("div", {className: "extra-name"}, this.props.data.availabilityinfo.product.name, " ", availabilityMsg), 
                React.createElement("div", {className: "extra-price"}, priceStr), 
                React.createElement("div", {className: "extra-desc"}, this.props.data.availabilityinfo.product.shortDescription), 
                React.createElement(WdForm.SwitcherGroup, {data: switcherGroupData, onChange: this.onChange})
            )
        );
    }
});

BookingUI.ScreenDetails = React.createClass({displayName: "ScreenDetails",
    getInitialState: function() {
        return {
            valid:false,
            forceValidate:false,
            details:this.props.data
        };
    },

    handleElementChange: function(el,newValue){
        var details = this.state.details;
        details[el.props.dataKey] = newValue;
        this.state.details = details;
        this.props.onChange(this.state.details);
    },
    handleFormChange: function(el){
        this.setState({valid:el.state.valid});
    },
    nextStep:function(){
        this.setState({forceValidate:true});
        if(this.state.valid===false){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                React.createElement(WdUi.Modal, {title: "More detail required", wdOnClose: BookingAppSettings.clearModal}, 
                    React.createElement("div", {className: "modal-body"}, "Please enter all required booking details."), 
                    React.createElement("div", {className: "modal-footer"}, 
                        React.createElement("button", {onClick: BookingAppSettings.clearModal, type: "button", className: "btn btn-primary", "data-dismiss": "modal"}, "OK")
                    )
                ),document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.props.onSave(this.state.details);
        }
    },
    render:function(){
        return(
            React.createElement("div", null, 
                React.createElement("h2", null, "Contact Details For This Booking"), 
                React.createElement("p", null, "Please enter the contact details for the person making this booking. If you are making a booking for more than one person you will be asked to add their names on the next screen."), 
                React.createElement(WdForm, {forceValidation: this.state.forceValidate, wdOnSubmit: this.nextStep, wdOnChange: this.handleFormChange}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-12"}, React.createElement("h3", null, "Contact Information")), 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "First Name", value: this.state.details.firstName, dataKey: "firstName", wdOnChange: this.handleElementChange})), 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "Last Name", value: this.state.details.lastName, dataKey: "lastName", wdOnChange: this.handleElementChange}))
                    ), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true,email:true}, type: "text", label: "Email", value: this.state.details.email, dataKey: "email", wdOnChange: this.handleElementChange})), 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "Preferred Phone Number", value: this.state.details.phone, dataKey: "phone", wdOnChange: this.handleElementChange}))
                    ), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-12"}, React.createElement("hr", null), React.createElement("h3", null, "Address")), 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "Address", value: this.state.details.address, dataKey: "address", wdOnChange: this.handleElementChange})), 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "City", value: this.state.details.city, dataKey: "city", wdOnChange: this.handleElementChange}))
                    ), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-4"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "State", value: this.state.details.state, dataKey: "state", wdOnChange: this.handleElementChange})), 
                        React.createElement("div", {className: "col-sm-4"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "Postcode", value: this.state.details.postcode, dataKey: "postcode", wdOnChange: this.handleElementChange})), 
                        React.createElement("div", {className: "col-sm-4"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "Country", value: this.state.details.country, dataKey: "country", wdOnChange: this.handleElementChange}))
                    ), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-12"}, 
                            React.createElement("hr", null), 
                            React.createElement("p", null, "Please let us know how you found out about Mulga Bicycle Tours and this tour specifically."), 
                            React.createElement(WdForm.TextArea, {type: "text", showLabel: false, label: "How did you hear about this tour?", value: this.state.details.howhear, dataKey: "howhear", wdOnChange: this.handleElementChange}))
                    ), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-xs-12 text-right"}, React.createElement("div", {style: {display:'inline-block'}, onClick: this.nextStep}, React.createElement("a", {className: "btn btn-primary", disabled: this.state.valid?"":"disabled"}, "Continue")))
                    )
                )

            )
        );
    }
});

BookingUI.ScreenPayment = React.createClass({displayName: "ScreenPayment",
    getInitialState: function() {
        //set deposit amount
        var depositAmount = BookingAppSettings.depositPerParticipant*this.props.appState.participants.length;
        var currDetails = this.props.data;
        currDetails.depositamount = depositAmount;
        currDetails.paymenttype = '';
        return {
            valid:false,
            forceValidate:false,
            details:currDetails,

            paymentProcessing:false,
            paymentTitle:false,
            paymentSubTitle:false,
            paymentFinished:false,
            paymentFinishedError:false,
            appState:this.props.appState,
            participants:this.props.appState.participants,
            bookingCosts:this.props.bookingCosts
        };
    },

    handleElementChange: function(el,newValue){
        var details = this.state.details;
        details[el.props.dataKey] = newValue;
        this.state.details = details;
        this.props.onChange(this.state.details);
    },
    handlePaymentTypeChange: function(el,newValue){
        this.state.details.paymenttype=newValue;
        this.forceUpdate();
    },
    handleFormChange: function(el){
        this.setState({valid:el.state.valid});
    },
    payDataCheck:function(data){
        var b_fail = false;
        if(this.state.participants.length>data.tour.seatsAvailable){
            this.setState({
                paymentTitle:'Required Tour Places Unavailable',
                paymentSubTitle:'You have attempted to book '+this.state.participants.length+' guests on the tour. '+data.tour.seatsAvailable+' places are available. Please try your booking again.'
            });
            b_fail=true;
        }else{
            //check accommodation
            var i,j;
            var thisPart;
            var thisAccom;
            var cnt;
            for(i=0;i<data.accom.length;i++){
                thisAccom = data.accom[i];
                cnt=0;
                for(j=0;j<this.state.participants.length;j++){
                    thisPart = this.state.participants[j];
                    if(thisPart.accom){
                        if(thisPart.accom.indexOf(thisAccom.productCode)){
                            cnt++;
                        }
                    }
                }
                if(cnt>thisAccom.seatsAvailable){
                    this.setState({
                        paymentTitle:'Required Accommodation Places Unavailable',
                        paymentSubTitle:'You have attempted to book '+cnt+' guests in '+thisAccom.product.name+'. Only '+thisAccom.seatsAvailable+' places are now available. Please modify the number of guests and try your booking again.',
                        paymentWaiting:false
                    });
                    b_fail=true;
                    break;
                }
            }
        }
        if(b_fail===false){
            this.ajaxCompletePay();
        }
    },
    completePayment:function(result){
        var success = false;
        if(result.paymentInfo){
            if(result.paymentInfo.status==='paid'){
                success = true;
            }
        }
        //hide navigation so user cannot go and mess with the same order.
        if(success){
            $('.steps.before.list-unstyled').hide();
            this.setState({
                paymentWaiting:false,
                paymentTitle:'Payment Complete',
                paymentSubTitle:'Transaction Number: '+result.paymentInfo.transactionNumber+' with reference '+result.paymentInfo.transactionRef+' completed successfully. You will receive an email confirmation at '+this.state.appState.bookingDetails.email+' containing your order and payment information.',
                paymentFinished:true,
                paymentFinishedError:false
            });
        }else{
            if(result.paymentInfo.status==='error'){
                this.setState({
                    paymentWaiting: false,
                    paymentTitle: 'Payment Error',
                    paymentSubTitle: result.paymentInfo.error.join(', '),
                    paymentFinished: true,
                    paymentFinishedError: true,
                });
            }else {
                $('.steps.before.list-unstyled').hide();
                this.setState({
                    paymentWaiting: false,
                    paymentTitle: 'Payment Error',
                    paymentSubTitle: 'Your payment or booking failed for an unknown reason. Please contact us to check the status of your booking/payment.',
                    paymentFinished: true,
                    paymentFinishedError: true,
                });
            }
        }
    },
    ajaxCompletePay:function(){

        this.setState({
            paymentTitle:'Booking Verified',
            paymentSubTitle:'Please wait, your payment is now being processed... '
        });
        var _thisRef = this;
        var dataToSend = {
            tour:this.props.appProps,
            cc_details:this.state.details,
            participants:this.state.appState.participants,
            bookingDetails:this.state.appState.bookingDetails,
            bookingCosts:this.state.bookingCosts,
            promotionCode:this.props.promotionInfo.couponCode,
            availabilityinfo:this.state.appState.availabilityinfo
        };

        $.ajax({
            type: "POST",
            url: '/tour/pay',
            data: dataToSend,
            success: function (result) {
                _thisRef.completePayment(result);
            }
        });
    },
    pay:function(){
        this.setState({forceValidate:true});
        if(this.state.details.paymenttype=='creditcard' && this.state.valid===false){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                React.createElement(WdUi.Modal, {title: "More detail required", wdOnClose: BookingAppSettings.clearModal}, 
                    React.createElement("div", {className: "modal-body"}, "Please enter all required payment details."), 
                    React.createElement("div", {className: "modal-footer"}, 
                        React.createElement("button", {onClick: BookingAppSettings.clearModal, type: "button", className: "btn btn-primary", "data-dismiss": "modal"}, "OK")
                    )
                ),document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.setState({
                paymentProcessing:true,
                paymentWaiting:true,
                paymentTitle:'Checking Booking Details',
                paymentSubTitle:'Please wait while we verify your booking... '
            });
            var _thisRef = this;
            $.ajax({
                type: "POST",
                url: '/tour/bookajax',
                data: this.props.appProps,
                success: function (data) {
                    _thisRef.payDataCheck(data);
                }
            });
            //this.props.onSave(this.state.details);
        }
    },
    popupCvnModal:function(){
        ReactDOM.render(
            React.createElement(WdUi.Modal, {title: "What is CVN (CVV2/CVC2)", wdOnClose: BookingAppSettings.clearModal}, 
                React.createElement("div", {className: "modal-body"}, 
                    React.createElement("p", null, "The CVN code is a special 3-4 digit code used as a security measure for all Internet transactions. It is called CVV2 (Credit card Verification Value) for Visa cards and CVC2 (Credit card Verification Code) for MasterCard."), 
                    React.createElement("p", null, "Since the CVN is listed on your credit card, but is not stored anywhere, the only way to know the correct number for your credit card is to physically have possession of the card itself. Because of this, it is a security enhancement against misuse of credit cards. All VISA, MasterCard and American Express cards newer than 5 years have a CVN."), 
                    React.createElement("p", null, React.createElement("strong", null, "Note:"), " The CVN code is printed on your card; it is not raised from the surface of the card like your credit card's account number.")
                ), 
                React.createElement("div", {className: "modal-footer"}, 
                    React.createElement("button", {onClick: BookingAppSettings.clearModal, type: "button", className: "btn btn-primary", "data-dismiss": "modal"}, "OK")
                )
            ),document.getElementById(BookingAppSettings.modalContainerID)
        );
    },
    tryPaymentAgain:function(){
        this.setState({paymentProcessing:false});
    },
    render:function(){
        var i;
        var creditCardOpts = [
            {value:'',label:'Please select...'},
            {value:'Visa',label:'Visa'},
            {value:'Mastercard',label:'Mastercard'}
        ];
        var monthCardOpts = [
            {value:'',label:'Select Month...'},
            {value:'01',label:'01'},
            {value:'02',label:'02'},
            {value:'03',label:'03'},
            {value:'04',label:'04'},
            {value:'05',label:'05'},
            {value:'06',label:'06'},
            {value:'07',label:'07'},
            {value:'08',label:'08'},
            {value:'09',label:'09'},
            {value:'10',label:'10'},
            {value:'11',label:'11'},
            {value:'12',label:'12'}
        ];

        var yearsToShow = 20;
        var currYear = new Date().getFullYear();
        var yearCardOpts = [
            {value:'',label:'Select Year...'},
        ];
        for(i=0;i<yearsToShow;i++){
            thisYear = currYear+i;
            yearCardOpts.push({value:thisYear-2000,label:thisYear});
        }

        var endNavigation=null;
        if(this.state.paymentFinishedError===true) {
            if($('.steps.before.list-unstyled').is(":visible")){
                endNavigation = React.createElement("a", {href: "javascript:;", onClick: this.tryPaymentAgain, className: "btn btn-primary"}, "Try Payment Again");
            }else {
                endNavigation = React.createElement("a", {href: "/contact", className: "btn btn-primary"}, "An Error Occurred - Please Contact Us");
            }
        }else if(this.state.paymentFinished===true){
            endNavigation = React.createElement("a", {href: "/tours", className: "btn btn-primary"}, "Back to Tours");
        }

        return(
            React.createElement("div", null, 
                React.createElement("div", {style: {display:this.state.paymentProcessing?'block':'none'}}, 
                    React.createElement("h2", null, this.state.paymentTitle), 
                    React.createElement("p", null, this.state.paymentSubTitle, " ", this.state.paymentWaiting?React.createElement("i", {className: "fa fa-refresh fa-spin"}):null), 
                    endNavigation
                ), 

                React.createElement("div", {style: {display:this.state.paymentProcessing?'none':'block'}}, 
                    React.createElement("h2", null, "Payment Details"), 
                    React.createElement("div", {className: "paymentAmount"}, 
                        React.createElement("div", {className: "main"}, "Booking fee: ", React.createElement("span", null, WdTools.priceFormat(this.state.details.depositamount), " incl GST")), 
                        React.createElement("div", {className: "subline"}, WdTools.priceFormat(BookingAppSettings.depositPerParticipant), " per guest")
                    ), 
                    React.createElement("hr", null), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-xs-12"}, 
                            React.createElement("h3", null, "Payment Method"), 
                            React.createElement(WdForm.Select, {rules: {required:true}, options: [
                                {value:'',label:'Please select...'},
                                {value:'creditcard',label:'Credit Card'},
                                {value:'banktransfer',label:'Bank Transfer'}
                            ], label: "Please select your method of payment", value: this.state.details.paymenttype, dataKey: "cardtype", wdOnChange: this.handlePaymentTypeChange})
                        ), 
                        React.createElement("div", {style: {display:this.state.details.paymenttype==''?'block':'none'}, className: "col-xs-12 text-right"}, React.createElement("div", {style: {display:'inline-block'}}, React.createElement("a", {className: "btn btn-primary", disabled: "disabled"}, "Book Now")))
                    ), 
                    React.createElement("div", {style: {display:this.state.details.paymenttype=='creditcard'?'block':'none'}}, 
                        React.createElement("div", {id: "eWAYBlock", style: {float:'right', margin:'0px 0px 10px 10px'}}, 
                            React.createElement("a", {href: "http://www.eway.com.au/secure-site-seal?i=11&s=3&pid=5ff76b25-0692-4ddf-8186-f852ca7b9b5e", title: "eWAY Payment Gateway", target: "_blank", rel: "nofollow"}, 
                                React.createElement("img", {alt: "eWAY Payment Gateway", src: "https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&size=3&pid=5ff76b25-0692-4ddf-8186-f852ca7b9b5e"})
                            )
                        ), 
                        React.createElement("h3", null, "Pay by Credit Card"), 
                        React.createElement("p", null, "Please enter your credit card information below to immediately make payment."), 
                        React.createElement("p", null, "Your credit card details are immediately encrypted and processed directly by eWAY. At no point does this site store your card details."), 

                        React.createElement(WdForm, {forceValidation: this.state.forceValidate, wdOnSubmit: this.nextStep, wdOnChange: this.handleFormChange}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Select, {rules: {required:true}, options: creditCardOpts, label: "Card Type", value: this.state.details.cardtype, dataKey: "cardtype", wdOnChange: this.handleElementChange})), 
                                React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "Card Name", value: this.state.details.cardname, dataKey: "cardname", wdOnChange: this.handleElementChange}))
                            ), 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true,isNumeric:true,minLength:14,maxLength:16}, type: "text", label: "Card Number", value: this.state.details.cardnumber, dataKey: "cardnumber", wdOnChange: this.handleElementChange})), 
                                React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true,isNumeric:true,minLength:3,maxLength:4}, type: "text", label: "CVN", value: this.state.details.cvn, dataKey: "cvn", wdOnChange: this.handleElementChange}), React.createElement("a", {href: "javascript:;", onClick: this.popupCvnModal}, "What is a CVN?"))

                            ), 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Select, {rules: {required:true}, options: monthCardOpts, label: "Expiry Month", value: this.state.details.expirymonth, dataKey: "expirymonth", wdOnChange: this.handleElementChange})), 
                                React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Select, {rules: {required:true}, options: yearCardOpts, label: "Expiry Year", value: this.state.details.expiryyear, dataKey: "expiryyear", wdOnChange: this.handleElementChange}))
                            ), 

                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-xs-12 text-right"}, React.createElement("div", {style: {display:'inline-block'}, onClick: this.pay}, React.createElement("a", {className: "btn btn-primary", disabled: this.state.valid?"":"disabled"}, "Book and Pay Now via Credit Card")))
                            )
                        )
                    ), 

                    React.createElement("div", {style: {display:this.state.details.paymenttype=='banktransfer'?'block':'none'}}, 
                        React.createElement("h3", null, "Pay by Bank Transfer"), 
                        React.createElement("p", null, "Please transfer the required booking fee of ", React.createElement("strong", null, WdTools.priceFormat(this.state.details.depositamount)), " to the bank account with details shown below."), 
                        React.createElement("p", null, "If payment is not received within ", React.createElement("strong", null, "72 hours"), ", your booking may be cancelled."), 
                        React.createElement("p", null, React.createElement("strong", null, "Reference:"), React.createElement("br", null), React.createElement("span", null, "When making your bank transfer payment, please use ", React.createElement("strong", null, "\"", React.createElement("em", null, this.state.appState.bookingDetails.lastName), "\""), " as your reference.")), 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-sm-6"}, React.createElement("strong", null, "Bank Name:")), 
                            React.createElement("div", {className: "col-sm-6"}, "Commonweatlth Bank")
                        ), 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-sm-6"}, React.createElement("strong", null, "BSB:")), 
                            React.createElement("div", {className: "col-sm-6"}, "062907")
                        ), 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-sm-6"}, React.createElement("strong", null, "Account Number:")), 
                            React.createElement("div", {className: "col-sm-6"}, "10352850")
                        ), 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-sm-6"}, React.createElement("strong", null, "Account Name:")), 
                            React.createElement("div", {className: "col-sm-6"}, "Multilocus Interactive Pty Ltd trading as Mulga Bicycle Tours")
                        ), 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-xs-12 text-right"}, React.createElement("div", {style: {display:'inline-block'}, onClick: this.pay}, React.createElement("a", {className: "btn btn-primary"}, "Book Now")))
                        )
                    )
                )
            )
        );
    }
});

BookingUI.ScreenTerms = React.createClass({displayName: "ScreenTerms",
    /*
     <h2>Insurance</h2>
     <p>We recommend that you purchase travel insurance that provides coverage for the cancellation of the tour, and covers costs incurred by you like transportation costs (for example, airfares), stolen goods, lost luggage, damaged property, medical expenses, and cancellation fees.</p>
     <p>We recommend that you hold current ambulance service membership and/or health or travel insurance. If you become ill or injured during the tour, we will endeavour to organise medical transportation for you but at your cost.</p>
     <p>We also strongly recommend you have Cyclist Insurance. This can be purchased separately and also comes with some cycling club memberships such as Bicycle Network, Bicycle NSW, Bicycle Queensland and Pedal Power (ACT). Cyclist insurance can cover you and your bike if you have an accident while riding.</p>
     */
    getInitialState: function() {
        return {
            couponExtended:false,
            valid:false,
            forceValidate:false,

            gettingDiscount:false,
            discountError:true,
            discountMessage:true,

            details:this.props.termsDetails
        };
    },
    handleElementChange: function(el,newValue){
        var details = this.state.details;
        details[el.props.dataKey] = newValue;
        this.state.details = details;
        this.props.onChange(this.state.details);
    },
    handleFormChange: function(el){
        this.setState({valid:el.state.valid});
    },
    nextStep:function(){
        this.setState({forceValidate:true});
        if(this.state.valid===false){
            BookingAppSettings.scrollToTopError();
            ReactDOM.render(
                React.createElement(WdUi.Modal, {title: "Agreement Needed", wdOnClose: BookingAppSettings.clearModal}, 
                    React.createElement("div", {className: "modal-body"}, "Please agree to all terms and conditions."), 
                    React.createElement("div", {className: "modal-footer"}, 
                        React.createElement("button", {onClick: BookingAppSettings.clearModal, type: "button", className: "btn btn-primary", "data-dismiss": "modal"}, "OK")
                    )
                ),document.getElementById(BookingAppSettings.modalContainerID)
            );
        }else{
            this.props.onSave(this.state.details);
        }
    },
    handlePromotionCodeValChange:function(el,newValue){
        var details = this.state.details;
        details[el.props.dataKey] = newValue;
        this.setState({details: details});
    },
    handlePromotionCodeChange:function(){
        this.state.gettingDiscount = true;
        this.setState({gettingDiscount: true});

        var _thisRef = this;
        $.ajax({
            type: "POST",
            url: '/tour/getpromotioninfoajax',
            data: {tourId:this.props.appProps.tourid,tourStarttime:this.props.appProps.starttime,promotionCode:this.state.details.promotionCode},
            success: function (data) {
                var details = _thisRef.state.details;
                var showError = false;
                if(data.promotionInfo===false){
                    showError = true;
                }
                _thisRef.setState({gettingDiscount: false,discountError:showError,discountMessage:data.message});
                _thisRef.props.onDiscountChange(data.promotionInfo);
            }
        });
    },
    extendCoupon:function(){
        this.state.couponExtended=true;
        this.setState({couponExtended: true});
    },
    render:function(){
        //discount message
        discountMessage = null;
        if(this.state.discountMessage!==false){
            if(this.state.discountError===true){
                discountMessage = React.createElement("p", {style: {fontSize:'90%', color:'#ff0000'}}, this.state.discountMessage)
            }else{
                discountMessage = React.createElement("p", {style: {fontSize:'90%',color:'#449d44'}}, this.state.discountMessage)
            }
        }

        return(
            React.createElement("div", null, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-12"}, 
                        React.createElement("h2", null, "Terms and Conditions"), 
                        React.createElement("p", null, "Here's the formal part of the booking process. We need to bring this to your attention first so we can then get on with organising the fun stuff! It includes links to documents that you will need to complete. You do not have to do this now, we will send these documents to you in our email confirming that we have received your booking request."), 
                        React.createElement("p", null, "Acceptance of your booking is subject to Mulga Bicycle Tours receiving a signed ", React.createElement("a", {target: "_blank", href: this.props.appProps.waiverForm}, "Waiver And Assumption Of Risk Form"), " and a completed ", React.createElement("a", {target: "_blank", href: this.props.appProps.personalInfoForm}, "Guest Personal Information Form"), " for each member of your party indicating acceptance of our Terms and Conditions, Safety Guidelines, Code of Conduct and Privacy Policy. These documents are available at the bottom of all our web pages."), 
                        React.createElement(WdForm, {forceValidation: this.state.forceValidate, wdOnChange: this.handleFormChange}, 
                            React.createElement(WdForm.Checkbox, {rules: {required:true}, errorText: "You must indicate your agreement to the above before continuing.", label: "I acknowledge and agree to the conditions set out in the Waiver And Assumption Of Risk Form and undertake to email or post to Mulga Bicycle Tours a signed Waiver And Assumption Of Risk Form and a completed Guest Personal Information Form for each person that I am making the booking for.", checked: this.state.details.termsagree?true:false, valueIfSelected: "Agreed", dataKey: "termsagree", wdOnChange: this.handleElementChange}), 
                            React.createElement(WdForm.Checkbox, {rules: {required:true}, errorText: "You must indicate your agreement to the above before continuing.", label: "I agree to read all tour information sent to me by Mulga Bicycle Tours", checked: this.state.details.termsagreefuture?true:false, valueIfSelected: "Agreed", dataKey: "termsagreefuture", wdOnChange: this.handleElementChange})
                        ), 

                        React.createElement("div", {className: "col-xs-12"}, 
                            React.createElement("div", {style: {display:this.state.couponExtended?'none':'block'}, className: "coupon-hide-link"}, React.createElement("a", {href: "javascript:;", onClick: this.extendCoupon}, "Add a Gift Card or Member Benefit Coupon +")), 
                            React.createElement("div", {style: {display:this.state.couponExtended?'block':'none'}}, 
                                React.createElement("h3", null, "Add a Gift Card or Member Benefit coupon"), 
                                React.createElement("p", null, "If you have a gift card or coupon enter the code here. If the code is still current we will display your discount as you make your choices on the following screens."), 
                                React.createElement("div", {className: "row"}, 
                                    React.createElement("div", {className: "col-sm-4"}, 
                                        React.createElement(WdForm.Input, {type: "text", label: "Enter code", showLabel: false, value: this.state.details.promotionCode, dataKey: "promotionCode", wdOnChange: this.handlePromotionCodeValChange})
                                    ), 
                                    React.createElement("div", {className: "col-sm-4"}, 
                                        React.createElement("a", {className: "btn btn-info", onClick: this.handlePromotionCodeChange}, "Apply Discount")
                                    )

                                ), 
                                this.state.gettingDiscount===true?React.createElement("span", null, "Please wait.. retrieving discount information... ", React.createElement("i", {className: "fa fa-refresh fa-spin"})):null, 
                                discountMessage
                            )
                        ), 


                        React.createElement("div", {className: "text-right"}, React.createElement("div", {style: {display:'inline-block'}, onClick: this.nextStep}, React.createElement("a", {className: "btn btn-primary", disabled: this.state.valid?"":"disabled"}, "Continue")))
                    )
                )
            )
        );
    }
});


