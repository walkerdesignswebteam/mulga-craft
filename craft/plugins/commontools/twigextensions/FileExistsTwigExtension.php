<?php
//{{ fileExists('/imgfolder/test.jpg') }}

namespace Craft;
use Twig_Extension;
use Twig_SimpleFunction;
class FileExistsTwigExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'FileExists';
    }

    public function fileExists($file)
    {
        if(file_exists($_SERVER['DOCUMENT_ROOT'].$file)){
            return true;
        }else{
            return false;
        }
    }

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('fileExists', array($this, 'fileExists')),
        );
    }

}