<?php
namespace Craft;

use QuickCart\Cart;
use QuickCart\CartItem;

class QuickCart_ProductController extends QuickCart_BaseController
{
    protected $allowAnonymous = array('actionShow');

    public function actionShow(array $variables = array())
    {
        $o_product = QuickCart_ProductRecord::model()->findByAttributes(array('code'=>$variables['productCode']));
        if($o_product) {
            $o_item =  new CartItem();

            echo $this->getQuickCartTemplate('product', array(
                'routes'=>Cart::getRoutes(),
                'product'=>$o_product)
            );
        }else{
            throw new HttpException(404,'No product found with code "'.$variables['productCode'].'".');
        }
    }
}