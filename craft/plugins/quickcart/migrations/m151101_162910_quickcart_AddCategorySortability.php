<?php
namespace Craft;

/**
 * The class name is the UTC timestamp in the format of mYYMMDD_HHMMSS_pluginHandle_migrationName
 */
class m151101_162910_quickcart_AddCategorySortability extends BaseMigration
{
	/**
	 * Any migration code in here is wrapped inside of a transaction.
	 *
	 * @return bool
	 */
	public function safeUp()
	{
		$this->addColumnAfter('quickcart_category', 'order',  array(ColumnType::Int, 'default'=>0), 'description');
		return true;
	}
}
