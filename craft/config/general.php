<?php
ini_set('memory_limit','512M');
// Ensure our urls have the right scheme
define('URI_SCHEME',  ( isset($_SERVER['HTTPS'] ) ) ? "https://" : "http://" );
define('SITE_URL',    URI_SCHEME . $_SERVER['SERVER_NAME'] . '/');


/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */
$b_isPageSpeedHit=false;
if(strpos($_SERVER['HTTP_USER_AGENT'],'Google Page Speed Insights')!==false){
    $b_isPageSpeedHit=true;
}
return array(
    '*' => array(
        'siteName' => array(
            'en' => 'Mulga Bicycle Tours',
        ),
        'siteUrl' => array(
            'en' => SITE_URL,
        ),
        'imageDriver'=>'gd',
        'cpTrigger' => 'craftadmin',
        'timezone' => 'Australia/Hobart',
		'omitScriptNameInUrls' => true, //sometimes index.php ends up in urls for no reason it seems.. this forces it to use .htaccess
        'extraAllowedFileExtensions' => 'kml,kmz',
        'isPageSpeedHit'=>$b_isPageSpeedHit
    ),
    'mulgabicycletours.dev.walkerdesigns.com.au' => array(
        'devMode' => true,
    ),
);
