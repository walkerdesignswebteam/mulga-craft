<?php
namespace Craft;
class QuickCart_CategoryProductRecord extends BaseRecord
{
	public function getTableName()
    {
        return 'quickcart_category_product';
    }

    protected function defineAttributes()
    {
        return array(
			'category_id' => AttributeType::Number,
			'product_id' => AttributeType::Number,
        );
    }
	public function defineRelations()
    {
        return array( 
			'category_id' => array(static::HAS_MANY, 'QuickCart_Category', 'id'),
			'product_id' => array(static::HAS_MANY, 'QuickCart_Product', 'id'),
        );
    }
}