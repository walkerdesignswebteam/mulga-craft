//MIXINS 
var WdformMixin = {
    getInitialState: function() {
        return {valid:false,hasChanged:false}
    },
    componentWillMount: function() {
        if(this.props.callbackElementAttach){
            this.props.callbackElementAttach(this);
        }
    },
    componentDidMount:function(){
        //validate for the first time
        this.validate(this.props.value);
    },

    //here we can re-run validation if rules and values change dynamically
    //careful here. this is a good place to cause endless loops
    componentDidUpdate: function(prevprops) {
        //if(this.props.value) {
            if (this.props.value !== prevprops.value) {
                this.validate(this.props.value);
            }
       // }
        //force validate if rules change
        if(this.props.rules) {
            if (this.props.rules && (this.props.rules.required !== prevprops.rules.required)){
                this.validate(this.props.value);
            }
        }
    },

    validate:function(newVal){
        var stateIsValid = true;
        this.a_errors = [];
        this.s_errors = '';
        var errId=0;
        if(this.props.rules){
            //required
            if(this.props.rules.required){
                if(validator.isNull(newVal)){
                    stateIsValid = false;

                    var s_error = this.props.label+' is required';
                    if(this.props.errorText){
                        s_error = this.props.errorText;
                    }
                    this.a_errors.push({id:errId,message:s_error});
                    errId++;
                }
            }

            //ensure is numeric
            if(this.props.rules.isNumeric){
                if(validator.isNull(newVal)===false && validator.isNumeric(newVal)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' must be numeric'});
                    errId++;
                }
            }

            //minLength
            if(this.props.rules.minLength){
                if(validator.isNull(newVal)===false && validator.isLength(newVal,this.props.rules.minLength)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' must be at least '+this.props.rules.minLength+' characters'});
                    errId++;
                }
            }
            //maxLength
            if(this.props.rules.maxLength){
                if(validator.isNull(newVal)===false && validator.isLength(newVal,0,this.props.rules.maxLength)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' can only contain up to '+(this.props.rules.maxLength)+' characters'});
                    errId++;
                }
            }

            //email
            if(this.props.rules.email){
                if(validator.isNull(newVal)===false && validator.isEmail(newVal)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' is not a valid email address'});
                    errId++;
                }
            }
            //urls
            if(this.props.rules.url){
                if(validator.isNull(newVal)===false && validator.isURL(newVal)===false){
                    stateIsValid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' is not a valid url'});
                    errId++;
                }
            }
        }
        this.labelErrors = this.a_errors.map(function(error) {
            return (
                <span className="suberror" key={error.id}>{error.message}</span>
            );
        });
        this.state.valid = stateIsValid;
        this.forceUpdate(); //take this away... and all goes to hell :) Not 100% sure why I need this but the screen doesn't update unless we do.

        //fire event if specified
        if(this.props.wdOnChange){
            this.props.wdOnChange(this,newVal);
        }

        //callback up the form for validation listening
        if(this.props.callbackElementValueChange) {
            this.props.callbackElementValueChange(this);
        }
    },
    update:function(obj){
        this.state.hasChanged = true;
        this.validate(obj.target.value);
    },
    updateWithValue:function(val){
        this.state.hasChanged = true;
        this.validate(val);
    },
    //For autofills
    intervalCheck:function(){
        //this was killing the promo code stuff for some reason. Don't do this.
    },
};


//FORM HANDLER
var WdForm = React.createClass({
    elementAttach:function(elementRef){
        this.wdformElements.push(elementRef);
    },
    elementValueChange:function(elementRef){
        //unregister any elements that are no longer on screen (can happen with dynamically visible elements when they drop from screen).
        //var newFormElements = [];
        //for(var i=0;i<this.wdformElements.length;i++){
        //    if(this.wdformElements[i]._reactInternalInstance){
        //        newFormElements.push(this.wdformElements[i]);
        //    }
        //}
        //this.wdformElements[i] = newFormElements;

        //validate form
        var invalidCnt=0;
        for(var i=0;i<this.wdformElements.length;i++){
            if(this.wdformElements[i].state.valid===false){
                invalidCnt++;
            }
        }

        this.state.invalidCount = invalidCnt;
        this.state.valid = invalidCnt===0;

        this.forceUpdate();
        if(this.props.wdOnChange){
            //add a very quick delay (1ms is enough) so that the form change event fires after any element events
            var _this = this;
            setTimeout(function(){_this.props.wdOnChange(_this)}, 1);
        }
    },
    registerElements:function(children){
        //just in case it is an empty form
        if (typeof children !== 'object' || children === null) {
            return children;
        }
        //loop all children 
        return React.Children.map(children, function (child) {
            if(child && child.type) {
                if (child.type.displayName == 'SubmitButton') {
                    return React.cloneElement(child, {
                        onClick: this.submit,
                        label: React.createElement(
                            "span",
                            null,
                            child.props.label,
                            this.state.showSubmitError && this.state.invalidCount > 0 ? React.createElement("span", null, " (", this.state.invalidCount, " error", this.state.invalidCount !== 1 ? 's' : null, ")") : null)
                    });
                } else if (child.props && child.props.label) {
                    return React.cloneElement(child, {
                        callbackElementAttach: this.elementAttach,
                        callbackElementValueChange: this.elementValueChange,
                        wdformElId: 'wdform-el-' + WdTools.makeUniqueId()
                    }, child.props.children);
                } else {
                    return React.cloneElement(child, {}, this.registerElements(child.props.children));
                }
            }
        }, this);
    },
    submit:function(e){
        if(this.state.valid){
            this.props.wdOnSubmit();
        }else {
            this.forceValidation();
            this.forceUpdate();
        }
    },
    forceValidation:function(){
        //force all elements to validate
        for(var i=0;i<this.wdformElements.length;i++){
            this.wdformElements[i].state.hasChanged=true;
            this.wdformElements[i].forceUpdate();
        }
        this.state.showSubmitError=true;
    },
    getInitialState:function(){
        return {valid:false,invalidCount:0,showSubmitError:false}
    },
    componentWillMount:function(){
        this.wdformElements=[];
    },
    componentDidUpdate:function(){
        if(this.props.forceValidation===true) {
            this.forceValidation();
        }
    },
    render:function(){
        this.formId = 'wdform-'+WdTools.makeUniqueId();
        var reprocessedChildren = this.registerElements(this.props.children);
        return <form id={this.formId} className={this.state.showSubmitError===true && this.state.valid===false?"wdform invalid":"wdform valid"} onSubmit={this.submit}>{reprocessedChildren}</form>
    }
});


//ELEMENTS
WdForm.Input = React.createClass({
    mixins: [WdformMixin],
    componentDidMount:function(){
        var _thisRef=this;
        this.interval = setInterval(function(){ _thisRef.intervalCheck() }, 1000);
        this.setState({value:this.props.value});
    },
    componentWillUnmount: function() {
        return clearInterval(this.interval);
    },
    updateStateVal:function(event){
        this.setState({value: event.target.value});
    },
    onBlur:function(event){
        this.updateStateVal(event);
        this.update(event);
    },
    render:function(){
        var maxLength=255;
        if(this.props.rules && this.props.rules.maxLength){
            maxLength=this.props.rules.maxLength;
        }
        var showLabel = true;
        if(this.props.hasOwnProperty('showLabel') && this.props.showLabel===false){
            showLabel = false;
        }
        return (
            <div className="formControlWrap">
                {showLabel?<label htmlFor={this.props.wdformElId}>{this.props.label}</label>:null}
                <input {...this.props} maxLength={maxLength} id={this.props.wdformElId} className="form-control" placeholder={this.props.label} value={this.state.value} onChange={this.updateStateVal} onBlur={this.onBlur} />
                {this.state.hasChanged===true && this.state.valid===false?<div><label htmlFor={this.props.wdformElId} className="el-errors">{this.labelErrors}</label></div>:null}
                {/* <pre>state: {JSON.stringify(this.state, undefined, 2)}</pre> */}
            </div>
        )
    }
});


//Select source http://matt-harrison.com/building-a-complex-web-component-with-facebooks-react-library/
WdForm.Select = React.createClass({
    mixins: [WdformMixin],
    render: function(){
        var optionNodes = this.props.options.map(function(option){
            return <option key={WdTools.makeUniqueId()} value={option.value}>{option.label}</option>;
        });

        return (
            <div className="formControlWrap">
                <label htmlFor={this.props.wdformElId}>{this.props.label}</label>
                <select id={this.props.wdformElId} className="form-control" ref="menu" value={this.props.value} onChange={this.update}>
                    {optionNodes}
                </select>
                {this.state.hasChanged===true && this.state.valid===false?<label htmlFor={this.props.wdformElId} className="el-errors">{this.labelErrors}</label>:null}
            </div>
        );
    }
});


//Select source http://matt-harrison.com/building-a-complex-web-component-with-facebooks-react-library/
WdForm.Switcher = React.createClass({
    mixins: [WdformMixin],
    componentDidMount:function(){
        var b_isSelected=false;
        if(this.props.isSelected===true){
            b_isSelected=true;
        }
        this.setState({selected:b_isSelected,key:this.props.data.key});
        this.validate(this.props.value);
    },
    handleChange:function(e){
        if(this.props.isDisabled && this.props.isDisabled===true) {
            //this is disabled
        }else{
            if(this.props.isSelected===true){
                this.state.selected=false;
                this.state.value=false;
            }else{
                this.state.selected=true;
                this.state.value=this.props.valueIfSelected;
            }
            this.state.hasChanged=true;
            this.forceUpdate();
            if(this.props.onChange) {
                this.props.onChange(this.state);
            }
        }
    },
    render: function(){
        var icon;
        var classNames = 'switcher';
        if(this.props.isSelected) {
            classNames += ' selected';
            icon = <i className="fa fa-check"></i>;
        }
        if(this.props.isDisabled && this.props.isDisabled===true) {
            classNames += ' disabled';
            icon = <i className="fa fa-ban"></i>;
        }

        return (<div className={classNames} value={this.props.value} onClick={this.handleChange}><span className="switcherlabel">{this.props.label} {icon}</span></div>);
    }
});

WdForm.Checkbox = React.createClass({
    mixins: [WdformMixin],
    componentDidMount:function(){
        var b_isSelected=false;
        if(this.props.checked===true){
            b_isSelected=true;
        }
        this.state.checked = b_isSelected;
        this.state.value = b_isSelected?this.props.valueIfSelected:'';
        this.state.valid = b_isSelected;
        this.validate(this.state.value);
    },

    handleChange: function(event) {
        var isSelected = event.target.checked;
        this.state.checked = isSelected;
        this.state.value = isSelected?this.props.valueIfSelected:'';
        this.state.hasChanged = true;
        this.validate(this.state.value);
    },
    render:function(){
        return (
            <div className="formControlWrap">
                <label className="checkradio">
                    <input checked={this.state.checked} type="checkbox" id={this.props.wdformElId} value={this.state.value} onChange={this.handleChange} />
                    <span className="checkradiolabel">{this.props.label}</span>
                </label>
                {this.state.hasChanged===true && this.state.valid===false?<div><label htmlFor={this.props.wdformElId} className="el-errors">{this.labelErrors}</label></div>:null}
            </div>
        )
    }
});


//Select source http://matt-harrison.com/building-a-complex-web-component-with-facebooks-react-library/
WdForm.SwitcherGroup = React.createClass({
    mixins: [WdformMixin],
    updateSwitcher:function(switcherState){
        this.state.hasChanged = true;
        if(this.props.onChange){
            this.props.onChange(switcherState);
        }
    },
    render: function(){
        var _self = this;
        var content = this.props.data.map(function(option) {
            return (
                <WdForm.Switcher isSelected={option.selected} isDisabled={option.isDisabled} label={option.label} valueIfSelected={option.value} key={option.key} data={option} onChange={_self.updateSwitcher} />
            );
        });
        return (
            <div className="formControlWrap">
                <div className="switcherGroup">{content}</div>
            </div>
        );
    }
});


WdForm.Date = React.createClass({
    mixins: [WdformMixin],
    componentDidMount:function(){
        var pickerEl =$('#'+this.props.wdformElId);
        var _this = this;
        pickerEl.datepicker({format: "dd/mm/yyyy"}).on("changeDate", function(e) {
            _this.value = e.target.value;
            _this.handleChange(e);
        }).on("clearDate", function(e) {
            _this.value = e.target.value;
            _this.handleChange(e);
        });
        this.validate(this.props.value);
    },
    handleChange:function(e){
        //fire custom event up chain so that parent can update props.value
        //Not 100% sure if this is the best way. Couldnt find real clear documentation to do this. 
        this.update(e);
    },
    render:function(){
        return (
            <div className="formControlWrap">
                <label htmlFor={this.props.wdformElId}>{this.props.label}</label>
                <input type="text" id={this.props.wdformElId} className="form-control date-picker" placeholder="DD/MM/YYYY" onChange={this.handleChange}  {...this.props}  />
                {this.state.hasChanged===true && this.state.valid===false?<label htmlFor={this.props.wdformElId} className="el-errors">{this.labelErrors}</label>:null}
            </div>
        )
    }
});

WdForm.Button = React.createClass({
    render:function(){
        return (
            <button type="submit" onClick={this.props.onClick} {...this.props} >{this.props.label}</button>
        )
    }
});
//element that is tied to a form
WdForm.SubmitButton = React.createClass({
    render:function(){
        return (
            <button type="submit" {...this.props} >{this.props.label}</button>
        )
    }
});

WdForm.TextArea = React.createClass({
    mixins: [WdformMixin],
    componentDidMount:function(){
        var _thisRef=this;
        this.interval = setInterval(function(){ _thisRef.intervalCheck() }, 1000); //handle autofills.
        this.setState({value:this.props.value});
    },
    componentWillUnmount: function() {
        return clearInterval(this.interval);
    },
    updateStateVal:function(event){
        this.setState({value: event.target.value});
    },
    onBlur:function(event){
        this.updateStateVal(event);
        this.update(event);
    },
    render:function(){
        var showLabel = true;
        if(this.props.hasOwnProperty('showLabel') && this.props.showLabel===false){
            showLabel = false;
        }
        return (
            <div className="formControlWrap">
                {showLabel?<label htmlFor={this.props.wdformElId}>{this.props.label}</label>:null}
                <textarea {...this.props} value={this.state.value} id={this.props.wdformElId} className="form-control" rows="3" placeholder={this.props.label} onChange={this.updateStateVal} onBlur={this.onBlur}  />
                {this.state.hasChanged===true && this.state.valid===false?<label htmlFor={this.props.wdformElId} className="el-errors">{this.labelErrors}</label>:null}
            </div>
        )
    }
});


