var WdUi = React.createClass({
  render: function() {
    return (
    <div></div>
    ); 
  }
});
 
WdUi.Modal = React.createClass({
  componentDidMount:function(){
    var thisRef = this;
    var modalRef = $(ReactDOM.findDOMNode(this));
    modalRef.modal({
      backdrop: 'static'
    });
    modalRef.on('hidden.bs.modal', function (e) {
      //fire event if specified
      if(thisRef.props.wdOnClose){
          thisRef.props.wdOnClose(thisRef);
      }
    });
  },
  render: function() {
    return (
      <div className="modal fade" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" id={"modal"+WdTools.makeUniqueId()}>
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id="myModalLabel">{this.props.title}</h4>
            </div>
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
});