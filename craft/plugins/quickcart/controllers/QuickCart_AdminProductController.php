<?php
namespace Craft;

use QuickCart\Cart;
use QuickCart\CartItem;
use QuickCart\Categories;

class QuickCart_AdminProductController extends QuickCart_BaseController
{
    public function actionIndex(array $variables = array())
    {
        $o_products =  QuickCart_ProductModel::populateModels(
            QuickCart_ProductRecord::model()->findAll()
            ,'id'
        );
        $this->renderTemplate('quickcart/cp/product/index',  array(
            'routes'=>Cart::getRoutes(),
            'products'=>$o_products
        ));
    }

    public function actionEdit(array $variables = array())
    {
        if(isset($variables['product'])){
            //Product save has been attempted, however an error occurred, sent product object back to form as is to show errors.
            $variables['title'] = Craft::t('Create a new product');
        }else {

            if ($variables['productId'] == 'new' || empty($variables['productId'])) {
                //New
                $o_product = new QuickCart_ProductModel();
                $o_product->enabled = true; //enable be default
                $variables['title'] = Craft::t('Create a new product');
            } else {
                //Editing existing
                $o_catRec = QuickCart_ProductRecord::model()->findById($variables['productId']);
                if (!$o_catRec) {
                    throw new HttpException(404);
                }
                $o_product = QuickCart_ProductModel::populateModel($o_catRec);
                $variables['title'] = $o_product->name;
            }
            $variables['product'] = $o_product;
        }
        $variables['categorySelectFieldOpts']=Categories::getTreeSelectFieldOptions();
        $this->renderTemplate('quickcart/cp/product/edit',$variables);
    }

    public function actionSave()
    {
        //quick alias for getPost
        function postvar($name){ return craft()->request->getPost($name); }

        $this->requirePostRequest();

        $id = postvar('productId');
        if($id){
            $o_catRec = QuickCart_ProductRecord::model()->findById($id);
            if (!$o_catRec) {
                throw new HttpException(404);
            }
        }else{
            $o_catRec = new QuickCart_ProductRecord();
        }
        $a_setVars = array('name','code','parent','description','enabled','taxable','price','order');
        foreach($a_setVars as $var){
            $o_catRec->setAttribute($var,postvar($var));
        }
        $o_product = QuickCart_ProductModel::populateModel($o_catRec);
        $o_catRec->validate();
        $o_product->addErrors($o_catRec->getErrors());
        if ($o_catRec->save()) {

            //write categories
            /*
            $a_categories = postvar('categories');
            print_r($a_categories); exit();
            */
            
            craft()->userSession->setNotice(Craft::t('Product '.$o_catRec->name.' saved.'));
            $this->redirectToPostedUrl();
        }	else {
            craft()->userSession->setError(Craft::t('Could not save product.'));
            craft()->urlManager->setRouteVariables(array(
                'product' => $o_product
            ));
        }
    }

    public function actionDelete()
    {
        $this->requirePostRequest();
        $this->requireAjaxRequest();

        $id = craft()->request->getRequiredPost('id');
        $o_product = QuickCart_ProductRecord::model()->findById($id);
        $b_result = $o_product->delete();
        $this->returnJson(array('success' =>$b_result));
    }
}