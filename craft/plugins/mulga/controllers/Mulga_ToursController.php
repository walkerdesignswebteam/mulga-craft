<?php
namespace Craft;

use WalkerRezdy;

require_once dirname(dirname(__FILE__)).'/includes/WalkerRezdy.class.php';
require_once dirname(dirname(__FILE__)).'/includes/EwayPayment.class.php';
require_once dirname(dirname(__FILE__)).'/includes/Payment.class.php';

date_default_timezone_set('Australia/Victoria'); // needs to be set otherwise it used UTC

class Mulga_ToursController extends Mulga_BaseController
{
    protected $allowAnonymous = array('actionIndex','actionShow','actionBook','actionBookAjax','actionPay','actionGetPromotionAjax','actionTestpaymentnotification');

    public function actionIndex(array $variables = array())
    {
        /*
        $couponCodeMatrix = craft()->globals->getSetByHandle('couponCodes');

        $criteria = craft()->elements->getCriteria(ElementType::MatrixBlock);
        $criteria->fieldId = craft()->fields->getFieldByHandle('couponCodes')->id;
        $criteria->ownerId = $couponCodeMatrix->id; // This is your Global Set id
        $criteria->status = null;
        $matrixBlocks = $criteria->find();

        $newMatrixData = array('couponCodes'=>array());
        foreach($matrixBlocks as $block){
            $blockData = array(
                'type' => $block->getType()->handle,
                'enabled' => $block->enabled,
                'fields' => array(
                    'couponCode' => $block->couponCode.'P',
                    'couponType' => $block->couponType,
                    'couponValue' => $block->couponValue,
                    'expiryDate' => $block->expiryDate,
                    'singleUse' => $block->singleUse,
                )
            );
            $newMatrixData['couponCodes'][$block->id] = $blockData;
        }
        $couponCodeMatrix->setContentFromPost($newMatrixData);
        craft()->globals->saveContent($couponCodeMatrix);
 exit();
        */



        $rezdyProdAvailabilityData = WalkerRezdy::getProductAvailabilityData(craft());
        $a_quarters = WalkerRezdy::availabilityDataToQuarters(craft(),$rezdyProdAvailabilityData['tours']);

        if(isset($rezdyProdAvailabilityData['tours'])){
            echo $this->getTemplateHtml('site/tours_index',array('tour_products'=>$rezdyProdAvailabilityData['products'],'quarterAvailabilities'=>$a_quarters));

        }else{
            throw new HttpException(404,'No tours found.');
        }
    }

    public function actionShow(array $variables = array())
    {
        $tourid =WalkerRezdy::getRezdyTourIdFromSlug(craft(),$variables['tourid']);
        $prod = WalkerRezdy::getProductByCode($tourid);
        if($prod) {
            /*
            $a_availabilityInfo = WalkerRezdy::request('availability', array(
                array('key' => 'productCode', 'val' => $prod->productCode),
                array('key' => 'startTime', 'val' => $variables['starttime']),
                array('key' => 'endTime', 'val' => date('c', strtotime($variables['starttime']) + (60)))
            ));
            */

            $foundTourEntry = WalkerRezdy::getEntryByTourCode(craft(), $prod->productCode);
            $this->redirect($foundTourEntry->url);

            //throw new HttpException(404,'Tour not found with code '.$prod->productCode);
            /*
            if ($foundTourEntry) {
                if (isset($a_availabilityInfo->sessions[0])) {
                    $singleAvailability = $a_availabilityInfo->sessions[0];
                    $singleAvailability->durationDays = WalkerRezdy::getAvailabilityDuration($singleAvailability->startTime,$singleAvailability->endTime);
                    echo $this->getTemplateHtml('site/tours_tour', array('entry' => $foundTourEntry, 'title' => $prod->name . ' - Starting ' . date('jS F, Y', strtotime($variables['starttime'])), 'tour' => $prod, 'availabilityInfo' => $a_availabilityInfo->sessions[0]));
                }else{
                    throw new HttpException(404, 'Can\'t find valid session for this tour. Please contact us.');
                }

            } else {
                throw new HttpException(404, 'Tour information entry matching "' . $prod->productCode . '" not found in Craft. Ensure entry is created in the Craft "Tours" section and the rezdyID is present and matches "' . $prod->productCode . '"');
            }
            */

        }else{
            throw new HttpException(404,'Tour not found with code '.$prod->productCode);
        }
    }

    public function actionBook(array $variables = array())
    {

        $rezdyId=WalkerRezdy::getRezdyTourIdFromSlug(craft(),$variables['tourid']);

        if($rezdyId===false){
            throw new HttpException(404,'No tour found with specified code.');
        }else {
            $prod = WalkerRezdy::getProductByCode($rezdyId);
            $foundTourEntry = WalkerRezdy::getEntryByTourCode(craft(), $rezdyId);
            if ($foundTourEntry) {
                if ($prod) {
                    $a_availabilitySessions = WalkerRezdy::getAvailability($prod->productCode, $variables['starttime']);

                    if(empty($a_availabilitySessions) || (isset($a_availabilitySessions[0]->seatsAvailable) && $a_availabilitySessions[0]->seatsAvailable==0)){
                        $this->redirect($foundTourEntry->url);
                    }else {

                        $s_defaultH1 = $s_defaultTitle = 'Book Tour: ' . $prod->name . ' - Starting ' . date('jS F, Y', strtotime($variables['starttime']));
                        if (!empty($foundTourEntry->bookPageTitleTag)) {
                            $s_defaultTitle = $foundTourEntry->bookPageTitleTag . ' - Starting ' . date('jS F, Y', strtotime($variables['starttime']));
                        }
                        if (!empty($foundTourEntry->bookPageHeadingTag)) {
                            $s_defaultH1 = $foundTourEntry->bookPageHeadingTag . ' - Starting ' . date('jS F, Y', strtotime($variables['starttime']));
                        }
                        //tour_entry.waiverForm
                        echo $this->getTemplateHtml('site/tours_book', array('tour_entry' => $foundTourEntry, 'tourName' => $prod->name, 'title' => $s_defaultTitle, 'h1' => $s_defaultH1, 'tourid' => $variables['tourid'], 'starttime' => $variables['starttime']));
                    }
                }
            } else {
                throw new HttpException(404, 'Tour not found with code ' . $prod->productCode);
            }
        }
    }

    public function actionBookAjax(){
        $this->requirePostRequest();
        $this->requireAjaxRequest();

        $tourid =WalkerRezdy::getRezdyTourIdFromSlug(craft(),craft()->request->getParam('tourid'));
        if($tourid ) {
            $starttime = craft()->request->getParam('starttime');

            $o_tourProduct = WalkerRezdy::getProductByCode($tourid);
            $a_availabilitySessions = WalkerRezdy::getAvailability($o_tourProduct->productCode, $starttime);
            $o_availability = $a_availabilitySessions[0];
            $o_availability->product = $o_tourProduct;

            $a_accomSessions = WalkerRezdy::getAccommodationOptions($o_availability->startTime);

            $a_accomExtraSessions = WalkerRezdy::getAccommodationExtras($o_availability->startTime);
            $a_extraSessions = WalkerRezdy::getExtrasOptions($o_availability->startTime);

            $this->returnJson(array('tour' => $o_availability, 'accom' => $a_accomSessions, 'accomExtras' => $a_accomExtraSessions, 'extras' => $a_extraSessions));
        }else{
            //shouldn't happen
            $this->returnJson(array('tour' => false));
        }
    }
    private function couponCheckMatch($couponCode,$couponObj){
        if($couponObj->enabled==1) {
            if (strtolower(trim($couponObj->couponCode)) == strtolower(trim($couponCode))) {
                if (empty($couponObj->expiryDate)){
                    return true;
                }else{
                    $i_startOfTodaysTime = strtotime(date('Y-m-d',time()));
                    $i_expireTime = strtotime($couponObj->expiryDate.' +1 day');
                    //echo date('Y-m-d H:i:s',$i_expireTime).' '.date('Y-m-d H:i:s',$i_startOfTodaysTime);
                    if ($i_expireTime>$i_startOfTodaysTime) {
                        return true;
                    }
                }
            }
        }
    }
    private function getPromotionInfo($s_code,$s_tourStarttime=null){
        $globalsMatrix = craft()->globals->getSetByHandle('couponCodes');

        $criteria = craft()->elements->getCriteria(ElementType::MatrixBlock);
        $criteria->fieldId = craft()->fields->getFieldByHandle('couponCodes')->id;
        $criteria->ownerId = $globalsMatrix->id; // This is your Global Set id
        $criteria->status = null;
        $matrixBlocks = $criteria->find();

        foreach($matrixBlocks as $couponObj){
            if($this->couponCheckMatch($s_code,$couponObj)){

                // See if we should restrict to a specific tourStartTime
                if(!empty($couponObj->restrictToRezdyDate) && !empty($s_tourStarttime)) {
                    // If date string start is zero, this is a match.
                    if(strpos($s_tourStarttime,$couponObj->restrictToRezdyDate)===false){
                        return false;
                    }
                }

                return array('couponCode' => $couponObj->couponCode, 'couponType' => $couponObj->couponType, 'couponValue' => $couponObj->couponValue);
            }
        }
        return false;
    }
    public function actionGetPromotionAjax(){
        $this->requirePostRequest();
        //$this->requireAjaxRequest(); // to allow postman, I've removed this.

        $a_couponInfo = $this->getPromotionInfo(craft()->request->getParam('promotionCode'),craft()->request->getParam('tourStarttime'));
        $s_message = 'The code you have entered is either incorrect or has expired. Please check the code and try again. Contact us if you need some help.';
        if($a_couponInfo){
            $s_message = 'Your code is valid.';
        }
        return $this->returnJson(array('promotionInfo'=>$a_couponInfo,'message'=>$s_message));
    }
    private function applyDiscount($s_code){
        $couponCodeMatrix = craft()->globals->getSetByHandle('couponCodes');

        $criteria = craft()->elements->getCriteria(ElementType::MatrixBlock);
        $criteria->fieldId = craft()->fields->getFieldByHandle('couponCodes')->id;
        $criteria->ownerId = $couponCodeMatrix->id; // This is your Global Set id
        $criteria->status = null;
        $matrixBlocks = $criteria->find();

        $b_updateMatrix = false;
        $o_foundCoupon = false;
        $newMatrixData = array('couponCodes'=>array());
        foreach($matrixBlocks as $block){

            $b_enabled = $block->enabled;
            if ($this->couponCheckMatch($s_code, $block)) {
                $o_foundCoupon = $block;
                if($block->singleUse==1){
                    $b_updateMatrix=true;
                    $b_enabled = false;
                }
            }

            $blockData = array(
                'type' => $block->getType()->handle,
                'enabled' => $b_enabled,
                'fields' => array(
                    'couponCode' => $block->couponCode,
                    'couponType' => $block->couponType,
                    'couponValue' => $block->couponValue,
                    'expiryDate' => $block->expiryDate,
                    'singleUse' => $block->singleUse,
                )
            );
            $newMatrixData['couponCodes'][$block->id] = $blockData;
        }
        if($b_updateMatrix) {
            $couponCodeMatrix->setContentFromPost($newMatrixData);
            craft()->globals->saveContent($couponCodeMatrix);
        }
        if($o_foundCoupon){
            return array('couponCode' => $o_foundCoupon->couponCode, 'couponType' => $o_foundCoupon->couponType, 'couponValue' => $o_foundCoupon->couponValue);
        }
        return false;
    }
    public function actionTestpaymentnotification()
    {
        //Quick payment notification test
        $this->sendPaymentNotification(Mulga_OrderRecord::model()->findById(106),true);
        exit();
    }

    public function actionPay(){
        $this->requireAjaxRequest();

        //set variables from post
        $a_tourInfo = craft()->request->getParam('tour');
        $a_transactiondetails = craft()->request->getParam('cc_details');
        $a_bookinginfo = craft()->request->getParam('bookingDetails');
        $a_bookingcosts = craft()->request->getParam('bookingCosts');
        $a_promotionInfo = craft()->request->getParam('promotionInfo');
        $a_participants = craft()->request->getParam('participants');
        $a_availabilityinfo = craft()->request->getParam('availabilityinfo');
        $glob_tourInfo = craft()->globals->getSetByHandle('tourInfo');
        $s_promoCode = strtoupper(trim(craft()->request->getParam('promotionCode')));

        $n_bookingFee = $glob_tourInfo->bookingFee;
        $n_deposit = $n_bookingFee*count($a_participants);

        //to test with
        /*
        echo $this->returnJson(json_decode('{"bookingInfo":{"requestStatus":{"success":true,"version":"v1"},"booking":{"orderNumber":"RBBBYTC","status":"CONFIRMED","supplierId":43955,"supplierName":"Multilocus Interactive Pty Ltd","customer":{"id":1944519,"firstName":"Marcus","lastName":"House","name":"Marcus House","email":"marcus@walkerdesigns.com.au","phone":"0438402887","city":"Prospect Vale","state":"Tasmania"},"items":[{"productName":"Three rivers and a waterfall","productCode":"PHSWAZ","startTime":"2015-12-29T13:00:00Z","endTime":"2016-01-07T13:00:00Z","quantities":[{"optionLabel":"Quantity","optionPrice":1500,"value":5}],"totalQuantity":5,"amount":7500,"extras":[],"participants":[{"fields":[{"label":"First Name","value":"Marcus","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false}]},{"fields":[{"label":"First Name","value":"Linda","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false}]},{"fields":[{"label":"First Name","value":"Alex","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false}]},{"fields":[{"label":"First Name","value":"Lachlan","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false}]},{"fields":[{"label":"First Name","value":"Melanie","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"Tyson","requiredPerParticipant":false,"requiredPerBooking":false}]}],"subtotal":7500},{"productName":"Dreamers Mt Beauty","productCode":"PKXMZD","startTime":"2015-12-29T13:00:00Z","endTime":"2015-12-30T13:00:00Z","quantities":[{"optionLabel":"Double","optionPrice":0,"value":1}],"totalQuantity":1,"amount":0,"extras":[],"participants":[{"fields":[{"label":"First Name","value":"Marcus","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Special Requirements","value":"Room Type - Double","requiredPerParticipant":false,"requiredPerBooking":false}]}],"subtotal":0},{"productName":"Dreamers Mt Beauty Chalet","productCode":"P8ZPVW","startTime":"2015-12-29T13:00:00Z","endTime":"2015-12-30T13:00:00Z","quantities":[{"optionLabel":"Single","optionPrice":120,"value":1},{"optionLabel":"Double","optionPrice":160,"value":1}],"totalQuantity":2,"amount":280,"extras":[],"participants":[{"fields":[{"label":"First Name","value":"Alex","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Special Requirements","value":"Room Type - Double","requiredPerParticipant":false,"requiredPerBooking":false}]},{"fields":[{"label":"First Name","value":"Melanie","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"Tyson","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Special Requirements","value":"Room Type - Single","requiredPerParticipant":false,"requiredPerBooking":false}]}],"subtotal":280},{"productName":"Bike Hire","productCode":"PRGXVZ","startTime":"2015-12-29T13:00:00Z","endTime":"2015-12-30T13:00:00Z","quantities":[{"optionLabel":"Each","optionPrice":50,"value":2}],"totalQuantity":2,"amount":100,"extras":[],"participants":[{"fields":[{"label":"First Name","value":"Marcus","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false}]},{"fields":[{"label":"First Name","value":"Alex","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false}]}],"subtotal":100},{"productName":"Electric Bike Hire","productCode":"P8XLPW","startTime":"2015-12-29T13:00:00Z","endTime":"2015-12-30T13:00:00Z","quantities":[{"optionLabel":"Each","optionPrice":80,"value":3}],"totalQuantity":3,"amount":240,"extras":[],"participants":[{"fields":[{"label":"First Name","value":"Linda","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false}]},{"fields":[{"label":"First Name","value":"Alex","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"House","requiredPerParticipant":false,"requiredPerBooking":false}]},{"fields":[{"label":"First Name","value":"Melanie","requiredPerParticipant":false,"requiredPerBooking":false},{"label":"Last Name","value":"Tyson","requiredPerParticipant":false,"requiredPerBooking":false}]}],"subtotal":240}],"totalAmount":8120,"totalCurrency":"AUD","totalPaid":2500,"totalDue":5620,"dateCreated":"2015-12-22T13:19:17.814Z","dateConfirmed":"2015-12-22T13:19:17.814Z","comments":"From Google!\nOh, and from Mulga :)\n\nkind regards\n\nMarcus\r\n","internalNotes":"Payment made by API from website.","payments":[{"type":"CREDITCARD","amount":2500,"currency":"AUD","date":"2015-12-22T13:19:16Z","label":"Payment processed externally by eWay"}],"fields":[],"source":"API","vouchers":[]}},"paymentInfo":{"status":"paid","transactionNumber":"12130163","transactionRef":"MULGA151222131916","fullMessage":"Transaction Status: True, Transaction Number: 12130163, Transaction Reference: MULGA151222131916, Return Amount: 250000, Auth Code: 224747"}}'));
        exit();
        */
        $a_ret = array();
        $paymentInfo = WalkerRezdy::makePayment(craft()->request);
        if(isset($paymentInfo['status']) && $paymentInfo['status']=='paid'){
            //Payment ok, attempt to make booking
            $a_ret['bookingInfo']=json_decode(WalkerRezdy::processBooking(craft(),$n_deposit));


            $o_catRec = new Mulga_OrderRecord();

            $n_discount = 0;
            if(isset($a_ret['bookingInfo']->booking->totalDue)) {
                //Get discount amount
                $n_balance = $a_ret['bookingInfo']->booking->totalDue;

                $n_total = (float)$n_deposit + (float)$n_balance;
                $a_promotionInfo = false;
                if (!empty($s_promoCode)) {
                    $applied_promo = $this->applyDiscount($s_promoCode);
                    if (!empty($applied_promo)) {
                        $a_promotionInfo = $applied_promo;
                        if ($a_promotionInfo['couponType']->value == 'percentage') {
                            if(count($a_bookingcosts['subtotals']) >1){
                                $n_discount = $a_bookingcosts['subtotals'][1]['total'] * ($a_promotionInfo['couponValue'] / 100); //Discounting based on the cost of the Tour not the total cost.
                            }else{
                                $n_discount = 0;
                            }
                            //$n_discount = $n_total * ($a_promotionInfo['couponValue'] / 100);
                        } else if ($a_promotionInfo['couponType']->value == 'dollar') {
                            $n_discount = $a_promotionInfo['couponValue']*count($a_participants);
                        }
                    } else {
                        //shouldn't happen generally unless a booking request fails in some way. May need to make a response for this.
                    }
                }
                $o_catRec->setAttribute('balance', $n_balance);
            }

            if(isset($a_ret['bookingInfo']->booking->orderNumber)) {
                $o_catRec->setAttribute('orderNumber', $a_ret['bookingInfo']->booking->orderNumber);
            }

            $o_catRec->setAttribute('firstName',$a_bookinginfo['firstName']);
            $o_catRec->setAttribute('lastName',$a_bookinginfo['lastName']);
            $o_catRec->setAttribute('transactionInfo',$a_transactiondetails);
            $o_catRec->setAttribute('promotionInfo',$a_promotionInfo); //store just for reference really
            $o_catRec->setAttribute('promotionCode',$s_promoCode);
            $o_catRec->setAttribute('tourInfo',$a_tourInfo);
            $o_catRec->setAttribute('tourId',$a_tourInfo['tourid']);
            $o_catRec->setAttribute('deposit',$n_deposit);
            $o_catRec->setAttribute('discount',$n_discount);
            $o_catRec->setAttribute('paymentSummary',$a_bookingcosts);
            $o_catRec->setAttribute('orderInfo',$a_bookinginfo);
            $o_catRec->setAttribute('availabilityInfo',$a_availabilityinfo);
            $o_catRec->setAttribute('participants',$a_participants);
            $o_catRec->setAttribute('rezdyResponse',json_encode($a_ret['bookingInfo']));

            $o_catRec->save();
            //send email
            $this->sendPaymentNotification($o_catRec);

        }
        $a_ret['paymentInfo']=$paymentInfo;

        $this->returnJson($a_ret);
    }

    private function getAccomInfoFromParticipants($participants){
        $a_ret = array();

        $a_usersIdsToIgnore = array();

        foreach($participants as $person){
            if(in_array($person['id'],$a_usersIdsToIgnore)){
                //partner already found and will have been combined already.
            }else{
                if(empty($person['sharedwith']) || $person['sharedwith']=='none'){
                    $a_ret[]=array('type'=>'Single','name'=>$person['firstName'].' '.$person['lastName']);
                }else{
                    //find the shared person
                    foreach($participants as $partner){
                        if($person['sharedwith']==$partner['id']){
                            $a_ret[]=array('type'=>$person['doubletwin'],'name'=>$person['firstName'].' & '.$partner['firstName']);
                            $a_usersIdsToIgnore[]=$person['sharedwith'];
                            break;
                        }
                    }
                }
            }
        }
        return $a_ret;
    }
    private function sendPaymentNotification($o_catRec,$b_debugOnly=false){

        //Tour info (get from this request)
        $a_tourAvailabilityInfo = $o_catRec->getAttribute('availabilityInfo');
        $startTime = $a_tourAvailabilityInfo['tour']['startTime'];
        $endtime = $a_tourAvailabilityInfo['tour']['endTime'];

        $starttimeDisplay = date('jS M Y',strtotime($startTime));
        $pp1Date = strtotime($startTime.' -60 days');
        $pp2Date = strtotime($startTime.' -35 days');
        if(time()>$pp1Date) {
            $pp1DateDisplay = '<span style="color:#ff0000;">'.date('jS M Y', $pp1Date).'</span>';
        }else{
            $pp1DateDisplay = date('jS M Y', $pp1Date);
        }

        if(time()>$pp2Date) {
            $pp2DateDisplay = '<span style="color:#ff0000;">'.date('jS M Y', $pp2Date).'</span>';
        }else{
            $pp2DateDisplay = date('jS M Y', $pp2Date);
        }

        //$endtimeDisplay = date('jS M Y',strtotime($endtime));
        $durationDays = WalkerRezdy::getAvailabilityDuration($startTime,$endtime);



        $s_siteUrl = craft()->config->get('siteUrl');

        $a_paymentSummary = $o_catRec->getAttribute('paymentSummary');
        $a_orderInfo = $o_catRec->getAttribute('orderInfo');
        $a_transactionInfo = $o_catRec->getAttribute('transactionInfo');
        $a_participants = $o_catRec->getAttribute('participants');
        $a_tourInfo = $o_catRec->getAttribute('tourInfo');

        //bedding info
        $a_accom = $this->getAccomInfoFromParticipants($a_participants);
        $accomLines = array();
        foreach($a_accom as $bedding){
            $accomLines[] = '<tr><td valign="top"><strong>' . htmlspecialchars($bedding['type']) . ':</strong></td><td>&nbsp;</td><td valign="top">' . htmlspecialchars($bedding['name']) . '</td></tr>';
        }

        $n_discount = $o_catRec->getAttribute('discount');
        $val = $o_catRec->getAttribute('balance');
        if(empty($val)){
            $balance = 'WILL BE CONFIRMED';
            $totalTourPrice = 'WILL BE CONFIRMED';
        }else {
            $balance = '$'.number_format((float)$o_catRec->getAttribute('balance') - (float)$n_discount, 2).' incl. GST';
            $totalTourPrice= '$'.htmlspecialchars(number_format($o_catRec->getAttribute('balance')+$o_catRec->getAttribute('deposit'),2)).' incl. GST';
        }

        $summaryTable = '<table>';
            foreach($a_paymentSummary['subtotals'] as $item){
                if(isset($item['subTitle'])){
                    $summaryTable .= '<tr><td colspan="3"  valign="top"><strong>'.htmlspecialchars($item['subTitle']).'</strong></td></tr>';
                }else {
                    $val = 'Free';
                    if($item['total']>0){
                        $val='$'.number_format($item['total'],2);
                    }
                    $summaryTable .= '<tr><td valign="top">'.htmlspecialchars($item['label']).'</td><td>&nbsp;</td><td valign="top"><em>'.htmlspecialchars($val).'</em></td></tr>';
                }
            }
            $summaryTable .= '<tr><td colspan="3">&nbsp;</strong></td></tr>';

            $summaryTable .= '<tr><td valign="top"><strong>TOTAL TOUR PRICE:</td><td>&nbsp;</td><td valign="top"><strong><em>'.$totalTourPrice.'</em></strong></td></tr>';

            if($n_discount>0) {
                $summaryTable .= '<tr><td valign="top"><strong>DISCOUNT:</td><td>&nbsp;</td><td valign="top"><strong><em>$' . htmlspecialchars(number_format($n_discount, 2)) . '</em></strong></td></tr>';
                $summaryTable .= '<tr><td valign="top"><strong>TOTAL AFTER DISCOUNT:</td><td>&nbsp;</td><td valign="top"><strong><em>$' . htmlspecialchars(number_format((float)$o_catRec->getAttribute('balance') + (float)$o_catRec->getAttribute('deposit') - (float)$n_discount, 2)) . '</em> incl. GST</strong></td></tr>';
            }

            $s_promoCode = $o_catRec->getAttribute('promotionCode');
            if(!empty($s_promoCode)) {
                $summaryTable .= '<tr><td colspan="3">&nbsp;</strong></td></tr>';
                $summaryTable .= '<tr><td valign="top"><strong>Promotion Code:</td><td>&nbsp;</td><td valign="top">'.$s_promoCode.'</td></tr>';
            }



            $summaryTable .= '<tr><td colspan="3">&nbsp;</strong></td></tr>';

            $summaryTable .= '
            <tr><td valign="top">Booking Fee Payment Method:</td><td>&nbsp;</td><td valign="top">';
            if($a_transactionInfo['paymenttype']=='creditcard'){
                $summaryTable.='<em>Credit Card</em>';
            }else{
                $summaryTable.='<em>Bank Transfer</em><br />Reference: Please use &quot;<strong>'.htmlspecialchars($a_orderInfo['lastName']).'</strong>&quot;<br />Bank: Commonwealth Bank of Australia<br />BSB: 062907<br />Account Num: 10352850<br />Account Name: Multilocus Interactive Pty Ltd trading as Mulga Bicycle Tours';
            }

            $summaryTable .='</td></tr>

            <tr><td colspan="3">&nbsp;</strong></td></tr>
            '.($a_transactionInfo['paymenttype']=='creditcard'?
                '<tr><td valign="top">Booking Fee Received:</td><td>&nbsp;</td>':
                '<tr><td valign="top">Booking Fee to be transferred:</td><td>&nbsp;</td>'
            ).'
            <td valign="top"><em>$'.htmlspecialchars(number_format($o_catRec->getAttribute('deposit'),2)).' incl. GST</em></td></tr>

            <tr><td valign="top">Progress Payment 1 due on or before:</td><td>&nbsp;</td><td valign="top"><em>'.$pp1DateDisplay.'</em></td></tr>
            <tr><td valign="top">Progress Payment 2 due on or before:</td><td>&nbsp;</td><td valign="top"><em>'.$pp2DateDisplay.'</em></td></tr>
            <tr><td colspan="3">&nbsp;</strong></td></tr>
            <tr><td valign="top"><strong>TOTAL BALANCE DUE:</td><td>&nbsp;</td><td valign="top"><strong><em>'.htmlspecialchars($balance).'</em></strong></td></tr>
            <tr><td colspan="3">&nbsp;</strong></td></tr>
            ';
        $summaryTable .='</table>';

        $a_participantNames = array();
        foreach($a_participants as $a_participant){
            $a_participantNames[]='<tr><td valign="top">'.htmlspecialchars($a_participant['firstName'].' '.$a_participant['lastName']).'</td></tr>';
        }
        $s_groupMembers = '<table>'.implode('',$a_participantNames).'</table>';
        $s_orderNumber = $o_catRec->getAttribute('orderNumber');
        $s_message = '
<html><head>
<style type="text/css">
    *,td{
    font-family: "Helvetica Neue Light", "HelveticaNeue-Light", "Helvetica Neue", Calibri, Helvetica, Arial, sans-serif;
    }

    @media only screen and (max-width: 767px){
        #slides{
            display: none;
        }
    }
    </style>
</head><body>
<div style="background-color:#EEEEEE;  padding:10px 0px 10px 0px;">
    <div style="margin:10px; padding:10px 20px; max-width:700px; margin-left:auto; margin-right:auto; background-color:#FFFFFF; border:1px solid #bfbfbf;">
        <p style="text-align:right;"><img src="' .$s_siteUrl['en'].'source-assets/email/email_sig_logo.png?r=1" /></p>
        <p>Dear '.htmlspecialchars($a_orderInfo['firstName']).',</p>
        <p>Thank you for choosing to book with Mulga Bicycle Tours. We are pleased to confirm receipt of your booking request<strong>'.htmlspecialchars(  empty($s_orderNumber)?'':' '.$s_orderNumber  ).'</strong>.</p>

        <p>Acceptance and confirmation of your booking is subject to Mulga Bicycle Tours receiving a signed Waiver And Assumption Of Risk Form and a completed Guest Personal Information Form for each member of your party. These two forms are attached to this email.</p>
        <p>We recommend you use Adobe Acrobat Reader DC to complete these documents. Adobe Acrobat Reader DC software is the free global standard for reliably viewing, completing and printing PDF documents. There is a version of Acrobat Reader DC available for most devices.</p>
        <p>If you do not have Adobe Acrobat Reader DC you can download it from <a href="https://get.adobe.com/reader/">https://get.adobe.com/reader/</a>.</p>
        <p>Until we receive this documentation your places on the tour will be RESERVED. To enable us to confirm your booking please complete and forward these documents to us as soon as possible.</p>
        <p>In the meantime if you have any questions, please feel free to give us a call on 04 1230 9711.</p>

        <h2 style="color:#1fbba1;font-size:16px;">YOUR BOOKING DETAILS</h2>
        <table>
            <tr><td valign="top"><strong>Booking Reference Number:</strong></td><td>&nbsp;</td><td valign="top">'.(empty($s_orderNumber)?'WILL BE CONFIRMED':htmlspecialchars($s_orderNumber)).'</td></tr>
            <tr><td valign="top"><strong>Booking Status:</strong></td><td>&nbsp;</td><td valign="top">Reserved</a></td></tr>
        </table>
        <h2 style="color:#1fbba1;font-size:16px;">YOUR TOUR DETAILS</h2>
        <table>
            <tr><td valign="top"><strong>Tour:</strong></td><td>&nbsp;</td><td valign="top">'.htmlspecialchars($a_tourAvailabilityInfo['tour']['product']['name']).'</td></tr>
            <tr><td valign="top"><strong>Departure Date:</strong></td><td>&nbsp;</td><td valign="top">'.htmlspecialchars($starttimeDisplay).'</a></td></tr>
            <tr><td valign="top"><strong>Duration:</strong></td><td>&nbsp;</td><td valign="top">'.$durationDays.' Day'.($durationDays>1?'s; '.$durationDays.' Nights':'').'</td></tr>
        </table>

        <h2 style="color:#1fbba1;font-size:16px;">YOUR DETAILS</h2>
        <table>
            <tr><td valign="top"><strong>Name:</strong></td><td>&nbsp;</td><td valign="top">'.htmlspecialchars($a_orderInfo['firstName'].' '.$a_orderInfo['lastName']).'</td></tr>
            <tr><td valign="top"><strong>Address:</strong></td><td>&nbsp;</td><td valign="top">'.htmlspecialchars($a_orderInfo['address'].', '.$a_orderInfo['city']).'<br />'.htmlspecialchars($a_orderInfo['state'].', '.$a_orderInfo['postcode'].', '.$a_orderInfo['country']).'</td></tr>
            <tr><td valign="top"><strong>Email:</strong></td><td>&nbsp;</td><td valign="top"><a href="mailto:'.htmlspecialchars($a_orderInfo['email']).'">'.htmlspecialchars($a_orderInfo['email']).'</a></td></tr>
            <tr><td valign="top"><strong>Preferred Phone Number:</strong></td><td>&nbsp;</td><td valign="top">'.htmlspecialchars($a_orderInfo['phone']).'</td></tr>
            '.(!empty($a_orderInfo['howhear'])?'<tr><td valign="top"><strong>How did you hear about this tour?:</strong></td><td>&nbsp;</td><td valign="top">'.nl2br(htmlspecialchars($a_orderInfo['howhear'])).'</td></tr>':'').'

        </table>

        <h2 style="color:#1fbba1;font-size:16px;">GROUP MEMBERS</h2>
        '.$s_groupMembers.'

        <h2 style="color:#1fbba1;font-size:16px;">BEDDING CONFIGURATION</h2>
        <table>'.implode($accomLines).'</table>

        <h2 style="color:#1fbba1;font-size:16px;">PRICE BREAKDOWN</h2>'.$summaryTable.'
        <p>&nbsp;</p>

        <p style="text-align:right; font-size:11px;">
        PO Box 775 <span style="color:#1fbba1; font-weight:bold;">/</span> Mitchell ACT 2911 <span style="color:#1fbba1; font-weight:bold;">/</span> Australia<br />
        <a href="tel:0412309711" style="color:#000000;">04 1230 9711</a> <span style="color:#1fbba1; font-weight:bold;">/</span> <a href="mailto:bookings@mulgatours.com.au" style="color:#000000;">bookings@mulgatours.com.au</a> <span style="color:#1fbba1; font-weight:bold;">/</span> <a style="color:#000000;" href="http://mulgabicycletours.com.au">mulgabicycletours.com.au</a><br />
        <span style="font-size:9px;">Mulga Bicycle Tours is a Division of Multilocus Interactive Pty Ltd <span style="color:#1fbba1; font-weight:bold;">/</span> ACN 085 586 661 <span style="color:#1fbba1; font-weight:bold;">/</span> ABN 47 085 586 661</span>
        </p>
    </div>
</div>
</body></html>
';
        $_endSubject = 'Tour Booking Payment';
        if(empty($s_orderNumber)){
            $_endSubject = 'Booking will be confirmed';
        }

        $email = new EmailModel();
        if($b_debugOnly===true){
            $emailTo = array('chris@walkerdesigns.com.au');
        }else {
            $emailTo = array($a_orderInfo['email'], 'hello@mulgatours.com.au');
        }
        $email->replyTo = $email->fromEmail = 'hello@mulgatours.com.au';
        $email->fromName  = 'Mulga Bicycle Tours';
        $email->subject   = $_endSubject.' - '.craft()->getSiteName();
        $email->htmlBody  = $s_message;

        foreach ($emailTo as $emailAddress) {
            $email->toEmail = $emailAddress;
            $s_pubhtmlBase = dirname(CRAFT_BASE_PATH).'/public_html';
            $a_filesToAttach=array(
                array('path'=>$s_pubhtmlBase.$a_tourInfo['waiverForm'],'name'=>'Waiver and Assumption of Risk Form.pdf'),
                array('path'=>$s_pubhtmlBase.$a_tourInfo['personalInfoForm'],'name'=>'Guest Personal Information Form.pdf'),
            );
            foreach($a_filesToAttach as $file){
                if(file_exists($file['path'])) {
                    $email->addAttachment($file['path'],$file['name']);
                }
            }


            if (!craft()->email->sendEmail($email)) {
            }
        }
    }


}