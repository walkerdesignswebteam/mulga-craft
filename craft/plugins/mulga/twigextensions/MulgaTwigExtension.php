<?php
namespace Craft;
use Twig_Extension;
use Twig_Filter_Method;

require_once dirname(dirname(__FILE__)).'/includes/WalkerRezdy.class.php';

class MulgaTwigExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'MulgaSearch';
    }

    public function getFilters()
    {
        return array(
            'mulgaSearch' => new Twig_Filter_Method($this, 'mulgaSearch'),
            'toursList' => new Twig_Filter_Method($this, 'toursList'),
            'featuredTourPanel' => new Twig_Filter_Method($this, 'featuredTourPanel'),
        );
    }

    public function toursList()
    {
        $rezdyProdAvailabilityData = \WalkerRezdy::getProductAvailabilityData(craft());
        $a_quarters = \WalkerRezdy::availabilityDataToQuarters(craft(),$rezdyProdAvailabilityData['tours']);
        return array('tour_products'=>$rezdyProdAvailabilityData['products'],'quarterAvailabilities'=>$a_quarters);
        /*
        if(isset($rezdyProdAvailabilityData['tours'])){
            echo $this->getTemplateHtml('site/tours_index',);
        }
        return false;
        */
    }
    public function featuredTourPanel($number=0)
    {
        if($number == null){
            $number = 0;
        }
        $criteria = craft()->elements->getCriteria('Entry');
        $criteria->section('tours');
        $toursEntries = $criteria->find();
        
        $a_toursForPanel=array();
        $count = 0;
        foreach($toursEntries as $entry){
            if($entry->featured==1){
                if($count == $number){
                    $a_toursForPanel[]=array('tour_entry'=>$entry);
                break;
                }
                $count++;
            }
        }
        
        $rezdyProdAvailabilityData = \WalkerRezdy::getProductAvailabilityData(craft());
        foreach($a_toursForPanel as &$tour){
            $tour['availability']=array();
            foreach($rezdyProdAvailabilityData['tours'] as $availability){
                if($tour['tour_entry']->rezdyId==$availability->productCode) {
                    $tour['availability']=$availability;
                    break;
                }
            }
        }
        
        return $a_toursForPanel;
    }

    public function OLD_featuredTourPanel()
    {
        $criteria = craft()->elements->getCriteria('Entry');
        $criteria->section('tours');
        $toursEntries = $criteria->find();
        $a_toursForPanel=array();
        foreach($toursEntries as $entry){
            if($entry->featured==1){
                $a_toursForPanel[]=array('tour_entry'=>$entry);
                if(count($a_toursForPanel)==2){
                    break;
                }
            }
        }

       // print_r($a_toursForPanel); exit();
        $rezdyProdAvailabilityData = \WalkerRezdy::getProductAvailabilityData(craft());
        foreach($a_toursForPanel as &$tour){
            $tour['availability']=array();
            foreach($rezdyProdAvailabilityData['tours'] as $availability){
                if($tour['tour_entry']->rezdyId==$availability->productCode) {
                    $tour['availability']=$availability;
                    break;
                }
            }
        }
        return $a_toursForPanel;
    }
    
    public function mulgaSearch($entry)
    {
        if(!empty($entry->rezdyId)){
            $rezdyProdAvailabilityData = \WalkerRezdy::getProductAvailabilityData(craft());
            $a_quarters = \WalkerRezdy::availabilityDataToQuarters(craft(),$rezdyProdAvailabilityData['tours']);
            $a_ret = array();
            foreach($rezdyProdAvailabilityData['tours'] as $availability){

                if($entry->rezdyId==$availability->productCode) {
                    $availability->durationDays = \WalkerRezdy::getAvailabilityDuration($availability->startTime, $availability->endTime);
                    $a_ret[] = $availability;
                }
            }
            if(count($a_ret)>0){
                return $a_ret;
            }
        }
        return false;
    }
}