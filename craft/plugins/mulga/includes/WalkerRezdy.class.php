<?php
class WalkerRezdy
{
    private static $s_apiKey = '791ccf6b9cac414dbb6b75ad29e698b0';
    private static $s_apiUrl = 'https://api.rezdy.com/v1/';
    private static $b_ewayTestMode = false;
    function __construct() {
    }
    public static function getEntryByTourCode($craft,$code){
        $foundTourEntry = null;
        $criteria = $craft->elements->getCriteria('Entry');
        $criteria->section('tours');
        $toursEntries = $criteria->find();
        foreach($toursEntries as $entry){
            if($entry->rezdyId==$code){
                $foundTourEntry =$entry;
                break;
            }
        }
        return $foundTourEntry;
    }
    public static function getProductAvailabilityData($craft){
        //We have made out own caceh file store here simply because the Craft cache was clearing itself fo no reason frequently.
        $cacheFile = dirname(__DIR__).'/cache/rezdycache.cache';
        //Find and cache product information
        if($_SERVER['REQUEST_URI']=='/regen'){
            @unlink($cacheFile);
            error_log('Rezdy cache manually deleted by request at '.date('Y-m-d H:i:s'));
        }
        $s_content = @file_get_contents($cacheFile);
        $a_cacheVal = @unserialize($s_content);
        if(empty($a_cacheVal)){
            error_log('Rezdy cache regenerating at '.date('Y-m-d H:i:s'));
            $a_products = self::request('categories/79710/products');

            //loop through all products and get 2 years worth of data
            $secsForEachQuery = 60 * 60 * 24 * 80;

            $a_availabilitiesData = array();
            foreach($a_products->products as $a_prod){
                $dayStart = time();
                for($i=0;$i<9;$i++) {
		            $a_availCommand = array(
                        array('key' => 'productCode', 'val' => $a_prod->productCode),
                        array('key' => 'startTime', 'val' => date('Y-m-d',$dayStart) . 'T00:00:00Z'),
                        array('key' => 'endTime', 'val' => date('Y-m-d', $dayStart + $secsForEachQuery) . 'T00:00:00Z'),
                    );
                    $a_availabilityInfo = self::request('availability', $a_availCommand);
                    if(!empty($a_availabilityInfo->sessions)) {
                        foreach ($a_availabilityInfo->sessions as $thisInfo) {
                            //using the availability id as the array key will make certain that nothing can be multiple added (as it would overwrite anyway)
                            foreach ($a_products->products as $prod) {
                                if ($prod->productCode == $thisInfo->productCode) {
                                    $thisInfo->product = $prod;
                                    break;
                                }
                            }
                            $a_availabilitiesData[$thisInfo->id] = $thisInfo;
                        }
                    }else{
		    	if(isset($a_availabilityInfo->requestStatus->success) && $a_availabilityInfo->requestStatus->success==1){
				//response was valid.
			}else{
                   // echo 'Availability info did not contain an expected result from Rezdy.<br />';
				//print_r($a_availabilityInfo);
				//print_r($a_availCommand);
				//exit();
                        	//This really shouldn't ever happen, but seems to all the darn time. Probably should hard exit here, but it was getting too hard to actually sync the data.
			}
                    }
                    $dayStart += $secsForEachQuery;
                }
            }


            $dataToAddToCache = array('products'=>$a_products->products,'tours'=>$a_availabilitiesData);
            file_put_contents($cacheFile,serialize($dataToAddToCache));
            error_log('Rezdy cache finished regenerating at '.date('Y-m-d H:i:s'));
            $a_dataToReturn = $dataToAddToCache;
        }else{
            //remove any
            $a_dataToReturn = $a_cacheVal;
        }
        return self::removeAvailabilitiesBeforeToday($a_dataToReturn);
    }
    public static function removeAvailabilitiesBeforeToday($data){
        $hr24 = 60*60*24;
        $n_timeCutoff = time()-$hr24;
        $a_processedTours = array();
       foreach($data['tours'] as $key=>$val){
           if(strtotime($val->startTime)<$n_timeCutoff){
               //start time is less than current, remove
           }else{
               $a_processedTours[$key]=$val;
           }
        }
        $data['tours'] = $a_processedTours;
        return $data;
    }
    public static function getAvailabilityDuration($startTime,$endTime){
        return floor((strtotime($endTime) - strtotime($startTime)-1000) / (60 * 60 * 24));
    }
    public static function availabilityDataToQuarters($craft,$availabilityData){
        //group into annual quarters
        $hr24 = 60*60*24;
        $a_quarters = array();
        for($i=0;$i<3;$i++){
            $currYear = date('Y')+$i;
            $a_quarters[]=array('start'=>strtotime($currYear.'-01-01'),'end'=>strtotime($currYear.'-04-01')-$hr24);
            $a_quarters[]=array('start'=>strtotime($currYear.'-04-01'),'end'=>strtotime($currYear.'-07-01')-$hr24);
            $a_quarters[]=array('start'=>strtotime($currYear.'-07-01'),'end'=>strtotime($currYear.'-10-01')-$hr24);
            $a_quarters[]=array('start'=>strtotime($currYear.'-10-01'),'end'=>strtotime(($currYear+1).'-01-01')-$hr24);
        }

        //using & ref to write into the main quarters array with new data
        foreach($a_quarters as &$a_quarter){
            $a_quarter['tours']=array();
            $thisTours = array();
            $a_sisterSorter = array();
            $a_toursInQuarter = array();
            foreach($availabilityData as $availability){
                if(strtotime($availability->startTime)>=$a_quarter['start'] && strtotime($availability->startTime)<=$a_quarter['end']){
                    $thisEntry = WalkerRezdy::getEntryByTourCode($craft, $availability->productCode);;
                    if($thisEntry) {
                        $availability->entry=$thisEntry;
                        $availability->durationDays = self::getAvailabilityDuration($availability->startTime, $availability->endTime);
                        $availability->endTime = strtotime($availability->endTime . ' -1 day');
                        $a_sisterSorter[] = strtotime($availability->startTime);
                        $a_toursInQuarter[] = $availability;
                    }
                }
            }
            //sort by start date
            array_multisort($a_sisterSorter, $a_toursInQuarter);
            $a_quarter['tours']=$a_toursInQuarter;
        }
        return $a_quarters;
    }
    private static function createGetStrFromArr($arr) {
        $a_parts = array();
        foreach ($arr as $arg) {
            $key = $arg['key'];
            $val = $arg['val'];
            $a_parts[] = rawurlencode($key) . '=' . rawurlencode($val);
        }
        return implode('&', $a_parts);
    }
    static function request($cmd, $a_args = false)
    {
        $s_getArgs = '';
        if ($a_args) {
            $s_getArgs = '&' . self::createGetStrFromArr($a_args);
        }

        // Initialize the cURL session with the request URL
        $url = self::$s_apiUrl . $cmd.'?apiKey='.self::$s_apiKey.$s_getArgs;

        //Illuminate\Log::info('Request URL: '.$url);
        $session = curl_init($url);
        // Tell cURL to return the request data
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        // Set the HTTP request authentication headers
        $headers = array(
            'Content-Type: application/json; charset=UTF-8',
            'Accept: application/json',
        );
        curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
        // Execute cURL on the session handle
        $response = curl_exec($session);
        // Close the cURL session
        curl_close($session);
        return  json_decode($response);
    }
    static function postRequest($endpoint, $putString='')
    {
        $strLen = strlen($putString);
        $headers = array(
            'Content-Type: application/json; charset=UTF-8',
            'Accept: application/json',
            'Content-Length: '.$strLen
        );
        // Start curl
        $ch = curl_init();

        // Headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_URL, self::$s_apiUrl . $endpoint.'?apiKey='.self::$s_apiKey);

        if($putString != ''){
            curl_setopt($ch, CURLOPT_POST, true);
            // Put string into a temporary file
            $putData = tmpfile();
            fwrite($putData, $putString);
            fseek($putData, 0);
            curl_setopt($ch, CURLOPT_INFILE, $putData);
        }
        $response = curl_exec($ch);
        if($putString != ''){
            // Close the file
            fclose($putData);
        }
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);
        curl_close($ch);
        return $body;
    }
    static function getProductByCode($prodCode){
        $a_productInfo = WalkerRezdy::request('products/'.$prodCode,array(
            array('key'=>'productCode','val'=>$prodCode)
        ));
        if(isset($a_productInfo->product)) {
            return $a_productInfo->product;
        }
        return false;
    }
    static function getProductsInCategory($categoryId){
        $a_prods = WalkerRezdy::request('categories/'.$categoryId.'/products');
        $a_prodCodes = array();
        $a_sortedProducts = array();
        $a_sisterSorter = array();
        foreach($a_prods->products as $prod){
            $a_prodCodes[]=$prod->productCode;
            $a_sortedProducts[]=$prod;
            $a_sisterSorter[]=$prod->internalCode;
        }
        array_multisort($a_sisterSorter, $a_sortedProducts,$a_prodCodes);
        return array('productCodes'=>$a_prodCodes,'products'=>$a_sortedProducts);
    }

    static function mergeAvailabilityProdInfo($products,$availability){
        $a_ret = array();
        if(!empty($availability)) {
            foreach ($products as $prod) {
                $thisProdCode = $prod->productCode;
                foreach ($availability as $item) {
                    if ($prod->productCode == $item->productCode) {
                        $a_thisFullInfo = $item;
                        $a_thisFullInfo->product = array('productCode' => $prod->productCode, 'name' => $prod->name, 'shortDescription' => $prod->shortDescription);
                        $a_ret[] = $a_thisFullInfo;
                        break;
                    }
                }
            }
        }
        return $a_ret;
    }

    static function getAccommodationExtras($startTime){
        $prodAvailabilityInfo = self::getProductsInCategory('84895');
        return self::mergeAvailabilityProdInfo($prodAvailabilityInfo['products'],WalkerRezdy::getAvailability($prodAvailabilityInfo['productCodes'],$startTime));
    }

    static function getAccommodationOptions($startTime){
        $prodAvailabilityInfo = self::getProductsInCategory('79968');
        return self::mergeAvailabilityProdInfo($prodAvailabilityInfo['products'],WalkerRezdy::getAvailability($prodAvailabilityInfo['productCodes'],$startTime));
    }

    static function getExtrasOptions($startTime){
        $prodAvailabilityInfo = self::getProductsInCategory('81760');
        return self::mergeAvailabilityProdInfo($prodAvailabilityInfo['products'],WalkerRezdy::getAvailability($prodAvailabilityInfo['productCodes'],$startTime));
    }
    static function getRezdyTourIdFromSlug($craft,$slug){
        $criteria = $craft->elements->getCriteria('Entry');
        $criteria->slug = $slug;
        $toursEntries = $criteria->find();
        if(!empty($toursEntries[0]->rezdyId)){
           return  $toursEntries[0]->rezdyId;
        }
        return false;
    }
    static function getAvailability($prodCode,$startDate){
        $prodCodeQString = $prodCode;
        $args = array(
            array('key'=>'startTime','val'=>$startDate),
            array('key'=>'endTime','val'=>date('Y-m-d',strtotime($startDate)+(60*60*24)).'T00:00:00Z')
        );
        if(is_array($prodCode)){
            $prodCodeQString='';
            $a_parts=array();
            foreach($prodCode as $code){
                $args[]= array('key'=>'productCode','val'=>$code);
            }
            $prodCodeQString = implode('&',$a_parts);
        }else{
            $args[]= array('key'=>'productCode','val'=>$prodCode);
        }

        $a_availabilityInfo = WalkerRezdy::request('availability',$args);
        if(isset($a_availabilityInfo->sessions[0])){
            return $a_availabilityInfo->sessions;
        }
        return false;
    }
    static function makePayment($o_craftRequest){
        $a_transactiondetails = $o_craftRequest->getParam('cc_details');
        $a_bookinginfo = $o_craftRequest->getParam('bookingDetails');

        //print_r($a_transactiondetails);

        $a_transactiondetails['firstName']=$a_bookinginfo['firstName'];
        $a_transactiondetails['lastName']=$a_bookinginfo['lastName'];
        $a_transactiondetails['email']=$a_bookinginfo['email'];
        $a_transactiondetails['totalCents']=$a_transactiondetails['depositamount'] * 100;

        //print_r($a_paymentArgs);
        if(self::$b_ewayTestMode){
            $a_transactiondetails['testMode']=true;
            $a_transactiondetails['customerId']='91306057';
        }else{
            $a_transactiondetails['testMode']=false;
            $a_transactiondetails['customerId']='18724967';
        }

        $o_payment = new Payment($a_transactiondetails);
        if(isset($a_transactiondetails['cardtype'])){
            return $o_payment->handleEwayPayment();
        }else{
            return $o_payment->handleBankTransferPayment();
        }
    }
    static function processBooking( $o_craft,$n_deposit ){

        $o_craftRequest = $o_craft->request;

        $a_ccdetails = $o_craftRequest->getParam('cc_details');
        $a_participants = $o_craftRequest->getParam('participants');
        $a_bookinginfo = $o_craftRequest->getParam('bookingDetails');
        $a_tour = $o_craftRequest->getParam('tour');

        unset($a_bookinginfo['phone']); //Rezdy is picky and will fail the entire booking if this is not valid.

        $rezdyTourId = WalkerRezdy::getRezdyTourIdFromSlug($o_craft,$a_tour['tourid']);

        if($a_ccdetails['paymenttype']=='creditcard'){
            $s_label = 'Payment processed externally by eWay';
            $s_type = 'CREDITCARD';
        }else{
            $s_label = 'Payment to be made by Bank Transfer';
            $s_type = 'BANKTRANSFER';
        }

        $a_requestToSend = array(
            'internalNotes'=>'Payment made by API from website.',
            'payments'=>array(
                array('type'=>$s_type,'amount'=>$n_deposit,'date'=>date('c'),'label'=>$s_label)
            )
        );
        $a_requestToSend['customer']=$a_bookinginfo;
        //$a_requestToSend['comments']=$a_bookinginfo['howhear'];

        $a_bookItems = array();


        //Make main tour booking item

        $peopleOnTourSingle = 0;
        $peopleOnTourDouble = 0;
        $a_participantsForTour=array();
        foreach($a_participants as $a_participant){

            if (empty($a_participant['sharedwith']) || $a_participant['sharedwith'] == 'none') {
                $peopleOnTourSingle++;
            } else {
                $peopleOnTourDouble++;
            }

            $a_participantsForTour[] = array(
                'fields'=>array(
                    array('label'=>'First Name', 'value'=>$a_participant['firstName']),
                    array('label'=>'Last Name', 'value'=>$a_participant['lastName']),
                )
            );
        }
        $a_bookItems[]= array(
            'productCode'=>$rezdyTourId,
            'startTime'=>$a_tour['starttime'],
            'quantities'=>array(
                array('optionLabel'=>'Single', 'value'=>$peopleOnTourSingle),
                array('optionLabel'=>'Double', 'value'=>$peopleOnTourDouble)
            ),
            'participants'=>$a_participantsForTour,
        );


        //Add Accom
        $a_accomCodes = array();
        $a_accomBookItems = array();
        $a_accomRoomCounts = array();
        foreach($a_participants as $a_participant){
            $a_arraysToRunThrough=array();
            if(isset($a_participant['accom'])){
                $a_arraysToRunThrough[]=$a_participant['accom'];
            }
            if(isset($a_participant['accomExtras'])){
                $a_arraysToRunThrough[]=$a_participant['accomExtras'];
            }
            foreach($a_arraysToRunThrough as $arrayToRunThrough) {
                foreach ($arrayToRunThrough as $s_accomCode) {
                    if (in_array($s_accomCode, $a_accomCodes) === false) {
                        $a_accomCodes[] = $s_accomCode;
                    }
                    if (isset($a_accomBookItems[$s_accomCode]) === false) {
                        $a_accomBookItems[$s_accomCode] = array();
                        $a_accomRoomCounts[$s_accomCode] = array('Single' => 0, 'Double' => 0, 'Twin' => 0);
                    }

                    //quick way to see if value other than '' or none
                    if (empty($a_participant['sharedwith']) || $a_participant['sharedwith'] == 'none') {
                        $s_roomType = 'Single';
                    } else {
                        $s_roomType = $a_participant['doubletwin'];
                    }
                    $a_accomRoomCounts[$s_accomCode][$s_roomType]++;
                    $a_accomBookItems[$s_accomCode][] = array('fields' => array(
                        array('label' => 'First Name', 'value' => $a_participant['firstName']),
                        array('label' => 'Last Name', 'value' => $a_participant['lastName']),
                        array('label' => 'Special Requirements', 'value' => 'Room Type - ' . $s_roomType),
                    ));
                }
            }
        }
        //build final accom book items
       foreach($a_accomBookItems as $s_proKey=>$a_accomParticipants) {
           $a_thisQuantities = array();
           if($a_accomRoomCounts[$s_proKey]['Single']>0){
               $a_thisQuantities[] = array('optionLabel' =>'Single', 'value'=>$a_accomRoomCounts[$s_proKey]['Single']);
           }
           if($a_accomRoomCounts[$s_proKey]['Double']>0){
               $a_thisQuantities[] = array('optionLabel' =>'Double', 'value'=>$a_accomRoomCounts[$s_proKey]['Double']);
           }
           if($a_accomRoomCounts[$s_proKey]['Twin']>0){
               $a_thisQuantities[] = array('optionLabel' =>'Twin', 'value'=>$a_accomRoomCounts[$s_proKey]['Twin']);
           }
           $a_bookItems[] = array(
                'productCode' => $s_proKey,
                'startTime' => $a_tour['starttime'],

                'quantities' => $a_thisQuantities,
                'participants' => $a_accomParticipants,
           );
        }

        //extras
        $a_extraCodes=array();
        $a_extraItems=array();
        $a_extraCounts=array();
        //print_r($a_participants); exit();
        foreach($a_participants as $a_participant){
            if(isset($a_participant['extras'])){
                foreach ($a_participant['extras'] as $s_prodCode) {
                    if(in_array($s_prodCode,$a_extraCodes)===false){
                        $a_extraCodes[]=$s_prodCode;
                    }
                    if(isset($a_extraItems[$s_prodCode])===false) {
                        $a_extraItems[$s_prodCode] = array();
                        $a_extraCounts[$s_prodCode] = 0;
                    }
                    $a_extraCounts[$s_prodCode]++;
                    $a_extraItems[$s_prodCode][]= array('fields'=>array(
                        array('label'=>'First Name', 'value'=>$a_participant['firstName']),
                        array('label'=>'Last Name', 'value'=>$a_participant['lastName']),
                    ));
                }
            }
        }

        //build final extra book items
        foreach($a_extraItems as $s_proKey=>$a_extraParticipants) {
            $a_bookItems[] = array(
                'productCode' => $s_proKey,
                'startTime' => $a_tour['starttime'],
                'quantities' => array(
                    array('optionLabel'=>'Each','value' => $a_extraCounts[$s_proKey]),
                ),
                'participants' => $a_extraParticipants,
            );
        }

        $a_requestToSend['items']=$a_bookItems;
        $a_requestToSend['sendNotifications']=false;

        $s_jsonPostRequest = json_encode($a_requestToSend);

        /*
        echo '---------------------';
        print_r($a_ccdetails);
        print_r($a_participants);
        print_r($a_bookinginfo);
        print_r( $a_tour);
        exit();
        $s_jsonPostRequest = json_encode(array(
            'customer'=>array(
                'firstName'=>$_REQUEST['firstName'],
                'lastName'=>$_REQUEST['lastName'],
                'email'=>$_REQUEST['email'],
                'phone'=>$_REQUEST['phone'],
            ),
            'comments'=>$_REQUEST['comments'],
            'internalNotes'=>'Payment made by API from website.',
            'items'=>array(
                array(
                    'productCode'=>$o_availability->productCode,
                    'startTime'=>$o_availability->startTime,
                    'extras'=>array(
                        array('name'=>'Bike Hire','quantity'=>1),
                        array('name'=>'Electric Bike Hire','quantity'=>1),
                    ),
                    'quantities'=>array(
                        array('optionLabel'=>'Adult', 'value'=>$_REQUEST['quantity'])
                    ),
                    'participants'=>array(
                        array('fields'=>array(
                            array('label'=>'First Name', 'value'=>$_REQUEST['firstName0']),
                            array('label'=>'Last Name', 'value'=>$_REQUEST['lastName0']),
                        )),
                        array('fields'=>array(
                            array('label'=>'First Name', 'value'=>$_REQUEST['firstName1']),
                            array('label'=>'Last Name', 'value'=>$_REQUEST['lastName1']),
                        )),
                    ),
                ),
                array(
                    'productCode'=>'PXXWTN',
                    'startTime'=>$o_availability->startTime,
                    'quantities'=>array(
                        array('optionLabel'=>'Adult', 'value'=>1)
                    ),
                    'participants'=>array(
                        array('fields'=>array(
                            array('label'=>'First Name', 'value'=>$_REQUEST['firstName0']),
                            array('label'=>'Last Name', 'value'=>$_REQUEST['lastName0']),
                        )),
                    ),
                ),
                array(
                    'productCode'=>'PKXMZD',
                    'startTime'=>$o_availability->startTime,
                    'quantities'=>array(
                        array('optionLabel'=>'Adult', 'value'=>1)
                    ),
                    'participants'=>array(
                        array('fields'=>array(
                            array('label'=>'First Name', 'value'=>$_REQUEST['firstName0']),
                            array('label'=>'Last Name', 'value'=>$_REQUEST['lastName0']),
                        )),
                    ),
                ),
                array(
                    'productCode'=>'P8ZPVW',
                    'startTime'=>$o_availability->startTime,
                    'quantities'=>array(
                        array('optionLabel'=>'Adult', 'value'=>1)
                    ),
                    'participants'=>array(
                        array('fields'=>array(
                            array('label'=>'First Name', 'value'=>$_REQUEST['firstName0']),
                            array('label'=>'Last Name', 'value'=>$_REQUEST['lastName0']),
                        )),
                    ),
                )
            ),
            'payments'=>array(
                array('type'=>'CREDITCARD','amount'=>200,'date'=>date('c'),'label'=>'Payment processed externally by eWay')
            )
        ));
        */
        return WalkerRezdy::postRequest('bookings',$s_jsonPostRequest);
    }




}
