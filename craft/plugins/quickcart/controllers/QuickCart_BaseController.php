<?php
namespace Craft;
class QuickCart_BaseController extends BaseController
{
    protected $route_checkout = 'checkout/';
    protected $route_product = 'product/';
    protected $route_category = 'category/';

    //Will look first for user templates, then default to plugin templates if not found
    public function getQuickCartTemplate($path,$vars=false){
        $origTemplatePath = craft()->path->getTemplatesPath();

        $templateOverridePath = 'quickcart/'.$path;
        if(craft()->templates->findTemplate($templateOverridePath)){
            //echo $origTemplatePath; exit();
            $html = craft()->templates->render($templateOverridePath,$vars);
        }else{
            $pluginTemplatePath = craft()->path->getPluginsPath() . 'quickcart/templates/site';
            craft()->path->setTemplatesPath($pluginTemplatePath);
            $html = craft()->templates->render($path,$vars);
        }

        //set back to what the original path was
        craft()->path->setTemplatesPath($origTemplatePath);

        return $html;
    }
}