//MIXINS 
var WdformMixin = {
    getInitialState: function() {
        return {valid:false,hasChanged:false}
    },
    componentWillMount: function() {
        this.props.callbackElementAttach(this);
    },
    componentDidMount:function(){
        //validate for the first time
        this.validate(this.props.value);
    },
    validate:function(newVal){
        this.state.valid = true;
        this.a_errors = [];
        this.s_errors = '';
        var errId=0;
        if(this.props.rules){
            //required
            if(this.props.rules.required){  
                if(validator.isNull(newVal)){
                    this.state.valid = false;
                    this.a_errors.push({id:errId,message:this.props.label+' is required'});
                    errId++;
                }
            }
            //email
            if(this.props.rules.email){
                if(validator.isNull(newVal)===false && validator.isEmail(newVal)===false){
                    this.state.valid = false; 
                    this.a_errors.push({id:errId,message:this.props.label+' is not a valid email address'});
                    errId++;
                }
            }
            //urls
            if(this.props.rules.url){
                if(validator.isNull(newVal)===false && validator.isURL(newVal)===false){
                    this.state.valid = false; 
                    this.a_errors.push({id:errId,message:this.props.label+' is not a valid url'});
                    errId++;
                }
            }
        }
        this.labelErrors = this.a_errors.map(function(error) {
            return ( 
                React.createElement("span", {key: error.id}, error.message)
            );
        });
        this.forceUpdate();

        //fire event if specified
        if(this.props.wdOnChange){
            this.props.wdOnChange(this,newVal);
        }

        //callback up the form for validation listening
        this.props.callbackElementValueChange(this);
    },
    update:function(obj){
        this.state.hasChanged = true;
        this.validate(obj.target.value);
    }
};


//FORM HANDLER
var WdForm = React.createClass({displayName: "WdForm",
    elementAttach:function(elementRef){
        this.wdformElements.push(elementRef);
    },
    elementValueChange:function(elementRef){
        var invalidCnt=0;
        for(var i=0;i<this.wdformElements.length;i++){
            if(this.wdformElements[i].state.valid===false){
                invalidCnt++;
            }
        }

        this.state.invalidCount = invalidCnt;
        this.state.valid = invalidCnt===0;

        this.forceUpdate();
        if(this.props.wdOnChange){
            //add a very quick delay (1ms is enough) so that the form change event fires after any element events
            var _this = this;
            setTimeout(function(){_this.props.wdOnChange(_this)}, 1);
        }
    },
    registerElements:function(children){
        //just in case it is an empty form
        if (typeof children !== 'object' || children === null) {
            return children;
        }
        //loop all children 
        return React.Children.map(children, function (child) {
            if(child.type.displayName=='SubmitButton'){
                return React.cloneElement(child, {
                    onClick:this.submit,
                    label:React.createElement("span", null, child.props.label, this.state.showSubmitError && this.state.invalidCount>0?React.createElement("span", null, " (", this.state.invalidCount, " error", this.state.invalidCount!==1?'s':null, ")"):null)
                });
            }else if(child.props && child.props.label) {
                return React.cloneElement(child, {
                    callbackElementAttach: this.elementAttach,
                    callbackElementValueChange:this.elementValueChange,
                    wdformElId:'wdform-el-'+WdTools.makeUniqueId()
                }, child.props.children);
            } else {
                return React.cloneElement(child, {}, this.registerElements(child.props.children));
            }
        }, this);
    },
    submit:function(e){
        if(this.state.valid){
            this.props.wdOnSubmit();
        }else{
            //force all elements to validate
            for(var i=0;i<this.wdformElements.length;i++){
                this.wdformElements[i].state.hasChanged=true;
                this.wdformElements[i].forceUpdate();
            }
            this.state.showSubmitError=true;
            this.forceUpdate();
        }
    },


    getInitialState:function(){
        return {valid:false,invalidCount:0,showSubmitError:false}
    },
    componentWillMount:function(){
        this.wdformElements=[];

    },
    render:function(){
        this.formId = 'wdform-'+WdTools.makeUniqueId();
        var reprocessedChildren = this.registerElements(this.props.children);
        return React.createElement("form", {id: this.formId, className: this.state.showSubmitError===true && this.state.valid===false?"wdform invalid":"wdform valid", onSubmit: this.submit}, reprocessedChildren)
    }
});




//ELEMENTS
WdForm.Input = React.createClass({displayName: "Input",
    mixins: [WdformMixin],

    render:function(){
        return (
            React.createElement("div", {className: "formControlWrap"}, 
                React.createElement("label", {htmlFor: this.props.wdformElId}, this.props.label), 
                React.createElement("input", React.__spread({id: this.props.wdformElId, className: "form-control", placeholder: this.props.label, onChange: this.update},  this.props)), 
                this.state.hasChanged===true && this.state.valid===false?React.createElement("div", null, React.createElement("label", {htmlFor: this.props.wdformElId, className: "el-errors"}, this.labelErrors)):null
                /* <pre>state: {JSON.stringify(this.state, undefined, 2)}</pre> */
            )
        )
    }
});

WdForm.Date = React.createClass({displayName: "Date",
    mixins: [WdformMixin],
    componentDidMount:function(){
        var pickerEl =$('#'+this.props.wdformElId);
        var _this = this;
        pickerEl.datepicker({format: "dd/mm/yyyy"}).on("changeDate", function(e) {
            _this.value = e.target.value;
            _this.handleChange(e);
        }).on("clearDate", function(e) {
            _this.value = e.target.value;
            _this.handleChange(e);
        });
    },
    handleChange:function(e){
        //fire custom event up chain so that parent can update props.value
        //Not 100% sure if this is the best way. Couldnt find real clear documentation to do this. 
        this.update(e);
    },
    render:function(){
        return (
            React.createElement("div", {className: "formControlWrap"}, 
                React.createElement("label", {htmlFor: this.props.wdformElId}, this.props.label), 
                React.createElement("input", React.__spread({type: "text", id: this.props.wdformElId, className: "form-control date-picker", placeholder: "DD/MM/YYYY", onChange: this.handleChange},   this.props)), 
                this.state.hasChanged===true && this.state.valid===false?React.createElement("label", {htmlFor: this.props.wdformElId, className: "el-errors"}, this.labelErrors):null
            )
        )
    }
});

WdForm.Button = React.createClass({displayName: "Button",
    render:function(){
        return (
            React.createElement("button", React.__spread({type: "submit", onClick: this.props.onClick},  this.props), this.props.label)
        )
    }
});
//element that is tied to a form
WdForm.SubmitButton = React.createClass({displayName: "SubmitButton",
    render:function(){
        return (
            React.createElement("button", React.__spread({type: "submit"},  this.props), this.props.label)
        )
    }
});

WdForm.TextArea = React.createClass({displayName: "TextArea",
    mixins: [WdformMixin],
    render:function(){
        return (
            React.createElement("div", {className: "formControlWrap"}, 
                React.createElement("label", {htmlFor: this.props.wdformElId}, this.props.label), 
                React.createElement("textarea", React.__spread({id: this.props.wdformElId, className: "form-control", rows: "3", placeholder: this.props.label, onChange: this.update},  this.props)), 
                this.state.hasChanged===true && this.state.valid===false?React.createElement("label", {htmlFor: this.props.wdformElId, className: "el-errors"}, this.labelErrors):null
            )
        )
    }
});



var WdTools = {
	genBit:function(){
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },
    makeUniqueId:function(){
    	return this.genBit() + '-' + this.genBit() + '-' + this.genBit() + '-' + this.genBit();
    }
};

var WdUi = React.createClass({displayName: "WdUi",
  render: function() {
    return (
    React.createElement("div", null)
    ); 
  }
});
 
WdUi.Modal = React.createClass({displayName: "Modal",
  componentDidMount:function(){
    var thisRef = this;
    var modalRef = $(this.getDOMNode());
    modalRef.modal({
      backdrop: 'static'
    });
    modalRef.on('hidden.bs.modal', function (e) {
      //fire event if specified
      if(thisRef.props.wdOnClose){
          thisRef.props.wdOnClose(thisRef);
      }
    });
  },
  render: function() {
    return (
      React.createElement("div", {className: "modal fade", tabIndex: "-1", role: "dialog", "aria-labelledby": "myModalLabel", id: this.props.id}, 
        React.createElement("div", {className: "modal-dialog", role: "document"}, 
          React.createElement("div", {className: "modal-content"}, 
            React.createElement("div", {className: "modal-header"}, 
              React.createElement("button", {type: "button", className: "close", "data-dismiss": "modal", "aria-label": "Close"}, React.createElement("span", {"aria-hidden": "true"}, "×")), 
              React.createElement("h4", {className: "modal-title", id: "myModalLabel"}, this.props.title)
            ), 
            this.props.children
          )
        )
      )
    );
  }
});

var StaffDetailForm = React.createClass({displayName: "StaffDetailForm",
    submit:function(e){

        //check if updating or creating new
        var saveObject;
        if(this.state.formdata.id){
             for(var i=0;i<this.props.data.length;i++){
                if(this.props.data[i].id===this.state.formdata.id){
                    saveObject = this.props.data[i];
                }
            }
        }else{
            saveObject = {};
        }
        saveObject.firstName=this.state.formdata.firstName;
        saveObject.lastName=this.state.formdata.lastName,
        saveObject.email=this.state.formdata.email,
        saveObject.dob=this.state.formdata.dob,
        saveObject.description=this.state.formdata.description,
        saveObject.imageUrl=this.state.formdata.imageUrl

        if(saveObject.id){
            //update
        }else{
            //new
            this.props.data.push(saveObject);
        }


        this.props.addedCallback();
    },
    getInitialState: function() {
        return {
            valid:false,
            formdata:{
                id:'',
                firstName:'',
                lastName:'',
                email:'',
                dob:'',
                description:'',
                imageUrl:''
            }
        };
    },
    componentWillMount:function(){
        if(this.props.defaultFormData){
            this.state.formdata.id = this.props.defaultFormData.id;
            this.state.formdata.firstName = this.props.defaultFormData.firstName;
            this.state.formdata.lastName = this.props.defaultFormData.lastName;
            this.state.formdata.email = this.props.defaultFormData.email;
            this.state.formdata.dob = this.props.defaultFormData.dob;
            this.state.formdata.description = this.props.defaultFormData.description;
            this.state.formdata.imageUrl = this.props.defaultFormData.imageUrl;
        }
    },
    handleElementChange: function(el,newValue){
        this.state.formdata[el.props.dataKey] = newValue;
        this.forceUpdate();
    },
    handleFormChange: function(el,newValue){
        //an event we are not using.
        //console.log('changed');
    },

    render:function(){
        return(
            React.createElement("div", null, 
                React.createElement(WdForm, {className: "container", wdOnSubmit: this.submit, wdOnChange: this.handleFormChange}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "First Name", value: this.state.formdata.firstName, dataKey: "firstName", wdOnChange: this.handleElementChange})), 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "Last Name", value: this.state.formdata.lastName, dataKey: "lastName", wdOnChange: this.handleElementChange}))
                    ), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Date, {rules: {required:true}, label: "DOB", dataKey: "dob", wdOnChange: this.handleElementChange, value: this.state.formdata.dob})), 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true,email:true}, label: "Email Address", value: this.state.formdata.email, dataKey: "email", wdOnChange: this.handleElementChange}))
                    ), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-12"}, 
                            React.createElement(WdForm.Input, {rules: {required:false,url:true}, label: "Image URL", value: this.state.formdata.imageUrl, dataKey: "imageUrl", wdOnChange: this.handleElementChange})
                        )
                    ), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-12"}, 
                            React.createElement(WdForm.TextArea, {rules: {required:false}, label: "Staff Profile Description", value: this.state.formdata.description, dataKey: "description", wdOnChange: this.handleElementChange})
                        )
                    ), 
                    React.createElement("div", {className: "modal-footer"}, 
                          React.createElement("button", {type: "button", className: "btn btn-default", "data-dismiss": "modal"}, "Cancel"), 
                          React.createElement(WdForm.SubmitButton, {className: "btn btn-primary", label: this.props.submitLabel})
                    )
                )
                /* <pre>state: {JSON.stringify(this.state, undefined, 2)}</pre> */
            )
        );
    }
});

// Job Manager
var BookingUI = React.createClass({displayName: "BookingUI",
    componentDidMount:function(){

        var post_data = this.props;
        var _self = this;
        $.ajax({
            type: "POST",
            url: '/tour/bookajax',
            data: post_data,
            success: function( data ) {
                console.log(data);
                _self.setState(_self.props);
            }
        });

    },
    render:function(){
        return(React.createElement(BookingUI.Main, null));
    }
});
BookingUI.Main= React.createClass({displayName: "Main",
    getInitialState: function() {
        return {
            activeMenuItem:'ScreenParticipants',
            menuItems: [
                {index:0,id:'ScreenParticipants',title:'Tour Participants'},
                {index:1,id:'ScreenExtras',title:'Extras'},
                {index:2,id:'ScreenDetails',title:'Order Details'},
                {index:3,id:'ScreenBook',title:'Summary and Payment'},
            ]
        };
    },
    loadScreen:function(screenId){
        if(BookingUI[screenId]){
            React.render(React.createElement(BookingUI[screenId]),document.getElementById('screenContent'));
        }else{
            React.render(React.createElement("div", null, "BookingUI.", screenId, " was not found."),document.getElementById('screenContent'));
        }
    },
    refreshState:function(){
        this.forceUpdate();
    },
    getScreenIndex:function(){
        var activeIndex = 0;
        for(var i=0;i<this.state.menuItems.length;i++){
            if(this.activeMenuItem==this.state.menuItems[i].id){
                return i;
            }
        }
        return 0;
    },
    menuItemClicked:function(menuItem){
        this.state.activeMenuItem = menuItem.id;
        this.loadScreen(this.state.activeMenuItem);
        this.refreshState();
    },
    componentDidMount:function(){
        this.loadScreen(this.state.activeMenuItem);
    },
    render:function(){
        var _state = this.state;
        var screenIndex = this.getScreenIndex();
        var menuItemsBefore = this.state.menuItems.map(function(menuItem) {
            if(menuItem.index<=screenIndex) {
                var activeClass = (this.state.activeMenuItem === menuItem.id ? 'active' : 'inactive');
                return (
                    React.createElement("li", {key: menuItem.id, className: activeClass, onClick: this.menuItemClicked.bind(this,menuItem)}, React.createElement("a", {href: "#"}, menuItem.title))
                );
            }
        }.bind(this));

        var menuItemsAfter = this.state.menuItems.map(function(menuItem) {
            if(menuItem.index>screenIndex) {
                return (
                    React.createElement("li", {key: menuItem.id, onClick: this.menuItemClicked.bind(this,menuItem)}, menuItem.title)
                );
            }
        }.bind(this));

        return(
            React.createElement("div", {className: "container"}, 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-sm-12"}, 
                        React.createElement("ul", {className: "steps before"}, menuItemsBefore)
                    ), 

                    React.createElement("div", {className: "col-sm-12"}, React.createElement("div", {id: "screenContent"})), 

                    React.createElement("div", {className: "col-sm-12"}, 
                        React.createElement("ul", {className: "steps after"}, menuItemsAfter)
                    )
                )
            )
        );
    }
});
BookingUI.ParticipantForm = React.createClass({displayName: "ParticipantForm",
    getInitialState: function() {
        return {
            participant:this.props.data
        };
    },
    handleElementChange: function(el,newValue){
        this.state.formdata[el.props.dataKey] = newValue;
        this.forceUpdate();
    },
    handleFormChange: function(el,newValue){
        //an event we are not using.
        //console.log('changed');
    },
    render:function(){
        return(
            React.createElement("li", {className: "participant"}, 
                React.createElement(WdForm, {className: "container", wdOnSubmit: this.submit, wdOnChange: this.handleFormChange}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "First Name", value: this.state.participant.firstName, dataKey: "firstName", wdOnChange: this.handleElementChange})), 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true}, type: "text", label: "Last Name", value: this.state.participant.lastName, dataKey: "lastName", wdOnChange: this.handleElementChange}))
                    ), 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-6"}, React.createElement(WdForm.Input, {rules: {required:true,email:true}, label: "Email Address", value: this.state.participant.email, dataKey: "email", wdOnChange: this.handleElementChange}))
                    )
                )
            )
        );
    }
});
BookingUI.ParticipantFormList = React.createClass({displayName: "ParticipantFormList",
    render:function(){
        var _thisref = this;
        var content = this.props.data.map(function(participant) {
            return (
                React.createElement(BookingUI.ParticipantForm, {key: participant.id, data: participant, onEdit: _thisref.props.onEdit})
            );
        });
        return (
            React.createElement("ul", {className: "participantList"}, content)
        );
    }
});
BookingUI.ScreenParticipants = React.createClass({displayName: "ScreenParticipants",
     getInitialState: function() {
        return {
            participants:[
                {id:1,firstName:'',lastName:'',email:'',dob:''},
            ]
        };
    },
    componentWillMount:function(){
        this.modalContainerId = 'StaffModelContainer-'+WdTools.makeUniqueId();
        this.modalId = 'StaffModel-'+WdTools.makeUniqueId();
    },
    handleFormChange: function(el,newValue){
        //an event we are not using.
        //console.log('changed');
    },
    render:function(){
        return(
        React.createElement("div", null, 
            React.createElement("h2", null, "Tour Participants"), 
            React.createElement("p", null, "Please enter the details for each participant below. To add more participants, press the \"Add Participant\" button."), 

            React.createElement(WdForm, {className: "container", wdOnSubmit: this.submit, wdOnChange: this.handleFormChange}, 
                React.createElement(BookingUI.ParticipantFormList, {data: this.state.participants}), 
                React.createElement("div", {className: "modal-footer"}, 
                    React.createElement(WdForm.SubmitButton, {className: "btn btn-primary", label: "Continue"})
                )
            )
        )
        );
    }
});