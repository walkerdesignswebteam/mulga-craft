<?php
namespace Craft;
class QuickCart_ProductModel extends BaseModel
{
    protected function defineAttributes()
    {
        return array(
            'id' => AttributeType::Number,

            //copy and pasted from record
            'name' => AttributeType::String,
            'price' => array('type'=>AttributeType::Number,'decimals'=>2),
            'code' => array(AttributeType::String, 'required'=>true),
            'enabled' => AttributeType::Bool,
            'taxable' => AttributeType::Bool,
            'order' => array(AttributeType::Number, 'default'=>0),
            'description'=> AttributeType::Mixed
        );
    }
    public function getPriceString(){
        return '$'.$this->price.' inc GST';
    }
    public function getUrl(){
        return 'ssss/safasf/'.$this->code;
    }
}