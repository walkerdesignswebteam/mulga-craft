<?php
namespace QuickCart;

use Craft\Craft;

class Cart
{
    function __construct(){

    }
    private static $a_routes = array(
        'checkout'=>array(
            'action'=>'quickCart/checkout/checkout',
            'rootUrl'=>'checkout/',
            'urlPartVars'=>array(array('varName'=>'stepKey','links'=>array(
                'cart'=>'Your Cart',
                'account'=>'Your Account',
                'checkout'=>'Checkout',
                'summary'=>'Payment Summary',
            ))),
        ),
        'product'=>array(
            'action'=>'quickCart/product/show',
            'rootUrl'=>'product/',
            'urlPartVars'=>array(array('varName'=>'productCode'))
        ),
        'category'=>array(
            'action'=>'quickCart/category/show',
            'rootUrl'=>'category/',
            'urlPartVars'=>array(array('varName'=>'categoryCode'))
        ),
    );
    public function init(){

        foreach(self::$a_routes as $routeKey=>$a_route){
            $fullUrls = array();
            foreach($a_route['urlPartVars'] as $a_urlPart){
                if(isset($a_urlPart['links'])){
                    foreach($a_urlPart['links'] as $linkKey=>$linkValue){
                        $fullUrls[$linkKey] = array('url'=>'//'.$_SERVER['HTTP_HOST'].'/'.$a_route['rootUrl'].$linkKey,'title'=>$linkValue);
                    }
                }
            }
            self::$a_routes[$routeKey]['urls']=$fullUrls;
        }
    }
    public static function getRoutes(){
        //to debug links and what not
        //print_r(self::$a_routes);exit();
        return self::$a_routes;
    }
    public static function getRoute($routeKey){
        if(isset(self::$a_routes[$routeKey])){
            return self::$a_routes[$routeKey];
        }else{
            throw new HttpException(500, 'The request to getRoute "'.$routeKey.'" from QuickCart failed.');
        }
    }
}
Cart::init();
