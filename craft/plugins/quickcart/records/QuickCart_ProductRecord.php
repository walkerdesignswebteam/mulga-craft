<?php
namespace Craft;
class QuickCart_ProductRecord extends BaseRecord
{
	public function getTableName()
    {
        return 'quickcart_product';
    }

    protected function defineAttributes()
    {
        return array(
			'name' => AttributeType::String,
			'price' => array('type'=>AttributeType::Number,'decimals'=>2),
            'code' => array(AttributeType::String, 'required'=>true),
            'enabled' => AttributeType::Bool,
			'taxable' => AttributeType::Bool,
            'order' => array(AttributeType::Number, 'default'=>0),
			'description'=> AttributeType::Mixed
        );
    }
    public function defineIndexes()
    {
        return array(
            array('columns' => array('code'), 'unique' => true),
        );
    }
}