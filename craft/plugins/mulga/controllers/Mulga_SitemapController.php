<?php
namespace Craft;
require_once dirname(dirname(__FILE__)).'/includes/WalkerRezdy.class.php';
use WalkerRezdy;
//Inspired by http://xodigital.com.au , https://github.com/xodigital/SimpleSitemap
class Mulga_SitemapController extends BaseController
{
	protected $allowAnonymous = array('actionIndex');

	public function actionIndex()
	{
		$xml = new \SimpleXMLElement(
			'<?xml version="1.0" encoding="UTF-8"?>' .
			'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>'
		);

		$s_siteUrl = craft()->config->get('siteUrl');
		$rezdyProdAvailabilityData = \WalkerRezdy::getProductAvailabilityData(craft());
		$a_quarters = \WalkerRezdy::availabilityDataToQuarters(craft(),$rezdyProdAvailabilityData['tours']);
		//print_r($a_quarters); exit();
		foreach($a_quarters as $q_quarter){
			if(!empty($q_quarter)){
				foreach($q_quarter['tours'] as $availability) {
					$url = $xml->addChild('url');
					$url->addChild('loc', $s_siteUrl['en'].$availability->entry->slug);
					$url->addChild('lastmod', $availability->entry->dateUpdated->format(\DateTime::W3C));
					$url->addChild('priority', 0.25);
					
					
					$url = $xml->addChild('url');
					$url->addChild('loc', $s_siteUrl['en'].'tour/book/'.$availability->entry->slug.'/'.$availability->startTime);
					$url->addChild('lastmod', $availability->entry->dateUpdated->format(\DateTime::W3C));
					$url->addChild('priority', 0.25);
				}
			}
		}
		/*
		foreach($a_quarters as $q_quarter){
			if(!empty($q_quarter)){
				foreach($q_quarter['tours'] as $availability) {
					$url = $xml->addChild('url');
					$url->addChild('loc', $s_siteUrl['en'].'tour/'.$availability->entry->slug.'/'.$availability->startTime);
					$url->addChild('lastmod', $availability->entry->dateUpdated->format(\DateTime::W3C));
					$url->addChild('priority', 0.25);
				}
			}
		}
		*/

		HeaderHelper::setContentTypeByExtension('xml');
		ob_start();
		echo $xml->asXML();
		craft()->end();
	}
}
