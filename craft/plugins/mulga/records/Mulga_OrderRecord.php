<?php
namespace Craft;
class Mulga_OrderRecord extends BaseRecord
{
	public function getTableName()
    {
        return 'mulga_order';
    }

    protected function defineAttributes()
    {
        return array(
			'firstName' => AttributeType::String,
			'lastName' => AttributeType::String,
			'orderNumber' => AttributeType::String,
            'tourId' => AttributeType::String,
            'deposit' => array('type'=>AttributeType::Number,'decimals'=>2),
            'balance' => array('type'=>AttributeType::Number,'decimals'=>2),
            'discount' => array('type'=>AttributeType::Number,'decimals'=>2),
            'paymentSummary' => AttributeType::Mixed,
            'orderInfo' => AttributeType::Mixed,
            'transactionInfo' => AttributeType::Mixed,
            'availabilityInfo' => AttributeType::Mixed,
            'promotionCode'=>AttributeType::String,
            'promotionInfo' => AttributeType::Mixed,
            'participants' => AttributeType::Mixed,
            'tourInfo' => AttributeType::Mixed,
            'rezdyResponse' => AttributeType::Mixed,
        );
    }
    public function defineIndexes()
    {
        return array(
            array('columns' => array('orderNumber'), 'unique' => true),
        );
    }
}