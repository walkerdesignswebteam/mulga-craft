<?php
namespace Craft;

class MulgaPlugin extends BasePlugin
{
    public function getName()
    {
         return Craft::t('Mulga');
    }
    public function getVersion()
    {
        return '0.1';
    }
    public function getDeveloper()
    {
        return 'Walker Designs';
    }
    public function getDeveloperUrl()
    {
        return 'http://walkerdesigns.com.au/';
    }
	public function hasCpSection()
    {
        return false;
    }
/*
	protected function defineSettings()
    {
        return array(
            'notificationEmail' => array(AttributeType::Email, 'required' => true),
        );
    }


	public function getSettingsHtml()
    {
       return craft()->templates->render('mulga/cp/settings', array(
           'settings' => $this->getSettings()
       ));
   }
*/
    public function registerCpRoutes()
    {
        return array(
            //'quickcart/products' => array('action' => 'quickCart/adminProduct/index'),
        );
    }
    public function registerSiteRoutes()
    {
        return array(
            //'tours' => array('action' => 'mulga/tours/index'),
            'tour/testpaymentnotification' => array('action' => 'mulga/tours/testpaymentnotification'),
            'tour/bookajax' => array('action' => 'mulga/tours/bookajax'),
            'tour/getpromotioninfoajax' => array('action' => 'mulga/tours/getpromotionajax'),
            'tour/pay' => array('action' => 'mulga/tours/pay'),
            'tour/book/(?P<tourid>[^/]+)/(?P<starttime>[^/]+)' => array('action' => 'mulga/tours/book'),
            'tours/(?P<arg>[^/]+)' => array('action' => 'mulga/tours/index'),
            'tour/(?P<tourid>[^/]+)/(?P<starttime>[^/]+)' => array('action' => 'mulga/tours/show'),
            'sitemaptours.xml' => array('action' => 'mulga/sitemap/index'),
        );
    }

    public function addTwigExtension()
    {
        Craft::import('plugins.mulga.twigextensions.MulgaTwigExtension');
        return array(
            new MulgaTwigExtension(),
        );
    }
}
