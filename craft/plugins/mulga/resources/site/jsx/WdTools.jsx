var WdTools = {
	genBit:function(){
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    },
    makeUniqueId:function(){
    	return this.genBit() + '-' + this.genBit() + '-' + this.genBit() + '-' + this.genBit();
    },
    toCurrency:function(number, decPlaces, thouSeparator, decSeparator) {
        var n = number
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    },
    priceFormat:function(number,valIfZero){
        if(number==0){
            if(typeof(valIfZero)==='undefined'){
                return 'Free';
            }else{
                return valIfZero;
            }
        }
        return '$'+this.toCurrency(number);
    }
};