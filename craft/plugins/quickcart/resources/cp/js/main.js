/**
 * Created by Marcus on 1/11/2015.
 */
var QuickCart = function() {
};

QuickCart.TableSorter = function(table,postUrl) {
    this.table = table;
    this.sorter = null;
    this.postUrl = postUrl;
    this.init();
};

QuickCart.TableSorter.prototype.init = function() {
    var _self = this;
    this.sorter = new Craft.DataTableSorter(this.table, {
        onSortChange: function () {
            //can get this, but don't really need it
            var draggeeId = _self.sorter.$draggee.data('id');
            var ids = [];
            for (var i = 0; i <_self.sorter.$items.length; i++) {
                var id = $(_self.sorter.$items[i]).data('id');
                ids.push(id);
            }
            Craft.postActionRequest(_self.postUrl,{ids:ids,draggeeId:draggeeId}, $.proxy(function (response, textStatus) {

                var loc = window.location.href.split('?');
                window.location.href = loc[0] + '?success='+encodeURIComponent(response.success)+'&message='+encodeURIComponent(response.message);
                Craft.cp.displayNotice('Reloading... please wait');

            }, this));
        }
    });
};
